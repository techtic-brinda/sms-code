import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerticalLayoutComponent } from './layout/layout.component';
import { AuthGuard } from 'app/shared/services/auth/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: VerticalLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            { path: 'dashboard', loadChildren: () => import('app/main/dashboard/dashboard.module').then(m => m.DashboardModule) },
            { path: 'users', loadChildren: () => import('app/main/users/users.module').then(m => m.UsersModule) },
            { path: 'profile', loadChildren: () => import('app/main/profile/profile.module').then(m => m.ProfileModule) },
            { path: 'transaction', loadChildren: () => import('app/main/transaction/transaction.module').then(m => m.TransactionModule)  },
            { path: 'invoice', loadChildren: () => import('app/main/invoice/invoice.module').then(m => m.InvoiceModule) },
            { path: 'stake', loadChildren: () => import('app/main/post/post.module').then(m => m.PostModule) },
            { path: 'sports-type', loadChildren: () => import('app/main/sports-type/sports-type.module').then(m => m.SportsTypeModule) },
            { path: 'support', loadChildren: () => import('app/main/support-ticket/support-ticket.module').then(m => m.SupportTicketModule) },
            { path: 'faq', loadChildren: () => import('app/main/faq/faq.module').then(m => m.FaqModule) },
            { path: 'page', loadChildren: () => import('app/main/pages/pages.module').then(m => m.PagesModule) },
            { path: 'tutorial', loadChildren: () => import('app/main/tutorial/tutorial.module').then(m => m.TutorialModule) },
            { path: 'email-template', loadChildren: () => import('app/main/email-template/email-template.module').then(m => m.EmailTemplateModule) },
            { path: 'notification-template', loadChildren: () => import('app/main/notification-template/notification-template.module').then(m => m.NotificationTemplateModule) },
            { path: 'settings', loadChildren: () => import('app/main/settings/settings.module').then(m => m.SettingsModule) },
            { path: 'private-coaching', loadChildren: () => import('app/main/private-coaching/private-coaching.module').then(m => m.PrivateCoachingModule) },
           
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
