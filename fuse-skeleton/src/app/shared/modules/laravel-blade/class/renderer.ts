import { DomSanitizer } from '@angular/platform-browser';
export class RendererClass {
    
    viewId:number;
    functions:any;
    variables:any;
    utils:any;
	builder:any;
	

    constructor(public sanitizer: DomSanitizer){
		
        this.viewId = 0;

   
        this.functions = {

        };

        this.variables = {

        };

        this.utils = {
			views: [],
			sanitizer: this.sanitizer,
			escapeHTML(value: any): any {
				return value;
			},
			viewBegin: viewId => {
				this.utils.views.push({
					parent: null
				});

				return '';
			},
			viewEnd: (parent, viewId, result) => {
				let viewData = this.utils.getCurrentViewData();

				if (viewData.parent) {
					result = this.render(viewData.parent, viewData.parentParameters);
				}

				this.utils.views.pop();

				return result;
			},

			forEach: array => {
				let result = [  ];

				if (array instanceof Array) {
					array.forEach((value, key) => result.push([ key, value ]));
				} else {
					for (let key in array) {
						if (array.hasOwnProperty(key)) {
							let value = array[key];
							result.push([ key, value ]);
						}
					}
				}

				return result;
			},

			getCurrentViewData: () => this.utils.views[this.utils.views.length - 1],
			getPrevViewData: () => this.utils.views[this.utils.views.length - 2]
		};
		
		// Control blocks

        this.registerFunction('if', {
			callback: parameters => `if (${parameters}) {`,
			output: false,
			escape: false
		});

		this.registerFunction('else', {
			callback: parameters => `} else {`,
			output: false,
			escape: false
		});

		this.registerFunction('elseif', {
			callback: parameters => `} else if (${parameters}) {`,
			output: false,
			escape: false
		});

		this.registerFunction('endif', {
			callback: parameters => `}`,
			output: false,
			escape: false
		});

		// Loops
		this.registerFunction('for', {
			callback: parameters => `for (${parameters}) {`,

			output: false,
			escape: false
		});

		this.registerFunction('endfor', {
			callback: parameters => `}`,

			output: false,
			escape: false
		});


		this.registerFunction('unless', {
			callback: parameters => `if (!(${parameters})) {`,

			output: false,
			escape: false
		});

		this.registerFunction('endunless', {
			callback: parameters => `}`,

			output: false,
			escape: false
		});

		

		this.registerFunction('foreach', {
			callback: parameters => {
				let variables = parameters.match(/([$A-Za-z][$A-Za-z]*)/g).filter(name => name !== 'as');

				if (variables.length === 2) {
					variables.splice(1, 0, '__unused');
				}

				let array = variables[0];
				let key = variables[1];
				let value = variables[2];

				return `{ let $__items = $__utils.forEach(${array});\nfor (let $__i = 0, $__entry = $__items[$__i] || [], ${key} = $__entry[0], ${value} = $__entry[1]; $__i < $__items.length; ++$__i, $__entry = $__items[$__i] || [], ${key} = $__entry[0], ${value} = $__entry[1]) {`;
			},

			output: false,
			escape: false
		});

		this.registerFunction('endforeach', {
			callback: parameters => `} }`,

			output: false,
			escape: false
		});

		this.registerFunction('forelse', {
			callback: parameters => {
				let variables = parameters.match(/([$A-Za-z][$A-Za-z]*)/g).filter(name => name !== 'as');

				if (variables.length === 2) {
					variables.splice(1, 0, '__unused');
				}

				let array = variables[0];
				let key = variables[1];
				let value = variables[2];

				return `{ let $__items = $__utils.forEach(${array});\nfor (let $__i = 0, $__entry = $__items[$__i] || [], ${key} = $__entry[0], ${value} = $__entry[1]; $__i < $__items.length; ++$__i, $__entry = $__items[$__i] || [], ${key} = $__entry[0], ${value} = $__entry[1]) {`;
			},

			output: false,
			escape: false
		});

		this.registerFunction('empty', {
			callback: parameters => `} if ($__items.length === 0) {`,

			output: false,
			escape: false
		});

		this.registerFunction('endforelse', {
			callback: parameters => `} }\n`,

			output: false,
			escape: false
		});

		this.registerFunction('while', {
			callback: parameters => `while (${parameters}) {`,

			output: false,
			escape: false
		});

		this.registerFunction('endwhile', {
			callback: parameters => `}`,

			output: false,
			escape: false
		});

		this.registerFunction('continue', {
			callback: parameters => {
				if (parameters === '') {
					return 'continue';
				}

				return `if (${parameters}) {\ncontinue;\n}`;
			},

			output: false,
			escape: false
		});

		this.registerFunction('break', {
			callback: parameters => {
				if (parameters === '') {
					return 'break';
				}

				return `if (${parameters}) {\nbreak;\n}`;
			},

			output: false,
			escape: false
		});


		
    }

    registerFunction(name:string, object:any) {
		object = Object.assign({
			callback: () => '',
			output: false,
			escape: false
		}, object);

		this.functions[name] = object;
	}


    registerVariable(name:string, value:any, interactive = false) {
		this.variables[name] = {
			value: value,
			interactive: interactive
		};
    }
    

    render(view, properties?:any) {
		let new_properties = {};
		for(let key in properties){
			new_properties["$"+key] = properties[key];
		}
		properties = new_properties;
		try {
            let viewId = this.viewId++;
            
			let viewObject = {
				view: view,
				properties: properties
			};

			let renderedContent = this._compile(view).build();

			return this.renderView(renderedContent, properties, viewId, viewObject);
		} catch (error) {

			console.error(`Failed to render view`, error);

			throw error;
		}
    }

    renderView(code, properties, viewId, viewObject) {
		
		let viewFunc:Function = eval.call(properties, code);
		return viewFunc.call(properties, this.utils, viewId, this.variables, viewObject);
	}
    

    _compile(code) {
		this.builder = {
			_text: '',

			append: (text) => {
				this.builder._text += text;
			},

			build: () => {
				return this.builder._text;
			}
		};

		this.builder.append(`(function($__utils, $__viewId, $__sharedVariables, $__viewObject) {
            for (let key in this) {
                if (this.hasOwnProperty(key)) {
                    if (typeof this[key] === 'function') {
                        eval(\`var \${key} = this[key].bind(null, $__viewObject);\`);
                    } else {
                        eval(\`var \${key} = this[key];\`);
                    }
                }
            }\n
	
            for (let key in $__sharedVariables) {
                if ($__sharedVariables.hasOwnProperty(key)) {
                    if (typeof $__sharedVariables[key].value === 'function') {
                        if ($__sharedVariables[key].interactive) {
                            eval(\`var \${key} = $__sharedVariables[key].value($__viewObject);\`);
                        } else {
                            eval(\`var \${key} = $__sharedVariables[key].value.bind(null, $__viewObject);\`);
                        }
                    } else {
                        eval(\`var \${key} = $__sharedVariables[key].value;\`);
                    }
                }
            }\n`);

        this.builder.append('var $__result = $__utils.viewBegin($__viewId);\n');

		this._compileCode(code, this.builder);

		this.builder.append('return $__utils.viewEnd($__viewObject, $__viewId, $__result);\n');
		this.builder.append('})\n');

		return this.builder;
    }
    



    _compileCode(code, builder) {
		const STATE_DEFAULT = 0;
		const STATE_ENCODED_DATA = 1;
		const STATE_UNENCODED_DATA = 2;
		const STATE_COMMENT = 3;
		const STATE_FUNCTION = 4;
		const STATE_FUNCTION_PARAMS = 5;

		let state = STATE_DEFAULT;
		let defaultStart = 0;
		let encodedDataStart = 0;
		let unencodedDataStart = 0;

		let functionStart = 0;
		let functionName = '';

		let functionParamsStart = 0;
		let functionParamsBraceCount = 0;

		/*
			{{ $value }}
			@if(...)
			@elseif(...)
			@endif
			@foreach(...)
			@endforeach


			/{{(.*?)}}/gmi

			/@if\((.*?)\)/gmi

			/@elseif\((.*?)\)/gmi

			/@endif/gmi

			/@foreach\((.*?)\)/gmi

			/@endforeach/gmi

		 */

		for (let i = 0; i < code.length; ++i) {
			let char = code[i];
			let char1 = code[i + 1];
			let char2 = code[i + 2];
			let char3 = code[i + 3];

			

			switch (state) {
				case STATE_DEFAULT:
					if (defaultStart === null) {
						defaultStart = i;
					}

					if (char === '{') {
						if (char1 === '{') {
							if ((char2 === '-') && (char2 === '-')) {
								state = STATE_COMMENT;
							} else {
								state = STATE_ENCODED_DATA;
								encodedDataStart = i + 2;
							}
						} else {
							if ((char1 === '!') && (char2 === '!')) {
								state = STATE_UNENCODED_DATA;
								unencodedDataStart = i + 3;
							}
						}
					}

			
					if (state === STATE_DEFAULT) {
					
						if (char === '@') {

							state = STATE_FUNCTION;
							functionStart = i + 1;
							functionName = null;
						}
					}


					if (state !== STATE_DEFAULT) {

						builder.append('$__result += ');
						builder.append(this._escape(code.substring(defaultStart, i)));
						builder.append(';\n');

						defaultStart = null;
					}
					break;
				case STATE_ENCODED_DATA:
					if ((char === '}') && (char1 === '}')) {
						builder.append('$__result += $__utils.escapeHTML(');
						builder.append(code.substring(encodedDataStart, i));
						builder.append(');\n');

						state = STATE_DEFAULT;
						++i;
					}
					break;
				case STATE_UNENCODED_DATA:
					if ((char === '!') && (char1 === '!') && (char2 === '}')) {
						builder.append('$__result += (');
						builder.append(code.substring(unencodedDataStart, i));
						builder.append(');\n');

						state = STATE_DEFAULT;
						i += 2;
					}
					break;
				case STATE_COMMENT:
					if ((char === '-') && (char1 === '-') && (char2 === '}') && (char3 === '}')) {
						state = STATE_DEFAULT;
						i += 3;
					}
					break;
				case STATE_FUNCTION:
					if (functionStart === i) {
						
						if (!char.match(/[a-zA-Z]/)) {
							builder.append('$__result += \'@\';\n');
							state = STATE_DEFAULT;
						}
					} else if (char.match(/[a-zA-Z0-9]/)) {
						
					} else if (char.match(/[ \t]/)) {
						
						if (functionName === null) {
							functionName = code.substring(functionStart, i).trim();
						}

						builder.append(this._makeFunction(functionName));
						state = STATE_DEFAULT;

					} else if (char === '(') {
						
						if (functionName === null) {
							functionName = code.substring(functionStart, i).trim();
						}

						state = STATE_FUNCTION_PARAMS;
						functionParamsStart = i + 1;
						functionParamsBraceCount = 1;
					} else {
						
						if (functionName === null) {
							functionName = code.substring(functionStart, i).trim();
						}

						builder.append(this._makeFunction(functionName));

						state = STATE_DEFAULT;
					}
					break;
				case STATE_FUNCTION_PARAMS:
					switch (char) {
					case '(':
						++functionParamsBraceCount;
						break;
					case ')':
						if (--functionParamsBraceCount === 0) {
							builder.append(this._makeFunction(functionName, code.substring(functionParamsStart, i)));

							state = STATE_DEFAULT;
						}
						break;
					}
					break;
			}
		}

		switch (state) {
		case STATE_DEFAULT:
			if (defaultStart !== null) {
				builder.append('$__result += ');
				
				builder.append(this._escape(code.substring(defaultStart)));
				builder.append(';');
			}
			break;
		case STATE_FUNCTION:
			if (functionName === null) {
				functionName = code.substring(functionStart).trim();
				builder.append(this._makeFunction(functionName));
			}
			break;
		}
    }
    
    _escape(string) {
		return `'${string}'`;
		//return `'${escape(string)}'`;
    }
    
    _makeFunction(name, parameters = '') {
		if (this.functions.hasOwnProperty(name)) {
			let functionData = this.functions[name];
			let result = functionData.callback(parameters);
			
			

			if (functionData.output) {
				
				if (functionData.escape) {
					result = `$__result += $__utils.escapeHTML(${result});`;
				} else {
					result = `$__result += ${result};`;
				}
			}
			return result + '\n';
		}

		return '';
	}

}