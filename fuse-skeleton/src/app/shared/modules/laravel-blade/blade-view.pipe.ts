import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { RendererClass } from './class/renderer';

@Pipe({
  name: 'bladeView'
})
export class BladeViewPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {
  }
  transform(value: any, args?: any): any {
    
    let template = "";
    template = new RendererClass(this.sanitizer).render(value, args);
    return template;
  }

}
