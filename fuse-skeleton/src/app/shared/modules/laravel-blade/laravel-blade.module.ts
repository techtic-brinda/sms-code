import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BladeViewPipe } from './blade-view.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [BladeViewPipe],
  exports: [BladeViewPipe]
})
export class LaravelBladeModule { }
