import { LaravelBladeModule } from './laravel-blade.module';

describe('LaravelBladeModule', () => {
  let laravelBladeModule: LaravelBladeModule;

  beforeEach(() => {
    laravelBladeModule = new LaravelBladeModule();
  });

  it('should create an instance', () => {
    expect(laravelBladeModule).toBeTruthy();
  });
});
