import { RepeaterFieldModule } from './repeater-field.module';

describe('RepeaterFieldModule', () => {
  let repeaterFieldModule: RepeaterFieldModule;

  beforeEach(() => {
    repeaterFieldModule = new RepeaterFieldModule();
  });

  it('should create an instance', () => {
    expect(repeaterFieldModule).toBeTruthy();
  });
});
