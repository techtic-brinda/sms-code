import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../../environments/environment';
@Pipe({
    name: 'assetsUrl'
})
export class AssetsUrl implements PipeTransform {
    transform(value: string): string {
        return environment.deployUrl + '/' + value;
    }
}