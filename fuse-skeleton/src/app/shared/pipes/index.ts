import { AssetsPath } from './image';
import { WrapPTag } from "./wrap-p-tag";
import { FilterPipe } from './filter.pipe';
import { UpdateAnchor, SafeHtmlPipe, TruncatePipe, UcFirstPipe } from './text';
import { StrToDatePipe } from './str-to-date.pipe';
import { AssetsUrl } from './assets-url.pipe';
import { AutofocusDirective } from './autofocuss.pipe';

const Pipes = [
    AssetsPath,
    FilterPipe,
    WrapPTag,
    UpdateAnchor,
    SafeHtmlPipe,
    TruncatePipe,
    StrToDatePipe,
    AssetsUrl,
    UcFirstPipe,
    AutofocusDirective
];

export { Pipes };
