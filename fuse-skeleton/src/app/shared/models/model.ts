export interface ModelInterface {
    getJson(deep?: boolean): Object;
    toJson(deep?: boolean): Object;
    getString(deep?: boolean): string;
}

export class Model implements ModelInterface {

    getJson(deep: boolean = false): Object {
        let json: any = this.toJson(deep);
        return json;
    }

    toJson(deep: boolean = false): Object {
        const proto = Object.getPrototypeOf(this);
        let jsonObj: any = Object.assign({}, this);

        Object.entries(Object.getOwnPropertyDescriptors(proto))
            .filter(([key, descriptor]) => typeof descriptor.get === 'function')
            .map(([key, descriptor]) => {
                if (descriptor && key[0] !== '_') {
                    try {
                        const val = (this as any)[key];
                        jsonObj[key] = val;

                        delete jsonObj["_" + key];

                    } catch (error) {
                        console.error(`Error calling getter ${key}`, error);
                    }
                }
            });


        if (deep) {
            jsonObj = this.checkToJson(jsonObj);
        }
        return jsonObj;
    }

    getString(deep: boolean = false): string {
        let json: any = this.toJson(deep);
        return JSON.stringify(json);
    }

    private checkToJson(obj: any): any {
        if (typeof obj == 'object') {
            for (const key in obj) {
                let jsonObj = obj[key];
                if (typeof jsonObj == 'object' && jsonObj.length > 0) {

                    for (let i = 0; i < jsonObj.length; i++) {
                        jsonObj[i] = this.checkToJson(jsonObj[i]);
                    }
                } else if (jsonObj && jsonObj.getJson != undefined) {
                    jsonObj = jsonObj.toJson(true);
                }
                obj[key] = jsonObj;
            }

            if (obj.getJson != undefined) {
                obj = obj.toJson();
            }
        }
        return obj;
    }


    constructor(input?: any) {
        if (input) {
            Object.assign(this, input);
        }
    }
    static fromArray<T>(this: new () => T, data: any): Array<T> {
        if (!data){
            return data;
        }
        return data.map((item) => {
            return Object.assign(new this(), item);
        });
    }

}
