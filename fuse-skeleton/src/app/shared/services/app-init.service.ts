import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from './auth/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AppInitService {

    dataStore: {
        settings: any,
        sport_types: any

    }

    private _settings: BehaviorSubject<any>;
    private _sport_types: BehaviorSubject<any>;


    constructor(
        private apollo: Apollo,
        private authService: AuthService
    ) {
        this.dataStore = {
            settings: {},
            sport_types: {}
        }
        this._settings = new BehaviorSubject(this.dataStore.settings);
        this._sport_types = new BehaviorSubject(this.dataStore.sport_types);
    }

    public get settings(): any {
        return this.dataStore.settings;
    }

    public get $settings(): Observable<any> {
        return this._settings.asObservable();
    }

    public set settings(value: any) {
        this.dataStore.settings = value;
        localStorage.setItem('settings', JSON.stringify(this.dataStore.settings));
        this._settings.next(Object.assign({}, this.dataStore).settings);
    }
    public get sport_types(): any {
        return this.dataStore.sport_types;
    }

    public get $sport_types(): Observable<any> {
        return this._sport_types.asObservable();
    }

    public set sport_types(value: any) {
        this.dataStore.sport_types = value;
        localStorage.setItem('sport_types', JSON.stringify(this.dataStore.sport_types));
        this._sport_types.next(Object.assign({}, this.dataStore).sport_types);
    }

    init() {
        return this.apollo.query({
            query: gql`
        query init{
          data : init{
            settings
            sport_types
          }
        }       
      `
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            this.settings = data.settings;
            this.sport_types = data.sport_types;
            return data;
        }).catch((error) => {
            this.settings = {};
            this.authService.user = null;
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

}
