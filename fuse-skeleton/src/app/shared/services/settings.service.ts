import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {

    constructor(
        private apollo: Apollo,
    ) {

    }

    get(request?: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getSettings{
                        data : getSettings
                    }
                `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })

    }

    find(id: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query Settings($id: String!){
                        data : Settings( id : $id){
                            _id
                            name
                            value
                            created_at
                            updated_at
                        }
                    }
                `,
            variables: {
                id
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    add(request: any) {

        return this.apollo.mutate({
            mutation: gql`
                mutation saveSettings($request: JSON!) {
                    data : saveSettings(settings: $request)
                }
            `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    delete(id: string) {
        return this.apollo.mutate({
            mutation: gql`
                mutation deleteSettings($id: String!){
                    data : deleteSettings(id: $id){
                        message
                    }
                }
            `,
            variables: {
                id
            }

        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }


}
