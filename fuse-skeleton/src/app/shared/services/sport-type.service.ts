import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SportTypeService {

    constructor(
        private apollo: Apollo,
    ) {

    }

    get(request: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query sportTypes($request: SportTypeDataTableInput){
                        data : sportTypes(input: $request){
                            meta{
                                total
                            }
                            data {
                                _id
                                name
                                status
                                created_at
                                updated_at
                            }
                        }
                    }
                `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })

    }

    find(id: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query sportType($id: String!){
                        data : sportType( id : $id){
                            _id
                            name
                            status
                            created_at
                            updated_at
                        }
                    }
                `,
            variables: {
                id
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    add(request: any) {


        return this.apollo.mutate({
            mutation: gql`
                mutation addSportType($request: SportTypeInput!){
                    data : addSportType( input: $request){
                        message
                        data
                    }
                }
            `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    delete(id: string) {
        return this.apollo.mutate({
            mutation: gql`
                mutation deleteSportType($id: String!){
                    data : deleteSportType(id: $id){
                        message
                    }
                }
            `,
            variables: {
                id
            }

        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    getAll() {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getSportTypes{
                    data : getSportTypes{
                        _id
                        name
                        status
                        created_at
                        updated_at
                    }
                }
            `
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })

    }


}
