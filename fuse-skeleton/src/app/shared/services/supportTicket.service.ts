import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

export const Fragments = {
    supportTicketData: gql`
        fragment supportTicketData on SupportTicket {
            _id
            user_id
            subject
            status
            user_read
            admin_read
            created_at
            updated_at
        }
    `,
    userData: gql`
        fragment userData on User {
            _id
            first_name
            last_name
            user_type
            profile_pic
        }
    `,
    supportTicketMsgData: gql`
        fragment supportTicketMsgData on SupportTicketMsg {
            _id
            user_id
            message
            created_at
        }
    `,
};

@Injectable({
    providedIn: 'root'
})
export class SupportTicketService {

    constructor(
        private apollo: Apollo,
    ) {
    }

    getList(request: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getSupportTicketList($request: SupportTicketDataTableInput){
                        data : getSupportTicketList(input: $request){
                            meta{
                                total
                                last_page
                                current_page
                            }
                            data {
                                ...supportTicketData
                                user{
                                    ...userData
                                }
                            }
                        }
                    }
                    ${Fragments.supportTicketData}
                    ${Fragments.userData}
                `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })

    }

    getSupportTicket(id){
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getSupportTicket($id: String!){
                        data : getSupportTicket( id : $id){
                            ...supportTicketData
                            closed_by_user{
                                ...userData
                            }
                            user{
                                ...userData
                            }
                            messages{
                                ...supportTicketMsgData
                                user{
                                    ...userData
                                }
                                file{
                                    file
                                }
                            }
                        }
                    }
                    ${Fragments.supportTicketData}
                    ${Fragments.userData}
                    ${Fragments.supportTicketMsgData}
                `,
            variables: {
                id
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })

    }

    addReplyToMsg(request: any){
        return this.apollo.mutate({
            mutation: gql`
                mutation replySupportTicket($request:ReplySupportTicketInput!){
                    data : replySupportTicket( input : $request){
                        ...supportTicketMsgData
                        user{
                            ...userData
                        }
                        file{
                            file
                        }
                        
                    }
                }
                ${Fragments.userData}
                ${Fragments.supportTicketMsgData}
            `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getSupportTicketTotal(){
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getSupportTicketTotal{
                        data : getSupportTicketTotal{
                           open
                           closed
                           unread
                        }
                    }
                `,
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }
    changeStatus(request){
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query changeStatus($request: ChangeStatusInput){
                        data : changeStatus( input : $request){
                            ...supportTicketData
                            closed_by_user{
                                ...userData
                            }
                            user{
                                ...userData
                            }
                            messages{
                                ...supportTicketMsgData
                                user{
                                    ...userData
                                }
                                file{
                                    file
                                }
                            }
                        }
                    }
                    ${Fragments.userData}
                    ${Fragments.supportTicketMsgData}
                    ${Fragments.supportTicketData}
                `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }
}