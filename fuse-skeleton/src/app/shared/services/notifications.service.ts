import { Injectable } from '@angular/core';
import { apiUrl } from '../utils';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(
    private http: HttpClient
  ) { }

  getNotifications(params) {
    return this.http.get(apiUrl('notification'), { params }).toPromise();
  }

  getNotification(id, params?: any) {
    return this.http.get(apiUrl('notification/' + id), { params }).toPromise();
  }
}
