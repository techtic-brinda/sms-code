import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class AnalyticsService {
    
    constructor(
        private apollo: Apollo,
    ) {

    }
    
    getAdminProfit(year) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getAdminProfit($year: Int!){
                        data : getAdminProfit(year: $year){
                            data
                        }
                    }
                `,
            variables: {
                year
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getGameWisePlayerStatistics() {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getGameWisePlayerStatistics{
                        data : getGameWisePlayerStatistics{
                            data
                        }
                    }
                `,
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getGameWisePostStatistics() {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getGameWisePostStatistics{
                        data : getGameWisePostStatistics{
                            data
                        }
                    }
                `,
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getStatusWiseUsers(){
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getStatusWiseUsers{
                        data : getStatusWiseUsers{
                            data
                        }
                    }
                `,
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }
}