import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiUrl } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class PageService {
  
  constructor(
    private http: HttpClient
  ) { }


  getPages(params) {
    return this.http.get(apiUrl('page'), { params }).toPromise();
  }

  getPage(id, params?: any) {
    return this.http.get(apiUrl('page/' + id), { params }).toPromise();
  }

}
