import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthService
    ) { }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let return_data: any = await new Promise((resolve, rject) => {
            this.authService.$user.subscribe((user) => {
                if (user && user.user_type == "admin") {
                    resolve(true)
                } else {
                    resolve(false)
                }
            })
        })

        if (!return_data) {
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        }

        return return_data;
    }
}