import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';


export const Fragments = {
    PostData: gql`
        fragment PostData on Post {
            _id
            user_id
            title
            description
            sport_type_id
            total_staked_amount
            total_won_amount
            game_amount
            player_amount
            total_get_amount
            closing_time
            stream_type
            stream_url
            is_streaming
            sold_percentage
            sold_total
            status
            paid_to_player
            gap_amount
            created_at
            updated_at
            markup_amount
        }
    `,
};


@Injectable({
    providedIn: 'root'
})
export class PostService {

    constructor(
        private apollo: Apollo,
    ) {

    }
    getAllPostList(){
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getAllPostList{
                        data : getAllPostList{
                            _id
                           title
                        }
                    }
                `,
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getAll(request: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getAllPost($request:PostsDataTableInput){
                        data : getAllPost( input : $request){
                            meta {
                                total
                            }
                            data {
                                ...PostData
                                user {
                                    first_name
                                    last_name
                                }
                                stack_users{
                                    _id
                                    user_id
                                }
                            }
                        }
                    }
                    ${Fragments.PostData}
                `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getOne(id: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getPost($id:String!){
                        data : getPost( id : $id){
                            ...PostData
                            stack_users{
                                staked_amount
                                won_amount
                                created_at
                                user {
                                    profile_pic
                                    first_name
                                    last_name
                                }
                            }
                            user {
                                profile_pic
                                first_name
                                last_name
                                about
                            }
                        }
                    }
                    ${Fragments.PostData}
                `,
            variables: {
                id
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    delete(id: string) {
        return this.apollo.mutate({
            mutation: gql`
                mutation deletePost($id:String!){
                    data : deletePost(id : $id){
                        message
                    }
                }
            `,
            variables: {
                id
            }

        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    add(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation addPost($request:PostsInput!){
                    data : addPost( input : $request){
                        message
                        data{
                            ...PostData
                            user {
                                first_name
                                last_name
                            }
                        }
                    }
                }
                ${Fragments.PostData}
            `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    updatePostResult(request: any){
        return this.apollo.mutate({
            mutation: gql`
                mutation updatePostResult($request: PostResultInput!){
                    data : updatePostResult( input : $request){
                        message
                    }
                }
            `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    paidToPlayer(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation paidToPlayer($request:PaidToPlayerInput!){
                    data : paidToPlayer( input : $request){
                        message
                        data
                    }
                }
            `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }
    
    paidUserData(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation refundToUser($request:PaidToPlayerInput!){
                    data : refundToUser( input : $request){
                        message
                        data
                    }
                }
            `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getUserStack(filters?: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getMyPost($input: GetPostsInput){
                    data : getMyPost(input : $input){
                        data{
                            ... PostData
                            stack_users {
                                won_amount
                                staked_amount
                                created_at
                                user {
                                    first_name
                                    last_name
                                    _id
                                }
                            }
                        }
                        meta{
                            total
                            last_page
                            current_page
                        }
                    }
                }
                ${Fragments.PostData}
            `,
            variables: {
                input: filters
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getBuyerPost(request) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getBuyerPosts($input: GetPostsInput){
                    data : getBuyerPosts(input : $input){
                        data{
                            ... PostData
                            player_staked_amount
                            player_won_amount
                            stack_users {
                                staked_amount
                                created_at
                            }
                        }
                        meta{
                            total
                            last_page
                            current_page
                        }
                    }
                }
                ${Fragments.PostData}
                `,
            variables: {
                input: request
            }
        }).pipe(
            map((resp: any) => resp.data.data),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }


}
