import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';

export const Fragments = {
    FaqData: gql`
        fragment FaqData on Faq {
            _id
            question
            answer
            created_at
            updated_at
        }
    `,
    PageData: gql`
        fragment PageData on Pages {
            _id
            name
            title
            slug
            content
            meta_title
            meta_description
            meta_keyword
            created_at
            updated_at
        }
    `,
    TutorialData : gql`
        fragment TutorialData on Tutorial {
            _id
            title
            description
            file
            link
            created_at
            updated_at
        }
    `
};


@Injectable({
    providedIn: 'root'
})
export class CmsService {

    constructor(
        private apollo: Apollo,
    ) {
    }

    getAllFaq(request: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getFaq($request:FaqDataTableInput){
                        data : getFaq( input : $request){
                            meta {
                                total
                            }
                            data {
                            ...FaqData
                            }
                        }
                    }
                    ${Fragments.FaqData}
                `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    deleteFaq(id: string){
        return this.apollo.mutate({
            mutation: gql`
                mutation deleteFaq($id:String!){
                    data : deleteFaq(id : $id){
                        message
                    }
                }
            `,
            variables: {
                id
            }

        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    addFaq(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation saveFaq($request: FaqInput!){
                    data : saveFaq( input: $request){
                        message
                        data
                    }
                }
            `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getAllPage(request: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getPages($request:PagesDataTableInput){
                        data : getPages( input : $request){
                            meta {
                                total
                            }
                            data {
                            ...PageData
                            }
                        }
                    }
                    ${Fragments.PageData}
                `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    deletePage(id: string) {
        return this.apollo.mutate({
            mutation: gql`
                mutation deletePage($id:String!){
                    data : deletePage(id : $id){
                        message
                    }
                }
            `,
            variables: {
                id
            }

        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    addPage(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation savePage($request: PagesInput!){
                    data : savePage( input: $request){
                        message
                        data
                    }
                }
            `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getPage(id: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getPage($id:String!){
                        data : getPage( id : $id){
                            ...PageData
                        }
                    }
                    ${Fragments.PageData}
                `,
            variables: {
                id
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getAllTutorials(request: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getTutorials($request:TutorialDataTableInput){
                        data : getTutorials( input : $request){
                            meta {
                                total
                            }
                            data {
                            ...TutorialData
                            }
                        }
                    }
                    ${Fragments.TutorialData}
                `,
            variables: {
                request
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    deleteTutorial(id: string) {
        return this.apollo.mutate({
            mutation: gql`
                mutation deleteTutorial($id:String!){
                    data : deleteTutorial(id : $id){
                        message
                    }
                }
            `,
            variables: {
                id
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getTutorial(id: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getTutorial($id:String!){
                        data : getTutorial( id : $id){
                            ...TutorialData
                        }
                    }
                    ${Fragments.TutorialData}
                `,
            variables: {
                id
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    addTutorial(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation saveTutorial($request: TutorialInput!){
                    data : saveTutorial( input: $request){
                        message
                        data
                    }
                }
            `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }


}