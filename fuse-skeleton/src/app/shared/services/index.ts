import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth/auth.interceptor';
import { NotificationsService } from './notifications.service';
import { PageService } from './page.service';
import { AuthService } from './auth/auth.service';
import { UserService } from './user.service';
import { NavService } from './nav.service';
import { HelperService } from './helper.service';
import { PostService } from './post.service';
import { NotificationTemplateService } from './notification-template.service';
import { EmailTemplateService } from './email-template.service';
import { SupportTicketService } from './supportTicket.service';
import { CmsService } from './cms.service';
import { AnalyticsService } from './analytics.service';

const Services = [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    UserService,
    NotificationsService,
    PageService,
    AuthService,
    NavService,
    HelperService,
    PostService,
    NotificationTemplateService,
    EmailTemplateService,
    CmsService,
    SupportTicketService,
    AnalyticsService
];

export { Services };
