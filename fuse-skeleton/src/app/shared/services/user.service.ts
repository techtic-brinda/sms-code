import { Injectable } from '@angular/core';
import { throwError, Subject } from 'rxjs';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import * as _ from "underscore";
import * as moment from "moment";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    onFilterChanged: Subject<any>;
    filterBy: string = 'all';

    constructor(
        private apollo: Apollo,
    ) {
        this.onFilterChanged = new Subject();
    }

    getUsers(request: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query users($request:UsersDataTableInput){
                        data : users( input : $request){
                            meta{
                                total
                            }
                            data {
                                _id
                                first_name
                                last_name
                                email
                                email_verified_at
                                gender
                                dob
                                status
                                about
                                coaching_amount
                                profile_pic
                                user_type
                                created_at
                                updated_at
                            }
                        }
                    }
                `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })

    }
    getUsersAll(){
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query users{
                        data : users{
                            data {
                                _id
                                first_name
                                last_name
                                email
                                email_verified_at
                                gender
                                dob
                                status
                                profile_pic
                                user_type
                                created_at
                                updated_at
                            }
                        }
                    }
                `,
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })

    }

    getUser(id: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query user($id:String!){
                        data : user( id : $id){
                            _id
                            first_name
                            last_name
                            email
                            email_verified_at
                            gender
                            dob
                            status
                            profile_pic
                            user_type
                            created_at
                            updated_at
                            wallet_amount
                            coaching_amount
                            about
                        }
                    }
                `,
            variables: {
                id
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    deleteUser(id: string) {
        return this.apollo.mutate({
            mutation: gql`
                mutation deleteUser($id:String!){
                    data : deleteUser(id : $id){
                        message
                    }
                }
            `,
            variables: {
                id
            }

        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    addUser(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation addUser($request: UserInput!){
                    data : addUser( input : $request){
                        message
                        data
                    }
                }
            `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    getPlayers(user_type?:any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getPlayers($user_type: String){
                        data : getPlayers(user_type:$user_type){
                            _id
                            first_name
                            last_name
                        }
                    }
                `,
                variables: {
                    user_type
                }
        }).pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    changePassword(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation changePassword(
                    $old_password: String!,
                    $password: String!
                ){
                    data : changePassword(
                        old_password : $old_password
                        password : $password
                    ){
                        message
                    }
                }
            `,
            variables: {
                ...request
            }
        })
            .pipe(
                map((resp: any) => resp.data.data),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    getEvent({ start, end, user_id }) {

        return this.apollo.query({
            fetchPolicy: "no-cache",
            query: gql`
                query getAvailability($input: GetAvailabilityInput!){
                    data : getAvailability( input : $input){
                        id
                        start
                        end
                        status
                        color
                        amount
                        booking_user{
                            _id
                            first_name
                            last_name
                            profile_pic
                        }
                    }
                }
            `,
            variables: {
                input: { start, end, user_id }
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getUserAvailability({ start, end, user_id }) {

        return this.apollo.query({
            fetchPolicy: "no-cache",
            query: gql`
                query getUserAvailability($input: GetAvailabilityInput!){
                    data : getUserAvailability( input : $input){
                        id
                        start
                        end
                        status
                        color
                        amount
                        booking_user{
                            _id
                            first_name
                            last_name
                            profile_pic
                        }
                        user{
                            _id
                            first_name
                            last_name
                            profile_pic
                        }
                    }
                }
            `,
            variables: {
                input: { start, end, user_id }
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getRequestList(request) {
        return this.apollo.query({
            fetchPolicy: "no-cache",
            query: gql`
                query getBookingRequestInput($input: GetBookingRequestInput){
                    data : getBookingRequestInput( input : $input){
                        data {
                            id
                            start
                            end
                            status
                            color
                            amount
                            booking_user{
                                _id
                                first_name
                                last_name
                                profile_pic
                            }
                            user{
                                _id
                                first_name
                                last_name
                                profile_pic
                            }
                        }
                        meta{
                            from
                            to
                            total
                            per_page
                            current_page
                            last_page
                        }
                    }
                }
            `,
            variables: {
                input: request
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }
    getStatistics(user_id) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getStatistics($user_id: String){
                    data : getStatistics(user_id : $user_id){
                        investment
                        amount
                        totalWithdraw
                        availbleBalance
                    }
                }
            `,
            variables: {
                user_id: user_id
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }
    getStakeStatistics(user_id){
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getStakeStatistics($user_id: String){
                    data : getStakeStatistics(user_id : $user_id){
                        active
                        pending_result
                        won
                        loss
                        inactive
                    }
                }
            `,
            variables: {
                user_id: user_id
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }
    withdrawPlayer(request){
        return this.apollo.mutate({
            fetchPolicy: "no-cache",
            mutation: gql`
                mutation withdrawPlayer($input: WithdrawPlayerInput!){
                    data : withdrawPlayer( input : $input){
                        message
                        data
                    }
                }
            `,
            variables: {
                input: request
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    sendUserRefund(availabityId){
        console.log(availabityId);
        return this.apollo.mutate({
            fetchPolicy: "no-cache",
            mutation: gql`
                mutation refundToUserCoaching($availabityId: String!){
                    data : refundToUserCoaching( availabityId : $availabityId){
                        message
                        data
                    }
                }
            `,
            variables: {
                availabityId: availabityId
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }
    followingList(request) {
        console.log(request);
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query followingList($input: UserFollowDataTableInput){
                    data : followingList(input : $input){
                        data{
                            _id
                            user_id
                            player_id
                            created_at
                            updated_at
                            user{
                                first_name
                                last_name
                                profile_pic
                            }
                            player{
                                first_name
                                last_name
                                profile_pic
                            }
                        }
                        meta{
                            total
                            last_page
                            current_page
                        }
                    }
                }
            `,
            variables: {
                input: request
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

}
