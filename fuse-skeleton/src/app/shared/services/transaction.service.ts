import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';

export const Fragments = {
    PaymentData: gql`
        fragment PaymentData on Transaction {
            _id
            user_id
            amount
            closing_balance
            description
            transactions_type
            data
            getway_transactions_id
            getway_type
            status
            created_at
        }
    `,
};

@Injectable({
    providedIn: 'root'
})
export class TransactionService {
    constructor(
        private apollo: Apollo,
    ) {

    }
    
    getAll(request: any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                    query getAllPaymentHistory($request: TransactionDataTableInput){
                        data : getAllPaymentHistory(input: $request){
                            meta{
                                total
                            }
                            data {
                                ...PaymentData
                                user {
                                    first_name
                                    last_name
                                }
                            }
                        }
                    }
                    ${Fragments.PaymentData}
                `,
            variables: {
                request
            }
        })
            .pipe(
                map((resp: any) => {
                    return resp.data.data;
                }),
                catchError((error) => {
                    return throwError(JSON.parse(JSON.stringify(error)));
                })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })

    }

    getAllInvoice(request) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getAllInvoices($input: InvoiceDataTableInput){
                    data : getAllInvoices(input : $input){
                        data{
                            _id
                            post {
                                title
                                description
                            }
                            user{
                                first_name
                                last_name
                            }
                            file_name
                            post_id
                            user_id
                            file
                            amount
                            created_at
                        }
                        meta{
                            total
                            last_page
                            current_page
                        }
                    }
                }
                `,
            variables: {
                input: request
            }
        }).pipe(
            map((resp: any) => resp.data.data),
            catchError((error) => {
                return throwError(JSON.parse(JSON.stringify(error)));
            })
        )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }
}