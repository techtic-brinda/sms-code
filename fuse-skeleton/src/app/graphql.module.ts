import { NgModule } from '@angular/core';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink, concat } from 'apollo-link';
import { environment } from 'environments/environment';

const middleware = new ApolloLink((operation, forward) => {
  const token = localStorage.getItem('token');
 
  if (token) {

    operation.setContext(({ headers = {} }) => ({
      headers: {
        ...headers,
        Authorization: `Bearer ${token}`,
      }
    }));
  }
  return forward(operation);
});

export function createApollo(httpLink: HttpLink) {
  const uri = environment.graphQlUrl + '/graphql';
  const http = httpLink.create({ uri });
  return {
    link: concat(middleware, http),
    cache: new InMemoryCache(),
  };
}

@NgModule({
  exports: [ApolloModule, HttpLinkModule],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink],
    },
  ],
})
export class GraphQLModule {

}
