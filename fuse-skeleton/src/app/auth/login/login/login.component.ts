import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/shared/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loginResponse: any;
  showPassword: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {

  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  doLogin() {
    if (this.loginForm.valid) {
      let request: any = {
        ...this.loginForm.value,
        user_type: 'admin'
      }

      this.authService.login(request)
        .then((_res) => {
          this.router.navigate(['/']);
        }).catch((err) => {
          this.loginResponse = err;
        });
    }
  }
  showPass(){
    if (this.showPassword == false){
      this.showPassword = true;
    }else{
      this.showPassword = false;
    }
  }

}
