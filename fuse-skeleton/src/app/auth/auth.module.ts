import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { ContentModule } from 'app/layout/components/content/content.module';


@NgModule({
  declarations: [LayoutComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FuseSharedModule,
    ContentModule
  ]
})
export class AuthModule { }
