import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './layout/users.component';
import { DetailsComponent } from './details/details.component';


const routes: Routes = [
  { path: "", component: UsersComponent },
  { path: ":id", component: DetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
