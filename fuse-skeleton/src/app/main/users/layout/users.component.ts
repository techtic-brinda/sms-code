import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild, TemplateRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject, from } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { UserService } from 'app/shared/services/user.service';
import { UsersUserFormDialogComponent } from '../user-form/user-form.component';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { DatatableComponent } from 'app/shared/modules/datatable/datatable/datatable.component';

@Component({
    selector: 'users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class UsersComponent implements OnInit, OnDestroy {
    dialogRef: any;
    hasSelectedUsers: boolean;
    searchInput: string;
    @ViewChild('dialogContent', { static: false }) dialogContent: TemplateRef<any>;
    @ViewChild('datatable', { static: false }) datatable: DatatableComponent;
    users: any;
    user: any;
    displayedColumns = ['checkbox', 'avatar', 'name', 'email', 'phone', 'jobTitle', 'buttons'];
    selectedUsers: any[];
    checkboxes: {};
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    private _unsubscribeAll: Subject<any>;
    private _filterBy: string = "all";
   

    constructor(
        private userService: UserService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog
    ) {
        this._unsubscribeAll = new Subject();
    }

    public get filterBy(): string {
        return this._filterBy;
    }
    public set filterBy(value: string) {
        console.log(value);
        
        this._filterBy = value;
        this.datatable.refresh();
    }

    ngOnInit(): void {
       
    }

    dataSource = (request: any) => {
        if (this.filterBy && this.filterBy != 'all') {
            request.user_type = this.filterBy.toLowerCase()
        }
        return from(this.userService.getUsers(request));
    }


    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    newUser(): void {
        this.dialogRef = this._matDialog.open(UsersUserFormDialogComponent, {
            panelClass: ['fix-left',  'user-form-dialog'],
            data: {
                action: 'new',
                user : {user_type : this.filterBy.toLowerCase()}
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                console.log(response,'response');
                if (!response) {
                    return;
                }
                this.datatable.refresh();
              // this.userService.addUser(response.getRawValue());
            });
    }

    editUser(user): void {
        console.log(user);
        
        this.dialogRef = this._matDialog.open(UsersUserFormDialogComponent, {
            panelClass: ['fix-left', 'user-form-dialog'],
            data: {
                user: user,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                switch (response) {
                    case 'save':
                        this.datatable.refresh();
                        break;
                    case 'delete':
                        this.deleteUser(user);
                        break;
                }
            });
    }

    deleteUser(user_id): void {
        this.userService.deleteUser(user_id).then(()=>{
            this.datatable.refresh();
        });
    }


    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}
