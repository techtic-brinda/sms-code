import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserService } from 'app/shared/services/user.service';
import * as _ from 'underscore';

@Component({
    selector: 'users-user-form-dialog',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class UsersUserFormDialogComponent {
    action: string;
    user: any;
    userForm: FormGroup;
    dialogTitle: string;
    isPassword:boolean = true;
    tomorrow = new Date();
    loading = false;
    ErrorResponse:any;

    constructor(
        public userService: UserService,
        public matDialogRef: MatDialogRef<UsersUserFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) _data: any,
    ) {
        this.action = _data.action;

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit User';
            this.user = Object.assign({}, _data.user);
        }else {
            this.dialogTitle = 'New User';
            this.user = Object.assign({ status: 'active' }, _data.user || {});
            if(this.user.user_type == 'all'){
                this.user.user_type = 'admin';
            }
        }
        this.tomorrow.setDate(this.tomorrow.getDate() - 1);
    }

  
    submit(form: NgForm): void {
        form.ngSubmit.emit();
    }
    
    deleteUser(user_id): void {
        this.userService.deleteUser(user_id);
        this.matDialogRef.close('delete');
    }

    addUpdateUser(f:NgForm): void {
        this.ErrorResponse = '';
        let user = _.pick(this.user, ["_id", "password", "first_name", "last_name", "email", "sport_type_id", "user_type", "password", "dob", "profile_pic", "about", "coaching_amount", "gender", "status"]);
        if(f.valid){
            this.loading = true;
            this.userService.addUser(user).then(()=>{
                this.loading = false;
                this.matDialogRef.close('save');
            }).catch((err)=>{
                this.loading = false;
                this.ErrorResponse = err.message;
            });
        }
    }
}
