import { Component, OnInit, ViewEncapsulation, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { UserService } from 'app/shared/services/user.service';
import { fuseAnimations } from '@fuse/animations';
import { HelperService } from 'app/shared/services/helper.service';
import { FullCalendarComponent } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { MatDialog } from '@angular/material';
import { PostService } from 'app/shared/services/post.service';
import { TransactionService } from 'app/shared/services/transaction.service';
import { from } from 'rxjs';
import { DatatableComponent } from 'app/shared/modules/datatable/datatable/datatable.component';

@Component({
  selector: 'app-user-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class DetailsComponent implements OnInit {
  loading: boolean;
  user: any = {};
  currentUser: any = {};
  statistics_list: any = [];
  stake_statistics_list: any = [];
  //transation_list: any = {};
  selectedEvent: any = null;
  viewDate: Date = new Date();
  @ViewChild('calendar', { static: false, read: FullCalendarComponent }) DetailsComponent: any;
  @ViewChild("infoDialog", { static: false }) infoDialog: TemplateRef<any>;
  @ViewChild("transactionInfoDialog", { static: false }) transactionInfoDialog: TemplateRef<any>;
  @ViewChild("paidInfoDialog", { static: false }) paidInfoDialog: TemplateRef<any>;
  @ViewChild('transationDatatable', { static: false }) transationDatatable: DatatableComponent;
  @ViewChild('followingDatatable', { static: false }) followingDatatable: DatatableComponent;

  calendarPlugins = [dayGridPlugin, timeGridPlugin, interactionPlugin];

  constructor(
    public usersService: UserService,
    public postService: PostService,
    public transactionService: TransactionService,
    public activatedRoute: ActivatedRoute,
    public helper: HelperService,
    private _matDialog: MatDialog,
    private router: Router

  ) {  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: any) => {
      if (params.id) {
        this.loading = true;
        this.usersService.getUser(params.id).then(user => {
          this.loading = false;
          this.user = user;
          if(this.user.user_type != 'admin'){
            //this.getEvents();
            if (this.DetailsComponent){
              this.DetailsComponent.calendar.refetchEvents();
            }
            this.getStatistics();
            this.getStakeStatistics();
          } 
        }).catch((error) => {
          this.loading = false;
          this.router.navigate(['/users']);
          this.helper.errorMessage({}, "You can not open the page.");
        });
      }
    })
    
  }
  getStatistics(){
    this.usersService.getStatistics(this.user._id).then(data => {
      this.statistics_list = data;
    }).catch(error => {
      this.helper.errorMessage(error, "You can not open the page.");
    });
  }
  getStakeStatistics() {
    this.usersService.getStakeStatistics(this.user._id).then(data => {
      this.stake_statistics_list = data;
    }).catch(error => {
      this.helper.errorMessage(error, "You can not open the page.");
    });
  }
  events = ({ start, end }) => {
    let event;
    if (this.user.user_type == "user") {
      event = this.usersService.getUserAvailability({ start, end, user_id: this.user._id });
    } else {
      event = this.usersService.getEvent({ start, end, user_id: this.user._id });
    }
    return event;
  }

  
  openInfo(data) {
    let { event } = data;
    this.selectedEvent = event;
    this.selectedEvent.currentUserId = this.user._id;
    this._matDialog.closeAll();
    const dialogRef = this._matDialog.open(this.infoDialog, {
      panelClass: ['fix-left', 'form-dialog'],
      data: this.selectedEvent
    });
  }
  stakeDataSource = (request: any) => {
    request.user_id = this.user._id;
    if (this.user.user_type == "user"){
      return from(this.postService.getBuyerPost(request));
    }else{
      return from(this.postService.getUserStack(request));
    }
  }
  transactionDataSource = (request: any) => {
    request.user_id = this.user._id;
    return from(this.transactionService.getAll(request));
  }

  invoiceDataSource = (request: any) =>{
    request.user_id = this.user._id;
    return from(this.transactionService.getAllInvoice(request));
  }

  followingDataSource = (request: any) => {
    console.log('followingDataSource');
    if (this.user.user_type=='user'){
      request.user_id = this.user._id;
    }else if(this.user.user_type=='player'){
      request.player_id = this.user._id;
    }else{
      return false;
    }
    let data = from(this.usersService.followingList(request));
    console.log(data);
    return data;
  }

  changeTab(event){
    if (event.tab.textLabel == "Availability" || event.tab.textLabel == "Booked Coching"){
      window.dispatchEvent(new Event('resize'));
    }
  }
  transctionInfoBox(data){
    data.obj_data = JSON.parse(data.data);
    this._matDialog.closeAll();
    const dialogRef = this._matDialog.open(this.transactionInfoDialog, {
      panelClass: ['fix-left', 'form-dialog'],
      data: data
    });
  }

  downloadFile(file) {
    var file_path = file;
    let a = document.createElement('a');
    a.href = file_path;
    a.target = '_blank';
    a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  paidPlayerData(item){
    item.withdraw_amount = null;
    this._matDialog.closeAll();
    const dialogRef = this._matDialog.open(this.paidInfoDialog, {
      panelClass: ['fix-left', 'form-dialog'],
      data: item
    });
  }
  playerFormData(item){
    if (parseFloat(item.wallet_amount) <= parseFloat(item.withdraw_amount)){
      this.helper.errorMessage({}, "You can't withdraw more then wallet amount.");
      return false;
    }
    let request = {
      amount: parseFloat(item.withdraw_amount),
      user_id : item._id
    }
    this.usersService.withdrawPlayer(request).then(data => {
      this._matDialog.closeAll();
      this.transationDatatable.refresh();
      this.getStatistics();
      this.user.wallet_amount -= parseFloat(item.withdraw_amount);
      this.helper.successMessage(data, "Amount is withdraw successfully.");
    }).catch(error => {
      this.helper.errorMessage(error, "some error occured. Please try after sometime.");
    });
    

  }
}
