import { Component, OnInit, ViewChild, ViewEncapsulation, TemplateRef } from '@angular/core';
import { from } from 'rxjs';
import * as _ from 'underscore';
import { NgForm } from '@angular/forms';
import { HelperService } from 'app/shared/services/helper.service';
import { CmsService } from 'app/shared/services/cms.service';
import { DatatableComponent } from 'app/shared/modules/datatable/datatable/datatable.component';
import { MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'app-faq',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations 
})
export class ListComponent implements OnInit {
  faqRequest: any = {};
  faqForm: any = {};
  step: number;
  expanded: boolean;
  searchInput: string;
  @ViewChild('datatable', { static: false }) datatable: DatatableComponent;
  isLoading: boolean = false;
  @ViewChild("faqDialog", { static: false }) faqDialog: TemplateRef<any>;


  constructor(
    public CmsService: CmsService,
    private helper: HelperService,
    private _matDialog: MatDialog
  ) { }

  ngOnInit() {
  }

  newFaq(faq, title){
    this.faqForm = Object.assign({},faq);
    this._matDialog.closeAll();
    const dialogRef = this._matDialog.open(this.faqDialog, {
      panelClass: ['fix-left', 'form-dialog'],
      data: {
        faq : this.faqForm,
        title: title
      }
    });
  }

 
  addUpdateFaq(form: NgForm): void {
    if (form.invalid) {
      return;
    }
    
    this.isLoading = true;

    let request = _.pick(form.value, ["_id", "question", "answer"]);
    request._id = this.faqForm._id;
    this.CmsService.addFaq(request).then((data) => {
      this.completeAction();
      this.helper.successMessage(data, `FAQ successfully ${request._id ? 'updated' : 'added'}.`);
    }).catch((error) => {
      this.completeAction();
      this.helper.errorMessage(error, `Error, while ${request._id ? 'updating' : 'adding'} FAQ.`);
    })
  }
  completeAction() {
    this.isLoading = false;
    this.datatable.refresh();
    this._matDialog.closeAll();
  }
  faqList =  (request: any) => {
    return from(this.CmsService.getAllFaq(request));
  }

  deleteFaq(id: string){
    this.CmsService.deleteFaq(id).then((data) => {
      this.datatable.refresh();
      this.helper.successMessage(data);
    }).catch((error) => {
      this.helper.errorMessage(error);
    });
  }

  submit(form: NgForm): void {
    form.ngSubmit.emit();
  }
}
