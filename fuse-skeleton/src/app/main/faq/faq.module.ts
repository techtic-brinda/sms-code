import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqRoutingModule } from './faq-routing.modules';
import { ListComponent } from './list/list.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    FaqRoutingModule,
    FuseSharedModule,
    FuseConfirmDialogModule
  ]
})
export class FaqModule { }
