import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { ProfileService } from 'app/main/profile/profile.service';
import { AuthService } from 'app/shared/services/auth/auth.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from 'app/shared/services/user.service';
import { HelperService } from 'app/shared/services/helper.service';

@Component({
    selector: 'profile-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProfileAboutComponent implements OnInit, OnDestroy {

    user: any;
    is_edit = false;
    userForm: FormGroup;
    rateHttpResponse: any;
    private _unsubscribeAll: Subject<any>;

    
    constructor(
        private userService: UserService,
        private authService: AuthService,
        private _formBuilder: FormBuilder,
        private helperService: HelperService,
    ) {

        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.authService.$user
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((user) => {
                this.user = user;
                this.userForm = this.createContactForm();
            })
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    toggleEditView() {
        this.is_edit = (this.is_edit == false) ? true : false;
    }

    createContactForm(): FormGroup {
        return this._formBuilder.group({
            _id: [this.user._id],
            first_name: [this.user.first_name],
            last_name: [this.user.last_name],
            profile_pic: [this.user.profile_pic],
            email: [this.user.email],
            dob: [this.user.dob],
            gender: [this.user.gender],
        });
    }

    setProfilePic($event) {
        if ($event.target.files.length > 0) {
            var fileToLoad = $event.target.files[0];
            var reader: FileReader = new FileReader();
            reader.onloadend =  (readerEvt: any) => {
                this.user.profile_pic = readerEvt.target.result;
            }
            reader.readAsDataURL(fileToLoad);
            this.user.profile_pic = $event.target.files;
        }
    }

    editUser() {
        this.rateHttpResponse = null
        if (this.userForm.valid) {
            this.userForm.value.profile_pic = this.user.profile_pic;
            this.userService.addUser(this.userForm.value).then((data) => {
                this.rateHttpResponse = data;
                this.authService.user = Object.assign(this.authService.user, this.userForm.value);
                this.is_edit = false;
                this.helperService.successMessage(data, 'Profile successfully updated.');
            }).catch((error) => {
                this.rateHttpResponse = error;
            });
        }

    }
}

