import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { ProfileService } from '../../profile.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NgForm, FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { HelperService } from 'app/shared/services/helper.service';
import { UserService } from 'app/shared/services/user.service';
import { AuthService } from 'app/shared/services/auth/auth.service';


@Component({
    selector: 'change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
    timeline: any;
    changePasswordForm: FormGroup;
    changePasswordHttpResponse: any;

    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProfileService} _profileService
     */
    constructor(
        private _profileService: ProfileService,
        private _formBuilder: FormBuilder,
        private helperService: HelperService,
        private userService: UserService,
        private authService: AuthService,
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this.changePasswordForm = this.createContactForm();
    }


    ngOnInit(): void {
        this._profileService.timelineOnChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(timeline => {
                this.timeline = timeline;
            });
    }

    createContactForm(): FormGroup {
        return this._formBuilder.group({
            'old_password': [null, Validators.required],
            'new_password': [null, Validators.required],
            'confirm_new_password': [null, [Validators.required, this.passwordMatch]]
        });
    }
    passwordMatch(control: AbstractControl) {
        let paswd = control.root.get('new_password');
        if (paswd && control.value != paswd.value) {
            return {
                passwordMatch: true
            };
        }
        return null;
    }
    ngOnDestroy(): void {

        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    doChangePassword() {
        this.changePasswordHttpResponse = null;
        if (this.changePasswordForm.valid) {
            this.userService.changePassword({
                old_password: this.changePasswordForm.value.old_password,
                password: this.changePasswordForm.value.new_password
            }).then((data) => {
                this.changePasswordHttpResponse = data;
                this.authService.user = Object.assign(this.authService.user, data.data);
                this.helperService.successMessage(data, 'Your password successfully changed.');
            }).catch((error) => {
                this.changePasswordHttpResponse = error;
            });
        }
    }
}
