import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationTemplateRoutingModule } from './notification-template-routing.module';
import { ListComponent } from './list/list.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

@NgModule({
  declarations: [
    ListComponent,
  ],
  imports: [
    CommonModule,
    NotificationTemplateRoutingModule,
    FuseSharedModule,
    FuseConfirmDialogModule
  ]
})
export class NotificationTemplateModule { }
