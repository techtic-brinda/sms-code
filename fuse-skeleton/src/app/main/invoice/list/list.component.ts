import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { TransactionService } from 'app/shared/services/transaction.service';
import { Subject, from, Observable } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute } from '@angular/router';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { UserService } from 'app/shared/services/user.service';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { DatatableComponent } from 'app/shared/modules/datatable/datatable/datatable.component';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { PostService } from 'app/shared/services/post.service';
import * as moment from 'moment';

@Component({
  selector: 'app-invoice',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ListComponent implements OnInit {
  loading: boolean = false;
  disabled: boolean = true;
  request: any = {};
  searchInput: string;
  user_id: any;
  post_id: any;
  userList: any = [];
  postList: any = [];
  minDate: Date;


  myControl = new FormControl();
  filteredOptions: Observable<{}>;
  
  postControl = new FormControl();
  postFilteredOptions: Observable<{}>;
  @ViewChild('datatable', { static: false }) datatable: DatatableComponent;
  
  constructor(
    private transactionService: TransactionService,
    private userService: UserService,
    private postService: PostService,
    public activatedRoute: ActivatedRoute,
  ) {
    // this.activatedRoute.params.subscribe((params: any) => {
    //   if (params.user_id) {
    //     this.request.user_id = params.user_id;
    //   }
    // })
  }

  ngOnInit() {
    this.getUsersAll();
    this.getAllPostList();
  }

  private _filter(name: string) {
    const filterValue = name.toLowerCase();

    return this.userList.data.filter(option => {
      return option.first_name.toLowerCase().indexOf(filterValue) === 0 || option.last_name.toLowerCase().indexOf(filterValue) === 0;
    });
  }
  getSelectedUser(value) {
    this.user_id = value._id;
    this.request.page = 0;
    this.disabled = false;
    this.datatable.refresh();
  }
  displayFn(user): string | undefined {
    return user ? user.first_name + ' ' + user.last_name : undefined;
  }
  getUsersAll() {
    this.userService.getUsersAll().then(data => {
      this.userList = data;
      this.filteredOptions = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(value => {
            return typeof value === 'string' ? value : (value.first_name + ' ' + value.last_name);
          }),
          map(name => name ? this._filter(name) : this.userList.data.slice())
        );
    }).catch(error => {

    });
  }
  private post_filter(name: string) {
    const filterValue = name.toLowerCase();
    return this.postList.filter(option => {
      return option.title.toLowerCase().indexOf(filterValue) === 0;
    });
  }

  getSelectedStake(value) {
    this.post_id = value._id;
    this.request.page = 0;
    this.disabled = false;
    this.datatable.refresh();
  }

  displayStakeFn(post): string | undefined {
    return post ? post.title : undefined;
  }

  getAllPostList() {
    this.postService.getAllPostList().then(data => {
      this.postList = data;
      this.postFilteredOptions = this.postControl.valueChanges
        .pipe(
          startWith(''),
          map(value => {
            return typeof value === 'string' ? value : (value.title);
          }),
          map(name => {
            return name ? this.post_filter(name) : this.postList.slice()
          })
        );
    }).catch(error => {

    });
  }
  invoiceList = (request) => {
   
    // if (this.request.user_id) {
    //   request.user_id = this.request.user_id;
    // }
    request = Object.assign(request, this.request)

    if (this.user_id) {
      request.user_id = this.user_id;
    }

    if (this.post_id) {
      request.post_id = this.post_id;
    }
    return from(this.transactionService.getAllInvoice(request));
  }

  downloadFile(file) {
    var file_path = file;
    let a = document.createElement('a');
    a.href = file_path;
    a.target = '_blank';
    a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
  dateValueChange(type) {
    if (this.request.start_date) {
      this.minDate = new Date(this.request.start_date);
      this.minDate.setDate(this.minDate.getDate() + 1);
    }

    if (type == 'start') {
      this.request.end_date = null;
    }
    if (this.request.start_date == undefined || this.request.end_date == undefined || !this.request.start_date || !this.request.end_date) {
      this.disabled = true;
      return false;
    }
    if (this.request.start_date > this.request.end_date) {
      this.disabled = true;
      return false;
    } else {
      let endDate = new Date(this.request.end_date);
      endDate.setHours(23);
      endDate.setMinutes(59);
      endDate.setSeconds(59);
      this.request.start_date = moment(new Date(this.request.start_date)).format();
      this.request.end_date = moment(endDate).format();

      this.changeDateSubmit();
      this.request.page = 0;
      this.disabled = false;
      return false;
    }
  }
  /* dateValueChange(type) {
    if (type == 'start') {
      this.request.end_date = null;
    }
    if (this.request.start_date == undefined || this.request.end_date == undefined || !this.request.start_date || !this.request.end_date) {
      this.disabled = true;
      return false;
    }
    if (this.request.start_date > this.request.end_date) {
      this.disabled = true;
      return false;
    } else {
      this.changeDateSubmit();
      this.request.page = 0;
      this.disabled = false;
      return false;
    }
  } */
  changeDateSubmit() {
    this.datatable.refresh();
  }

  clearDate() {
    this.user_id = null;
    this.post_id = null;
    this.myControl.setValue('');
    this.postControl.setValue('');
    this.request = {};
    this.request.page = 0;
    this.datatable.refresh();
    this.disabled = true;
  }

}
