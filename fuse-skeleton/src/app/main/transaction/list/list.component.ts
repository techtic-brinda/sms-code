import { Component, OnInit, ViewChild, ViewEncapsulation, TemplateRef } from '@angular/core';
import { TransactionService } from 'app/shared/services/transaction.service';
import { Subject, from, Observable } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute } from '@angular/router';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { UserService } from 'app/shared/services/user.service';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { DatatableComponent } from 'app/shared/modules/datatable/datatable/datatable.component';
import { MatDialog } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ListComponent implements OnInit {
  loading: boolean = false;
  disabled: boolean = true;
  user_id: any;
  request_date:any = {};
  userList:any = [];
  searchInput:string;
  dialogRef: any;
  minDate: Date;

  myControl = new FormControl();
  filteredOptions: Observable<{}>;

  @ViewChild('datatable', { static: false }) datatable: DatatableComponent;
  @ViewChild("transactionInfoDialog", { static: false }) transactionInfoDialog: TemplateRef<any>;
  private _filterBy: string = "all";
  
  constructor(
    private transactionService: TransactionService,
    private userService: UserService,
    public activatedRoute: ActivatedRoute,
    private _matDialog: MatDialog,
  
   
  ) {
    // this.activatedRoute.params.subscribe((params: any) => {
    //   if (params.user_id) {
    //     this.request_date.user_id = params.user_id;
    //   }
    // })
   }
  
  ngOnInit() {
    this.getUsersAll();
  }
  private _filter(name: string) {
    const filterValue = name.toLowerCase();

    return this.userList.data.filter(option => {
      return option.first_name.toLowerCase().indexOf(filterValue) === 0 || option.last_name.toLowerCase().indexOf(filterValue) === 0;
    });
  }

  public get filterBy(): string {
    return this._filterBy;
  }
  public set filterBy(value: string) {
    this._filterBy = value;
    this.datatable.refresh();
  }

  transactionList = (request) => {

    if (this.filterBy && this.filterBy != 'all') {
      request.transactions_type = this.filterBy.toLowerCase()
    }
    request = Object.assign(request, this.request_date)

    if (this.user_id) {
      request.user_id = this.user_id;
    }

    return from(this.transactionService.getAll(request));
  }

  getSelectedUser(value){
    this.user_id = value._id;
    this.request_date.page = 0;
    this.disabled = false;
    this.datatable.refresh();
  }
  displayFn(user): string | undefined {
    return user ? user.first_name + ' ' + user.last_name : undefined;
  }

  getUsersAll() {
    this.userService.getUsersAll().then(data => {
      this.userList = data;
      this.filteredOptions = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(value =>{
            return typeof value === 'string' ? value : (value.first_name + ' ' + value.last_name);
          }),
          map(name => name ? this._filter(name) : this.userList.data.slice())
        );
    }).catch(error => {

    });
  }

 
  changeDateSubmit(){
    this.datatable.refresh();
  }
  dateValueChange(type){
    if (this.request_date.start_date) {
      this.minDate = new Date(this.request_date.start_date);
      this.minDate.setDate(this.minDate.getDate() + 1);
    }
    
    if(type == 'start'){
      this.request_date.end_date = null;
    }
    if (this.request_date.start_date == undefined || this.request_date.end_date == undefined || !this.request_date.start_date || !this.request_date.end_date) {
      this.disabled = true;
      return false;
    }
    if (this.request_date.start_date > this.request_date.end_date) {
      this.disabled = true;
      return false;
    }else{
      let endDate = new Date(this.request_date.end_date);
      endDate.setHours(23);
      endDate.setMinutes(59);
      endDate.setSeconds(59);
      this.request_date.start_date = moment(new Date(this.request_date.start_date)).format();
      this.request_date.end_date = moment(endDate).format();

      this.changeDateSubmit();
      this.request_date.page = 0;
      this.disabled = false;
      return false;
    }
  }
  clearDate(){
    this.user_id = null;
    this.myControl.setValue('');
    this.request_date = {};
    this.datatable.refresh();
    this.disabled = true;
  }
  viewTransation(item){

  }
  transctionInfoBox(data) {
    data.obj_data = JSON.parse(data.data);
    this._matDialog.closeAll();
    const dialogRef = this._matDialog.open(this.transactionInfoDialog, {
      panelClass: ['fix-left', 'form-dialog'],
      data: data
    });
  }
  
 /*  editUser(user): void {
        this.dialogRef = this._matDialog.open(UsersUserFormDialogComponent, {
            panelClass: ['fix-left', 'user-form-dialog'],
            data: {
                user: user,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                switch (response) {
                    case 'save':
                        this.datatable.refresh();
                        break;
                }
            });
    } */

}