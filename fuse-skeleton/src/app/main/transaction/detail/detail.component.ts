import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';

@Component({
    selector: 'app-transaction-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class DetailComponent implements OnInit {
    transactionDetail:any = {};
    
    ngOnInit() {
        this.transactionDetail.amount = 203.245;
        this.transactionDetail.closing_balance = 38883.8;
        this.transactionDetail.created_at = "2019-12-10 16:38:12";
        this.transactionDetail.data = "2019-12-10 16:38:12";
        this.transactionDetail.description = "Result has been declared for quo sunt ratione";
        this.transactionDetail.getway_transactions_id = "";
        this.transactionDetail.getway_type = null;
        this.transactionDetail.status = "success";
        this.transactionDetail.transactions_type = "credit";
        this.transactionDetail.user = { first_name: "Octavia", last_name: "Grimes"};
        this.transactionDetail.user_id = "2e393fe0-9e08-40ef-b090-54d23e0a6abf";
        this.transactionDetail._id = "6edf5005-5057-409c-8f82-64d031c745d8";
        
    }
}