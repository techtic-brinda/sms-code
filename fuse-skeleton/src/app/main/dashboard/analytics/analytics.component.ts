import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/shared/services/auth/auth.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AnalyticsService } from 'app/shared/services/analytics.service';
import { HelperService } from 'app/shared/services/helper.service';
import * as _ from 'underscore';
import { dateToLocalArray } from '@fullcalendar/core/datelib/marker';
import { SupportTicketService } from 'app/shared/services/supportTicket.service';
import { UserService } from 'app/shared/services/user.service';

@Component({
    selector     : 'analytics-dashboard',
    templateUrl  : './analytics.component.html',
    styleUrls    : ['./analytics.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AnalyticsDashboardComponent implements OnInit
{
    /* Profit */
    widgetSelectedYear = new Date().getFullYear();
    alreadyCheckYear = [];
    widgetYear = [];
    widgets = {
        datasets: [
            {
                label: 'Sales',
                data: [],
                fill: 'start'

            }
        ],
        labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
        colors: [
            {
                borderColor: '#42a5f5',
                backgroundColor: '#42a5f5',
                pointBackgroundColor: '#1e88e5',
                pointHoverBackgroundColor: '#1e88e5',
                pointBorderColor: '#ffffff',
                pointHoverBorderColor: '#ffffff'
            }
        ],
        options: {
            spanGaps: false,
            legend: {
                display: false
            },
            maintainAspectRatio: false,
            layout: {
                padding: {
                    top: 32,
                    left: 32,
                    right: 32
                }
            },
            elements: {
                point: {
                    radius: 4,
                    borderWidth: 2,
                    hoverRadius: 4,
                    hoverBorderWidth: 2
                },
                line: {
                    tension: 0
                }
            },
            scales: {
                xAxes: [
                    {
                        gridLines: {
                            display: false,
                            drawBorder: false,
                            tickMarkLength: 18
                        },
                        ticks: {
                            fontColor: '#ffffff'
                        }
                    }
                ],
                yAxes: [
                    {
                        display: false,
                        ticks: {
                            min: 0,
                            stepSize:1
                        }
                    }
                ]
            },
            plugins: {
                filler: {
                    propagate: false
                },
                xLabelsOnTop: {
                    active: true
                }
            }
        },
        chartType: "line"
    }
    user:any;

    private _unsubscribeAll: Subject<any>;
    /* Profit */
    /* Gamewise Player */
    playerWidget = {
        mainChart: [],
        xAxis: true,
        yAxis: true,
        gradient: false,
        legend: true,
        showXAxisLabel: true,
        xAxisLabel: 'Sports Type',
        showYAxisLabel: true,
        yAxisLabel: 'Total Shark',
        yAxisTicks: {
            stepSize:1
        },
        scheme: {
            domain: ['#4682b4']
        },
        onSelect: (ev) => {
        }
    };
    //
    /* Gamewise Player */
    /* Gamewise Post */
    postWidget = {
        mainChart: [],
        xAxis: true,
        yAxis: true,
        gradient: false,
        legend: true,
        showXAxisLabel: true,
        xAxisLabel: 'Sports Type',
        showYAxisLabel: true,
        yAxisLabel: 'Stake Total',
        yAxisTicks: {
            stepSize: 1
        },
        scheme: {
            domain: ['#4d5796']
        },
        onSelect: (ev) => {
        }
    };
    /* Gamewise Post */
    /* Support Tickets */
    supportMax = 0;
    supportWidget = {
        scheme: {
            domain: ['#4867d2', '#5c84f1', '#89a9f4']
        },
        items: [
            {
                name: 'Open',
                value: 92.8
            },
            {
                name: 'Closed',
                value: 6.1
            },
            {
                name: 'Unread',
                value: 1.1
            }
        ]
    };
    /* Support Tickets */
    /* stakes status */
    stakeMax = 0;
    stakesStatusWidget = {
        mainChart: [],
        legend: false,
        explodeSlices: false,
        labels: false,
        doughnut: true,
        gradient: false,
        scheme: {
            domain: ['#03a9f4', '#f44336', '#ff9800', '#607d8b', '#09d261']
        },
        onSelect: (ev) => {
            console.log(ev);
        }
    }
    /* stakes status */

    userWidget = {
        mainChart: [],
        legend: false,
        explodeSlices: false,
        labels: false,
        doughnut: false,
        gradient: false,
        scheme: {
            domain: ['#f44336', '#03a9f4', '#e91e63', '#ffc107']
        },
        onSelect: (ev) => {
            console.log(ev);
        },
        footerView: {
            totalPlayer : 0,
            totalUser: 0
        }
    };
    constructor(
        private authService: AuthService,
        private analyticsService: AnalyticsService,
        private helper: HelperService,
        public supportTicketService: SupportTicketService,
        public usersService: UserService,

    )
    {
        
        this._unsubscribeAll = new Subject();
        /* Profit */    
        this.getAdminProfit(this.widgetSelectedYear);
        /* Profit */
        /* Gamewise Player */
        this.getGameWisePlayerStatistics();
        /* Gamewise Player */
        /* Gamewise Post */
        this.getGameWisePostStatistics();
        /* Gamewise Post */
        /* Support Tickets */
        this.getSupportTicketTotal();
        /* Support Tickets */
        /* stakes status */
        this.getstakeTotal();
        /* stakes status */
        this.getStatusWiseUsers();
    }

    ngOnInit(): void {
        this.authService.$user
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((user) => {
                this.user = user;
                /* Profit */
                this.getYearList();
                /* Profit */
                
            })
    }


    /* Profit */
    getAdminProfit(year){
        this.widgetSelectedYear = year;
        this.analyticsService.getAdminProfit(this.widgetSelectedYear).then(data=>{
            let profitData = data.data;
            this.widgets.datasets[0].data = [];
            this.widgets.labels.forEach((element,index) => {
                let f = _.findIndex(profitData, {
                    month: (index + 1)
                });
                if (f >= 0) {
                    this.widgets.datasets[0].data.push(parseFloat(profitData[f].amount));
                }else{
                    this.widgets.datasets[0].data.push(0);
                }
            });
            let f = this.alreadyCheckYear.indexOf(this.widgetSelectedYear);
            if (this.alreadyCheckYear.length == 0){
                this._registerCustomChartJSPlugin();
                this.alreadyCheckYear.push(this.widgetSelectedYear);
            }
        }).catch(error =>{
            this.helper.errorMessage(error, `Error, while getting admin profit info.`);
        })
    }
    getYearList(){
        let userYear = new Date(this.user.created_at).getFullYear();
        for(let i=2; i >= 0; i--){
            if ((this.widgetSelectedYear-i) >= userYear){
                this.widgetYear.push((this.widgetSelectedYear - i));
            }
        }
    }
    private _registerCustomChartJSPlugin(): void
    {
        (window as any).Chart.plugins.register({
            afterDatasetsDraw: function(chart, easing): any {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                )
                {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function(dataset, i): any {
                    const meta = chart.getDatasetMeta(i);
                    if ( !meta.hidden )
                    {
                        meta.data.forEach(function(element, index): any {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (window as any).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            const dataString = '$ ' + dataset.data[index].toString();

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 15;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);
                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }
    /* Profit */
    /* Gamewise Player */
    getGameWisePlayerStatistics(){
        this.analyticsService.getGameWisePlayerStatistics().then(data => {
            let d = data.data;
            this.playerWidget.mainChart = [];
            d.forEach(element => {
                this.playerWidget.mainChart.push(
                        {
                            name: element.name,
                            value: element.total
                        }
                );
            });
        }).catch(error => {
            this.helper.errorMessage(error, `Error, while getting gamewise players info.`);
        })
    }
    /* Gamewise Player */
    /* Gamewise Post */
    getGameWisePostStatistics(){
        this.analyticsService.getGameWisePostStatistics().then(data => {
            let d = data.data;
            this.postWidget.mainChart = [];
            d.forEach(element => {
                this.postWidget.mainChart.push(
                    {
                        name: element.name,
                        value: element.total
                    }
                );
            });
        }).catch(error => {
            this.helper.errorMessage(error, `Error, while getting gamewise players info.`);
        })
    }
    /* Gamewise Post */
    /* Support Tickets */
    getSupportTicketTotal(){
        this.supportTicketService.getSupportTicketTotal().then(data => {
            this.supportWidget.items = [];
            this.supportWidget.items.push({ name: 'Open', value: data.open});
            this.supportWidget.items.push({ name: 'Closed', value: data.closed});
            this.supportWidget.items.push({ name: 'Unread', value: data.unread});
            let max = _.max(this.supportWidget.items, function (item) { return item.value; });
            this.supportMax = max.value;
            console.log(this.supportMax);
        }).catch(error => {
            this.helper.errorMessage(error, `Error, while getting support ticket info.`);
        });
    }
    /* Support Tickets */
    /* stakes status */
    getstakeTotal(){
        this.usersService.getStakeStatistics(this.user).then(data => {
            console.log(data);
            let d:[]  = data;
            this.stakesStatusWidget.mainChart = [];
            
            this.stakesStatusWidget.mainChart.push({ name: 'Active', value: data.active });
            this.stakesStatusWidget.mainChart.push({ name: 'Inactive', value: data.inactive });
            this.stakesStatusWidget.mainChart.push({ name: 'Pending Result', value: data.pending_result });
            this.stakesStatusWidget.mainChart.push({ name: 'Won', value: data.won });
            this.stakesStatusWidget.mainChart.push({ name: 'Loss', value: data.loss });
            
            let max = _.max(this.stakesStatusWidget.mainChart, function (item) { return item.value; });
            this.stakeMax = max.value;
        }).catch(error => {
            this.helper.errorMessage(error, "Error, while getting stakes info.");
        });
    }
    /* stakes status */

    getStatusWiseUsers(){
        this.analyticsService.getStatusWiseUsers().then(data => {
            data.data.data.forEach(element => {
                if (element.user_type == 'player'){
                    element.user_type = 'shark'
                }
                this.userWidget.mainChart.push({
                    name: (element.status+' '+element.user_type),
                    value: parseInt(element.total)
                })
            });
            if (data.data.totalData.length) {
                let dP = _.findWhere(data.data.totalData, { user_type: "player" });
                this.userWidget.footerView.totalPlayer = (dP) ? dP.total : 0;

                let dU = _.findWhere(data.data.totalData, { user_type: "user" });
                this.userWidget.footerView.totalUser = (dU) ? dU.total : 0;
            }
             console.log(this.userWidget);
        }).catch(error => {
            this.helper.errorMessage(error, "Error, while getting user info.");
        });
    }
}

