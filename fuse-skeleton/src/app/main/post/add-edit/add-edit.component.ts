import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { NgForm, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import * as _ from 'underscore';
import * as moment from 'moment';
import { from } from 'rxjs';

import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { PostService } from 'app/shared/services/post.service';
import { HelperService } from 'app/shared/services/helper.service';
import { SportTypeService } from 'app/shared/services/sport-type.service';
import { UserService } from 'app/shared/services/user.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AppInitService } from 'app/shared/services/app-init.service';

@Component({
	selector: 'post-add-edit',
	templateUrl: './add-edit.component.html',
	styleUrls: ['./add-edit.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class AddEditComponent implements OnInit {

	today: Date = new Date();
	// post: object = { closing_time: moment().add(1, 'day').toDate(), status: "pending" };
	sport_types: any;
	isLoading: boolean = false;
	user_list: any = [];

	// postId = new BehaviorSubject(null);
	// type = "Add";
	action: string;
	dialogTitle: string;
	postForm: FormGroup;
	post: any;
	//private _shark_amount: any = "";
	private _sold_percentage: any = "";

	constructor(
		// public route: ActivatedRoute,
		// public router: Router,
		public postService: PostService,
		public helper: HelperService,
		public sportTypeService: SportTypeService,
		public userService: UserService,
		public matDialogRef: MatDialogRef<AddEditComponent>,
		private appInitService: AppInitService,

		@Inject(MAT_DIALOG_DATA) _data: any,
	) {
		this.appInitService.$sport_types.subscribe((data) => this.sport_types = data);
		//	this.getSportTypes();
		this.getPlayerUsers();
		this.action = _data.action;
		if (this.action == 'edit') {
			this.dialogTitle = 'Edit Stake';
			this.post = Object.assign({}, _data.post);

			//this.post.game_amount = this.post.game_amount;
			this.sold_percentage = this.post.sold_percentage;
			this.post.closing_time = moment.utc(this.post.closing_time).local();
			this.post.closing_time = moment(this.post.closing_time).toDate();
			this.post.closing_time_picker = moment(this.post.closing_time).format("HH:mm");
		} else {
			this.dialogTitle = 'New Stake';
			this.post = Object.assign({ status: 'pending', closing_time: moment().add(1, 'day').toDate() }, _data.post || {});
		}
	}

	ngOnInit() {
		//subscribe post_id
		// this.postId.subscribe((postId) => {
		// 	if (postId) {
		// 		this.getPost(postId);
		// 	}
		// });
	}

	public get sold_percentage(): any {
		return this._sold_percentage;
	}
	public set sold_percentage(value: any) {
		this.post.shark_amount = ((parseFloat(value) * parseFloat(this.post.game_amount)) / 100).toFixed(2);
		this._sold_percentage = value;
		if ((value != null && value <= 0) || (value != null && value >= 100)) {
			this._sold_percentage = null;
			this.post.shark_amount = null;
		}
	}

	changePercentage(value) {
		this.post.shark_amount = ((parseFloat(this.sold_percentage) * parseFloat(value)) / 100).toFixed(2);
		if (value <= 0 && value >= 100) {
			this._sold_percentage = null;
			this.post.shark_amount = null;
		}
	}


	/* public get shark_amount(): any {
		return this._shark_amount;
	}
	public set shark_amount(value: any) {
		this.post.sold_percentage = (100 - ((parseFloat(value) * 100) / parseFloat(this.post.game_amount))).toFixed(2);
		this._shark_amount = value;
	}

	changePercentage(value) {
		this.post.sold_percentage = (100 - ((parseFloat(this.shark_amount) * 100) / parseFloat(value))).toFixed(2);
	} */

	submit(form: NgForm): void {
		form.ngSubmit.emit();
	}
	// getSportTypes(){
	//     this.sportTypeService.getAll().then((data: any) => {
	//       	this.sport_types = data;
	//     }).catch((error) => {});
	// }

	getPlayerUsers() {
		this.userService.getPlayers().then((data: any) => {
			this.user_list = data;
		}).catch((error) => { });
	}

	addUpdatePost(form: NgForm): void {
		if (form.invalid) {
			return;
		}
		this.isLoading = true;
		let request = _.pick(this.post, ['_id', 'title', 'closing_time', 'closing_time_picker', 'game_amount', 'total_staked_amount', 'sold_percentage', 'sport_type_id', 'user_id', 'description', 'is_streaming', 'stream_type', 'stream_url', 'status', 'markup_amount']);
		if (request.closing_time) {
			/* request.closing_time = moment(request.closing_time_picker).toISOString(); */
			request.closing_time = moment(request.closing_time).format('DD/MM/YYYY');
			if (request.closing_time_picker) {
				request.closing_time = moment(request.closing_time + ' ' + request.closing_time_picker, 'DD/MM/YYYY HH:mm');
			}
			request.closing_time = moment(request.closing_time).utc().format('YYYY-MM-DD HH:mm');
		}

		if (this.post.sold_percentage < 0) {
			this.helper.errorMessage({}, 'Please enter correct total staking amount and amount you have.');
		}

		//request.sold_percentage = parseFloat(request.sold_percentage);
		//	request.total_staked_amount = parseFloat(request.game_amount) - parseFloat(this._shark_amount);
		let inputData: any = {
			title: request.title,
			user_id: request.user_id,
			sport_type_id: request.sport_type_id,
			description: request.description,
			//total_staked_amount: parseFloat(request.game_amount) - parseFloat(this._shark_amount),
			total_staked_amount: parseFloat(request.game_amount) - parseFloat(this.post.shark_amount),
			//sold_percentage: parseFloat(request.sold_percentage),
			sold_percentage: parseFloat(this.sold_percentage),
			game_amount: request.game_amount,
			//player_amount: this._shark_amount,
			player_amount: parseFloat(this.post.shark_amount),
			closing_time: request.closing_time,
			is_streaming: request.is_streaming,
			stream_type: request.stream_type,
			stream_url: request.stream_url,
			status: 'active',
			markup_amount: request.markup_amount
		}
		if (this.post._id != undefined) {
			inputData._id = this.post._id;
		}
		this.postService.add(inputData).then((data) => {
			this.post = { closing_time: moment().add(1, 'day').toDate() };
			//this.router.navigateByUrl('/stake');
			this.helper.successMessage(data, `Post successfully ${request._id ? 'updated' : 'added'}.`);
			this.matDialogRef.close('save');
			this.isLoading = false;
		}).catch((error) => {
			this.matDialogRef.close('save');
			this.isLoading = false;
			this.helper.errorMessage(error, `Error, while ${request._id ? 'updating' : 'adding'} Post.`);
		});
	}
	deletePost(post_id): void {
		this.postService.delete(post_id);
		this.matDialogRef.close('delete');
	}
}
