import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { DetailsComponent } from './details/details.component';
import { AddEditComponent as PostAddEditComponent } from './add-edit/add-edit.component';

const routes: Routes = [
  	{ path: "", component: ListComponent },
	// { path: "add", data: { type: "post", mode:'add' }, component: PostAddEditComponent },
  	{ path: ":id", component: DetailsComponent },
  	// { path: ":id/edit", component: PostAddEditComponent },
];

@NgModule({
  	imports: [RouterModule.forChild(routes)],
  	exports: [RouterModule]
})
export class PostRoutingModule { }
