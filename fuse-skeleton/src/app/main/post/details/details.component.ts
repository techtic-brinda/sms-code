import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'app/shared/services/post.service';
import { fuseAnimations } from '@fuse/animations';
import { HelperService } from 'app/shared/services/helper.service';

@Component({
  selector: 'app-post-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class DetailsComponent implements OnInit {
  loading: boolean;
  post: any = {};
  // color = 'primary';
  // mode = 'determinate';
  // value = 50;
  // bufferValue = 75;

  constructor(
    public postService: PostService,
    public activatedRoute: ActivatedRoute,
    public helper: HelperService,
  ) {
    
   }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: any) => {
      if (params.id) {
        this.loading = true;
        this.postService.getOne(params.id).then(post => {
          this.post = post;
          console.log(this.post);
          this.loading = false;
        }).catch((error) => {
          this.loading = false;
          this.helper.errorMessage(error, "You can not open the page.");
        });
      }
    })
  }

  paidPlayerData = (id: string) => {
    let request = {
      post_id: id
    }
    this.postService.paidToPlayer(request).then((data) => {
      this.post.paid_to_player = data.data.paid_to_player;
      this.post.status = data.data.status;
      this.helper.successMessage(data);
    }).catch((error) => {
      this.helper.errorMessage(error);
    });
  }
}
