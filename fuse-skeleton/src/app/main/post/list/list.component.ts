import { Component, OnInit, ViewChild, ViewEncapsulation, TemplateRef, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { from, Subject } from 'rxjs';
import * as _ from 'underscore';
import * as moment from 'moment';
import { ModalComponent } from '../material-dialog/modal.component';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { HelperService } from 'app/shared/services/helper.service';
import { PostService } from 'app/shared/services/post.service';
import { AppInitService } from 'app/shared/services/app-init.service';
import { CustomDirective } from 'app/shared/modules/dialogs/directive/custom.directive';
import { DatatableComponent } from 'app/shared/modules/datatable/datatable/datatable.component';
import { UserService } from 'app/shared/services/user.service';
import { fuseAnimations } from '@fuse/animations';
import { AddEditComponent } from '../add-edit/add-edit.component';
import { FormGroup } from '@angular/forms';
import { AuthService } from 'app/shared/services/auth/auth.service';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-list-stake',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})

export class ListComponent implements OnInit {
    winning_stack_amount: number = 0;
    settings: any;
    searchInput: string;
  //  private _filterBy: string = "all";
    dialogRef: any;
    user: any
    stake_statistics_list: any;
    private _unsubscribeAll: Subject<any>;
    @ViewChild('datatable', { static: false }) datatable: DatatableComponent;
    @ViewChild("resultDeclarePostDialog", { static: false }) resultDeclarePostDialog: TemplateRef<any>;
    @ViewChild("refundPostDialog", { static: false }) refundPostDialog: TemplateRef<any>;
    
    constructor(
        public helper: HelperService,
        public postService: PostService,
        private _matDialog: MatDialog,
        private appInitService: AppInitService,
        private userService: UserService,
        private authService: AuthService
    ) {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit() {
        this.appInitService.$settings.subscribe((data) => {
            this.settings = data;
        });
        this.authService.$user
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((user) => {
                this.user = user;
        })

        this.getStakeStatistics();
    }
   

    dataSource = (request: any) => {
        let data = from(this.postService.getAll(request));
        return data;
    }

    // public get filterBy(): string {
    //     return this._filterBy;
    // }
    // public set filterBy(value: string) {
    //     this._filterBy = value;
    //     this.datatable.refresh();
    // }
    getStakeStatistics() {
        this.userService.getStakeStatistics(this.user._id).then(data => {
            this.stake_statistics_list = data;
        }).catch(error => {
            this.helper.errorMessage(error, "You can not open the page.");
        });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    deletePost(id: string) {
        this.postService.delete(id).then((data) => {
            this.datatable.refresh();
            this.helper.successMessage(data);
        }).catch((error) => {
            this.datatable.refresh();
            this.helper.errorMessage(error);
        });
    }

     paidPlayerData = (id:string) => {
        let request = {
            post_id: id
        }
        this.postService.paidToPlayer(request).then((data) => {
            this.datatable.refresh();
            this.helper.successMessage(data);
        }).catch((error) => {
            this.datatable.refresh();
            this.helper.errorMessage(error);
        });
    }
    paidUserData = (data) => {
        if (data.reason){
            let request = {
                post_id: data.user_id,
                reason:data.reason
            };
            this.postService.paidUserData(request).then((data) => {
                this._matDialog.closeAll();
                this.datatable.refresh();
                this.helper.successMessage(data);
            }).catch((error) => {
                this.datatable.refresh();
                this.helper.errorMessage(error);
            }); 
            
        }
    }


    editPost(post) {
        this.dialogRef = this._matDialog.open(AddEditComponent, {
            panelClass: ['fix-left', 'post-form-dialog'],
            data: {
                post: post,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                switch (response) {
                    case 'save':
                        this.datatable.refresh();
                        break;
                    case 'delete':
                        //this.deletePost(post._id);
                        this.datatable.refresh();
                        break;
                }
            });
    }
    newPost(): void {
        this.dialogRef = this._matDialog.open(AddEditComponent, {
            panelClass: ['fix-left', 'post-form-dialog'],
            data: {
                action: 'new',
                post: { }
            }
        });

        this.dialogRef.afterClosed()
            .subscribe((response: FormGroup) => {
                if (!response) {
                    return;
                }
                this.datatable.refresh();
            });
    }
    editData(data) {
        let new_data = Object.assign({}, data);

        if (new_data.closing_time) {
            new_data.closing_time = moment(new_data.closing_time).toDate();
        }
        return new_data;
    }
    resultDeclareData = (postData) => {
        let data = Object.assign({}, postData);
        let total_staked_amount = data.total_staked_amount;
        let sold_percentage = data.sold_percentage;
        let total_amount = data.game_amount;
        if(data.gap_amount){
            total_staked_amount = data.total_staked_amount - data.gap_amount;
            sold_percentage = (((total_staked_amount) * 100) /total_amount);
        }
        // let total_amount = (data.total_staked_amount * 100) / (data.sold_percentage);
        // let player_percentage = (100 - data.sold_percentage);
        data.game_amount = total_amount;
        data.shark_amount = total_amount - data.total_staked_amount; //Amount that Shark is willing to put in

        data.actual_user_amount = total_amount - data.shark_amount;
        data.user_percentage = data.sold_total * 100 / data.total_staked_amount;
        data.user_percentage = parseFloat(data.user_percentage.toFixed(2));
        let player_percentage = (100 - sold_percentage);
       
        data.winning_total_amount = parseFloat(data.winning_total_amount);
        let playerAmount = (data.winning_total_amount * player_percentage) / 100;
        let userAmount = (data.winning_total_amount * sold_percentage) / 100;

        let playerAdminCommission = (playerAmount * parseFloat(this.settings.commissions)) / 100;
        let userAdminCommission = (userAmount * parseFloat(this.settings.commissions)) / 100;
    
        data.admin_total_commission = playerAdminCommission + userAdminCommission;
        data.player_winning_amount = (playerAmount - playerAdminCommission);
        this.winning_stack_amount = data.user_winning_amount = (userAmount - userAdminCommission);
        let status = (data.winning_total_amount >= total_amount) ? "won" : "loss";
        data.winning_status = status;
        this.openModelResultDeclare(data);

    }
    openModel(post): void {
        this._matDialog.closeAll();
        let postForm = Object.assign({}, post);
        const dialogRef = this._matDialog.open(this.resultDeclarePostDialog, {
            panelClass: ['fix-left', 'post-form-dialog'],
            data: postForm
        });
    }

    openRefundModel(user_id): void {
        let id = { user_id: user_id };
        this._matDialog.closeAll();
        let postForm = Object.assign(id);
        const dialogRef = this._matDialog.open(this.refundPostDialog, {
            panelClass: ['fix-left', 'post-form-dialog'],
            data: postForm
        });
    }

    openModelResultDeclare(data) {
        this._matDialog.closeAll();
        data.datatable = this.datatable;
        const dialogRef = this._matDialog.open(ModalComponent, {
            panelClass: ['fix-left', 'post-form-dialog'],
            data: data
        });
    }
    
}
