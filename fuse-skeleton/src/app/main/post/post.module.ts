import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostRoutingModule } from './post-routing.module';
import { ListComponent } from './list/list.component';
import { DetailsComponent } from './details/details.component';
import { SharedModule } from '../../shared/shared.module';
import { AddEditComponent as PostAddEditComponent } from './add-edit/add-edit.component';
import { ModalComponent } from './material-dialog/modal.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';
/* import { AmazingTimePickerModule } from 'amazing-time-picker'; */
//import { AmazingTimePickerModule } from 'amazing-time-picker';

@NgModule({
    declarations: [
        ListComponent, 
        DetailsComponent,
        PostAddEditComponent,
        ModalComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        PostRoutingModule,
        FuseSharedModule,
        //AmazingTimePickerModule,
        FuseConfirmDialogModule
    ],
    entryComponents:[
        PostAddEditComponent,
        ModalComponent
    ]
})
export class PostModule { }
