import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import * as _ from 'underscore';
import { PostService } from 'app/shared/services/post.service';
import { HelperService } from 'app/shared/services/helper.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    public postService: PostService,
    public helper: HelperService,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onConform(data): void {
    let request = _.pick(data, ["_id", "winning_status"]);
    
    request.admin_total_commission = parseFloat(data.admin_total_commission.toFixed(2));
    request.winning_total_amount = parseFloat(data.winning_total_amount.toFixed(2));
    request.total_staked_amount = parseFloat(data.total_staked_amount.toFixed(2));
    request.user_winning_amount = parseFloat(data.user_winning_amount.toFixed(2));
    request.player_winning_amount = parseFloat(data.player_winning_amount.toFixed(2));
    
    this.postService.updatePostResult(request).then((data) => {
      this.helper.successMessage(data, `Result Declared.`);
      this.onCancel();
      this.data.datatable.refresh();
    }).catch((error) => {
      this.helper.errorMessage(error, `Error, while Result Declare.`);
    });
  }
  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}