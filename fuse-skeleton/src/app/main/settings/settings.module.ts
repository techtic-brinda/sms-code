import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { ListComponent } from './list/list.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

@NgModule({
  declarations: [
    ListComponent,
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    FuseSharedModule,
    FuseConfirmDialogModule
  ]
})
export class SettingsModule { }
