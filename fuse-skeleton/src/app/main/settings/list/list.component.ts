import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { from } from 'rxjs';
import * as _ from 'underscore';
import { NgForm } from '@angular/forms';
import { HelperService } from 'app/shared/services/helper.service';
import { SettingsService } from 'app/shared/services/settings.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'app-settings',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations 
})
export class ListComponent implements OnInit {
  settings: any = {};
  loading: boolean = false;
  isLoading: boolean = false;
  type: string;
  @ViewChild('f', { static: false }) addEditForm: NgForm;

  constructor(
    private settingsService:SettingsService,
    private helper:HelperService
  ) { 
    this.getSettings();
  }

  ngOnInit() {
  }

  getSettings(){
    this.loading = true;
    this.settingsService.get().then(settings => {
      this.loading = false;
      this.settings = settings;
    }).catch((error) => {
      this.loading = false;
    });
  }
  addUpdateSettings(form: NgForm): void {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    let keys = [];
    for (let key in this.settings) {
      if ((key == 'commissions' || key == 'coaching_commissions' || key == 'stripe_charges') && !this.settings[key]){
        this.settings[key] = 0;
      }
      keys.push({name: key, value: this.settings[key]});
    }
    this.settingsService.add(keys).then((data) => {
      this.helper.successMessage(data);
      this.isLoading = false;
    }).catch((error) => {
      this.isLoading = false;
      this.helper.errorMessage(error);
    });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  settingList = (request?:any) => {
    return from(this.settingsService.get(request));
  }

  editData(data) {
    return Object.assign({}, data);
  }
}
