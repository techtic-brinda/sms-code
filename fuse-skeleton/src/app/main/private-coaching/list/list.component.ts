import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { SportTypeService } from 'app/shared/services/sport-type.service';
import { Subject, from, Observable } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute } from '@angular/router';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { DatatableComponent } from 'app/shared/modules/datatable/datatable/datatable.component';
import { MatDialog } from '@angular/material';
import { CustomDirective } from 'app/shared/modules/dialogs/directive/custom.directive';
import * as _ from 'underscore';
import { NgForm, FormControl } from '@angular/forms';
import { HelperService } from 'app/shared/services/helper.service';
import { UserService } from 'app/shared/services/user.service';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-sports-type-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})

export class ListComponent implements OnInit {
  
  userControl = new FormControl();
  userFilteredOptions: Observable<{}>;
  userList: any = [];
  user_id: any;
  disabled: boolean = true;


  playerControl = new FormControl();
  playerFilteredOptions: Observable<{}>;
  playerList: any = [];
  player_id: any;
  searchInput: any;

  requestData:any = {};

  @ViewChild('datatable', { static: false }) datatable: DatatableComponent;
  
  constructor(
    public usersService: UserService,
    private helper: HelperService

  ) { }

  privateCoachingList = (request) => {
    request = Object.assign(request, this.requestData);

    request.status = ['booked','inactive'];
    return from(this.usersService.getRequestList(request));
  }

  ngOnInit() {
    this.getUsersAll();
    this.getPlayersAll();
  }
  private user_filter(name: string) {
    const filterValue = name.toLowerCase();

    return this.userList.filter(option => {
      return option.first_name.toLowerCase().indexOf(filterValue) === 0 || option.last_name.toLowerCase().indexOf(filterValue) === 0;
    });
  }
  getSelectedUser(value, type) {
    if(type === 'user'){
      this.user_id = value._id;
      this.requestData.user_id = value._id;
    }

    if (type === 'player') {
      this.player_id = value._id;
      this.requestData.player_id = value._id;
    }

    this.requestData.page = 0;
    this.disabled = false;
    this.datatable.refresh();
  }
  displayFn(user): string | undefined {
    return user ? user.first_name + ' ' + user.last_name : undefined;
  }

  getUsersAll() {
    this.usersService.getPlayers('user').then(data => {
      this.userList = data;
      this.userFilteredOptions = this.userControl.valueChanges
        .pipe(
          startWith(''),
          map(value => {
            return typeof value === 'string' ? value : (value.first_name + ' ' + value.last_name);
          }),
          map(name => name ? this.user_filter(name) : this.userList.slice())
        );
    }).catch(error => {

    });
  }

  getPlayersAll(){
    this.usersService.getPlayers('player').then(data => {
      this.playerList = data;
      this.playerFilteredOptions = this.playerControl.valueChanges
        .pipe(
          startWith(''),
          map(value => {
            return typeof value === 'string' ? value : (value.first_name + ' ' + value.last_name);
          }),
          map(name => name ? this.player_filter(name) : this.playerList.slice())
        );
    }).catch(error => {

    });
  }
  private player_filter(name: string) {
    const filterValue = name.toLowerCase();

    return this.playerList.filter(option => {
      return option.first_name.toLowerCase().indexOf(filterValue) === 0 || option.last_name.toLowerCase().indexOf(filterValue) === 0;
    });
  }
  displayFnPlayer(user): string | undefined {
    return user ? user.first_name + ' ' + user.last_name : undefined;
  }

  clearFilter(){
    this.user_id = null;
    this.userControl.setValue('');

    this.player_id = null;
    this.playerControl.setValue('');

    this.requestData = {};
    this.datatable.refresh();
    this.disabled = true;
  }

  dateValueChange(type) {
    if (type == 'start') {
      this.requestData.end = null;
    }
    if (this.requestData.start == undefined || this.requestData.end == undefined || !this.requestData.start || !this.requestData.end) {
      this.disabled = true;
      return false;
    }
    if (this.requestData.start > this.requestData.end) {
      this.disabled = true;
      return false;
    } else {
      this.requestData.page = 0;
      this.datatable.refresh();
      this.disabled = false;
      return false;
    }
  }

  refundUser(item){
    this.usersService.sendUserRefund(item.id).then(data => {
      this.datatable.refresh();
      this.helper.successMessage(data, "Private Coaching successfully cancelled.");
    }).catch(error=>{
      this.datatable.refresh();

      this.helper.errorMessage(error, "Error, while cancel private coaching.");
    })
  }

  
}
