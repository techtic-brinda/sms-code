import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { from } from 'rxjs';
import { SupportTicketService } from 'app/shared/services/supportTicket.service';
import { DatatableComponent } from 'app/shared/modules/datatable/datatable/datatable.component';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'app-support-ticket',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: fuseAnimations
})
export class ListComponent implements OnInit {
  type = 'all';
  loading:boolean = false;
  loadMore: boolean = false;
  total:any = {};
  supportTicketList:any = [];
  supportTicketRequest:any = {
    limit: 30,
    page: 0
  };
  filteredTicket:any = [];
  headerClass = ['web-bg', 'android-bg', 'cloud-bg', 'firebase-bg']
  searchTerm: string;
  currentCategory:any;

  @ViewChild('datatable', { static: false }) datatable: DatatableComponent;
  constructor(
    public supportTicketService: SupportTicketService,
  ) { }

  ngOnInit() {
    this.getSupportTicketTotal();
    this.getList().then((data: any) => {
      this.supportTicketList = data;
    });
    
  }

  getList(){
    this.loading = true;
    this.supportTicketRequest.type = this.type;
    return this.supportTicketService.getList(this.supportTicketRequest).then(data =>{
        this.loading = false;
        if (data.meta.last_page > data.meta.current_page) {
          this.loadMore = true;
        } else {
          this.loadMore = false;
        }
        return data.data;
      }).catch(err => {
        this.loading = false;
      });
  }
  onLoadMore() {
    this.supportTicketRequest.page++;
   // this.getList();
    this.getList().then((data) => {
      this.supportTicketList = this.supportTicketList.concat(data);
     // this.filteredTicket = Object.assign({}, this.supportTicketList);
    });
  }

  getSupportTicketTotal(){
    this.loading = true;
    this.supportTicketService.getSupportTicketTotal().then(data => {
      this.total = data;
      this.loading = false;
    }).catch(err => {
      this.loading = false;
    });
  }

  refreshDataTable(type){
    this.type = type.value;
    this.getSupportTicketTotal();
    this.supportTicketRequest = {
      limit: 30,
      page: 0
    };
    this.getList().then((data) => {
      this.supportTicketList = data;
    });
  }

  filterCoursesByTerm(): void {
    const searchTerm = this.searchTerm.toLowerCase();
    this.supportTicketRequest.filter = searchTerm;
    this.supportTicketRequest.filter_in = [{ name: "subject", type: "default" }, { name: "user.name", type: "default"}];
    this.getList().then((data) => {
      this.supportTicketList = data;
    });
  }

}
