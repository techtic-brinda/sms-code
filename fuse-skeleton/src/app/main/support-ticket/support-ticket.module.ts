import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { SupportTicketRoutingModule } from './support-ticket-routing.module';
import { ViewComponent } from './view/view.component';
import { SharedModule } from 'app/shared/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule, FuseSidebarModule } from '@fuse/components';
import { ReplyMsgComponent } from './reply-msg/reply-msg.component';
import { CKEditorModule } from 'ng2-ckeditor';

@NgModule({
  declarations: [
    ListComponent,
    ViewComponent,
    ReplyMsgComponent
  ],
  imports: [
    CommonModule,
    SupportTicketRoutingModule,
    SharedModule,
    FuseWidgetModule,
    FuseSharedModule,
    FuseConfirmDialogModule,
    FuseSidebarModule,
    CKEditorModule
  ],
  entryComponents: [
    ReplyMsgComponent
  ]
})
export class SupportTicketModule { }
