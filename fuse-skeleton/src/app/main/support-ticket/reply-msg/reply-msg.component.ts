import { Component, ViewEncapsulation, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgForm } from '@angular/forms';
import { HelperService } from 'app/shared/services/helper.service';
import { SupportTicketService } from 'app/shared/services/supportTicket.service';
import * as _ from 'underscore';

@Component({
	selector: 'reply-msg',
	templateUrl: './reply-msg.component.html',
	styleUrls: ['./reply-msg.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class ReplyMsgComponent implements OnInit {
	supportTicket: any = {};
	isLoading: boolean = false;
	constructor(
		public matDialogRef: MatDialogRef<ReplyMsgComponent>,
		public helper: HelperService,
		public supportTicketService: SupportTicketService,
		@Inject(MAT_DIALOG_DATA) _data: any,
	) {
		this.supportTicket._id = _data.ticket._id;
	}


	ngOnInit(): void {
	}
	submit(form: NgForm): void {
		form.ngSubmit.emit();
	}

	replySend(form: NgForm): void {
		let request = _.pick(this.supportTicket, ["message", "file"]);
      if (!request.message) {
        this.helper.errorMessage('', `Please enter message.`);
        return;
      }
      this.isLoading = true;
		request._id = this.supportTicket._id;

      this.supportTicketService.addReplyToMsg(request).then((data) => {
		this.isLoading = false;
		this.matDialogRef.close();
		// this.supportTicketDetail.messages.unshift(data);
		this.helper.successMessage('', `Reply has been send successfully.`);
		// this.clearContent();
	}).catch((error) => {
		this.matDialogRef.close();
        this.isLoading = false;
        this.helper.errorMessage(error, `Error, while send reply.`);
      })
	}
	setFile($event) {
      	if ($event.target.files.length > 0) {
			var fileToLoad = $event.target.files[0];
			var reader: FileReader = new FileReader();
			reader.onloadend = (readerEvt: any) => {
			this.supportTicket.file = readerEvt.target.result;
			}
			reader.readAsDataURL(fileToLoad);
			this.supportTicket.file = $event.target.files;
			this.supportTicket.file_name = $event.target.files[0].name;
			this.supportTicket.type = fileToLoad.type;
      	}
  	}

}
