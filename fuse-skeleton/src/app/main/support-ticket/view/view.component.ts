import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import * as _ from 'underscore';
import { AuthService } from 'app/shared/services/auth/auth.service';
import { HelperService } from 'app/shared/services/helper.service';
import { SupportTicketService } from 'app/shared/services/supportTicket.service';
import { fuseAnimations } from '@fuse/animations';
import { MatDialog } from '@angular/material';
import { ReplyMsgComponent } from '../reply-msg/reply-msg.component';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ViewComponent implements OnInit {
  supportTicketDetail:any;
  loading: boolean = false;
  supportTicket:any = {};
  panelOpenState = false;
  currentUser:any;
  isLoading: boolean = false;
  animationDirection: 'left' | 'right' | 'none';
  currentStep: number;
  dialogRef: any;
  ticket_id:any;

  constructor(
    public route: ActivatedRoute,
    public supportTicketService: SupportTicketService,
    public helper: HelperService,
    public authService: AuthService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _matDialog: MatDialog,
  ) {
      route.params.subscribe((params) => {
        this.ticket_id = params['id'];
        this.getSupportTicket(params['id']);
      });
  }

  ngOnInit() {
      this.currentUser = this.authService.user;
      this.currentStep = 0;
  }

  getSupportTicket(id){
      this.loading = true;
      this.supportTicketService.getSupportTicket(id).then( data => {
            this.loading = false;
            this.supportTicketDetail = data;
          }).catch( err =>{
            this.loading = false;
            this.helper.errorMessage(err, "You can not open the page.");
      });
  }
  gotoStep(step): void {
    // Decide the animation direction
    this.animationDirection = this.currentStep < step ? 'left' : 'right';

    // Run change detection so the change
    // in the animation direction registered
    this._changeDetectorRef.detectChanges();

    // Set the current step
    this.currentStep = step;
  }
  gotoNextStep(): void {
    if (this.currentStep === this.supportTicketDetail.messages.length - 1) {
      return;
    }

    // Set the animation direction
    this.animationDirection = 'left';

    // Run change detection so the change
    // in the animation direction registered
    this._changeDetectorRef.detectChanges();

    // Increase the current step
    this.currentStep++;
  }

  /**
   * Go to previous step
   */
  gotoPreviousStep(): void {
    if (this.currentStep === 0) {
      return;
    }

    // Set the animation direction
    this.animationDirection = 'right';

    // Run change detection so the change
    // in the animation direction registered
    this._changeDetectorRef.detectChanges();

    // Decrease the current step
    this.currentStep--;
  }

  sendReply(ticket){
    this.dialogRef = this._matDialog.open(ReplyMsgComponent, {
      panelClass: ['fix-left', 'relpy-form-dialog'],
      data: {
        ticket: ticket
      }
    });

    this.dialogRef.afterClosed()
      .subscribe(response => {
        // if (!response) {
        //   return;
        // }

        this.getSupportTicket(this.ticket_id);
      });
  }

  downloadFile(file) {
    var file_path = file.file;
    let a = document.createElement('a');
    a.href = file_path;
    a.target = '_blank';
    a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  markAsClosed(status?: any) {
    let request = {
      id: this.supportTicketDetail._id,
      status: status,
    }
    this.supportTicketService.changeStatus(request).then((data) => {
      this.helper.successMessage('', `Ticket has been ${status} successfully.`);
      this.supportTicketDetail = data;
    }).catch((error) => {
      this.helper.errorMessage(error, `Error, while close ticket.`);
    })
  }

 
  // onSubmit(form: NgForm){
  //     let request = _.pick(this.supportTicket, ["message", "file"]);
  //     if (!request.message) {
  //       this.helper.errorMessage('', `Please enter message.`);
  //       return;
  //     }
  //     this.isLoading = true;
  //     request._id = this.supportTicketDetail._id;
      
  //     this.supportTicketService.addReplyToMsg(request).then((data) => {
  //       this.isLoading = false;
  //       this.supportTicketDetail.messages.unshift(data);
  //       this.helper.successMessage('', `Reply has been send successfully.`);
  //       this.clearContent();
  //     }).catch((error) => {
  //       this.isLoading = false;
  //       this.helper.errorMessage(error, `Error, while send reply.`);
  //     })
  // }

  

  // downloadFile(file) {
  //     var file_path = file.file;
  //     let a = document.createElement('a');
  //     a.href = file_path;
  //     a.target = '_blank';
  //     a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
  //     document.body.appendChild(a);
  //     a.click();
  //     document.body.removeChild(a);
  // }

  // downloadFileUpload(file) {
  //   var file_path = file;
  //   let a = document.createElement('a');
  //   a.href = file_path;
  //   a.target = '_blank';
  //   a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
  //   document.body.appendChild(a);
  //   a.click();
  //   document.body.removeChild(a);
  // }

  // clearContent(){
  //     this.supportTicket = {
  //       message:""
  //     };
  // }
  // markAsClosed(status?:any){
  //   let request = {
  //     id: this.supportTicketDetail._id,
  //     status: status,
  //   }
  //   this.supportTicketService.changeStatus(request).then((data) => {
  //       this.helper.successMessage('', `Ticket has been ${status} successfully.`);
  //       this.supportTicketDetail = data;
  //     }).catch((error) => {
  //       this.helper.errorMessage(error, `Error, while close ticket.`);
  //     })
  // }

}
