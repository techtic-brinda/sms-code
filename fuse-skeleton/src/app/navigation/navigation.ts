import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'item',
        icon: 'dashboard',
        url: '/dashboard',
    },
    {
        id: 'users',
        title: 'Users',
        type: 'item',
        icon: 'supervisor_account',
        url: '/users',
        // badge: {
        //     title: '25',
        //     translate: 'NAV.SAMPLE.BADGE',
        //     bg: '#F44336',
        //     fg: '#FFFFFF'
        // }
    },

    {
        id: 'transaction',
        title: 'Transactions',
        type: 'item',
        icon: 'account_balance_wallet',
        url: '/transaction',
    },
    {
        id: 'invoice',
        title: 'Invoices',
        type: 'item',
        icon: 'file_copy',
        url: '/invoice',
    },
    {
        id: 'sports_type',
        title: 'Sports Types',
        type: 'item',
        icon: 'list',
        url: '/sports-type',
    },
    {
        id: 'stake',
        title: 'Stakes',
        type: 'item',
        icon: 'list_alt',
        url: '/stake',
    },

    {
        id: 'settings',
        title: 'Settings',
        type: 'collapsable',
        icon: 'settings',
        children: [
            {
                id: 'general_setting',
                title: 'General Settings',
                type: 'item',
                icon: 'settings',
                url: '/settings',
            },
            {
                id: 'email_template',
                title: 'Email Templates',
                type: 'item',
                icon: 'email',
                url: '/email-template',
            },
            {
                id: 'notification_template',
                title: 'Notification Templates',
                type: 'item',
                icon: 'notifications',
                url: '/notification-template',
            }
        ]
    },

    {
        id: 'cms',
        title: 'CMS',
        type: 'collapsable',
        icon: 'file_copy',
        children: [
            {
                id: 'faq',
                title: 'FAQ',
                type: 'item',
                icon: 'help_outline',
                url: '/faq',
            },
            {
                id: 'page',
                title: 'Pages',
                type: 'item',
                icon: 'assignment',
                url: '/page',
            },
            {
                id: 'tutorial',
                title: 'Tutorials',
                type: 'item',
                icon: 'import_contacts',
                url: '/tutorial',
            }
        ]
    },
    {
        id: 'support',
        title: 'Support',
        type: 'item',
        icon: 'forum',
        url: '/support',
    },
    {
        id: 'private_coaching',
        title: 'Private Coaching',
        type: 'item',
        icon: 'list',
        url: '/private-coaching',
    },
];
