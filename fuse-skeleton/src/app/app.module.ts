import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';
import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { fuseConfig } from 'app/fuse-config';
import { AppComponent } from 'app/app.component';
import { AppRoutingModule } from './app-routing.module';
import { JwtModule } from '@auth0/angular-jwt';
import { DialogsModule } from './shared/modules/dialogs/dialogs.module';
import { GraphQLModule } from './graphql.module';
import { ToastyModule } from 'ngx-toasty';
import { Services } from './shared/services';
import { AppInitService } from './shared/services/app-init.service';
export function tokenGetter() {
    return localStorage.getItem('admin-token');
}
import { MomentModule } from 'ngx-moment';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,
        GraphQLModule,
        DialogsModule.forRoot(),
        TranslateModule.forRoot(),
        ToastyModule.forRoot(),
        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter,
                whitelistedDomains: new Array(new RegExp('^null$'))
            }
        }),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,
        MomentModule.forRoot()
    ],
    providers: [
        ...Services,
        {
            provide: APP_INITIALIZER,
            useFactory: (appInitService: AppInitService) => {
                return () => appInitService.init()
            },
            deps: [AppInitService],
            multi: true,
        },
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
