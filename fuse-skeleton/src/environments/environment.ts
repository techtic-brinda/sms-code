export const environment = {
  production: true,
  apiUrl: '/api',
  deployUrl: '',
  baseUrl: '',
  graphQlUrl: 'http://45.79.111.106:3000',
  hmr: false,
};
