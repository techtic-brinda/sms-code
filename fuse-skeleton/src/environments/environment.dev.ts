
export const environment = {
  production: false,
  apiUrl: 'http://45.79.111.106/stake-my-shark-server',
  deployUrl: '/stake-my-shark-server/dist',
  baseUrl: '/stake-my-shark-server',
  graphQlUrl: 'http://45.79.111.106:3000',
  hmr: false,
};
