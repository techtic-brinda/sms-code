
export const environment = {
    production: false,
    apiUrl: '/api',
    deployUrl: '',
    baseUrl: '/backend',
    graphQlUrl: 'http://45.79.111.106:3000',
    hmr: false,
};
