// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: '/api',
  deployUrl: '',
  baseUrl: '',
  graphQlUrl: 'http://45.79.111.106:3000',
  hmr: false,
};
