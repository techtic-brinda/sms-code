export const environment = {
  production: true,
  apiUrl: '/api',
  deployUrl: '/admin',
  baseUrl: '/admin',
  graphQlUrl: '',
  hmr: false,
};
