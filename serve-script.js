const concurrently = require('concurrently');
concurrently([
    { command: 'ng serve --configuration=serve --proxy-config proxy.config.json', name: 'angular' },
    { command: 'tsc-watch -p server/tsconfig.json --onSuccess "node dist/server-app/main.js"', name: 'server' },
], {
    prefix: 'name',
    killOthers: ['failure', 'success'],
    restartTries: 3,
    restartDelay: 10000,
}).then((data)=>{
    console.log(data);
}, (error)=>{
    console.error(error)
});