<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>


## Setup Project

```bash

# Step 1 : Go to project root dir.
yarn install

# Step 2 : Coply .env.dev to .env
cp .env.local .env

# Step 3 : Yarn install and Coply .env.dev to .env  
cd server
yarn install
cp .env.local .env

# Step 4 : Run Migration (You need to run this command under the server dir)
yarn migrate

```

## Installation and other command

```bash

# Root (Frontend & Server)
yarn install
yarn start

# Back End
cd backend
npm install
npm run build


# Start Developement Server 
# Root (Server & Frontend)
yarn start # It will start server and fronedn for development, If you want to workin Backend then you have to run npm start from backend dir.

# If you want to work Back end
cd backend
npm start
# Build
npm run start


# Build
# in root
yarn build # Build server and frontend SSR for Production (Live Server)
yarn build:dev # Build server and frontend SSR for Development (Live Server)
yarn build:local # Build server and frontend SSR for Local (Live Server)

# Run production server
# You need to run `yarn build` before the start production server

# in root
yarn start:prod

# in Back end
npm run build

```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```


## License

  Nest is [MIT licensed](LICENSE).
