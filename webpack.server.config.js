// Work around for https://github.com/angular/angular-cli/issues/7200
const path = require('path');
const webpack = require('webpack');

const WebpackConfigFactory = require('@nestjs/ng-universal')
  .WebpackConfigFactory;

/**
 * In fact, passing following configuration to the WebpackConfigFactory is not required
 * default options object returned from this method has equivalent entries defined by default.
 *
 * Example: WebpackConfigFactory.create(webpack);
 */
module.exports = {
  ...WebpackConfigFactory.create(webpack, { server: './server/src/main.ts' }),

  resolve: {
    extensions: ['*', '.mjs', '.js', '.vue', '.json', '.gql', '.graphql']
  },
}
