import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashierComponent } from './cashier/cashier.component';
import { CashierRoutingModule } from './cashier-routing.module';
import { PayPalComponent } from '../partials/pay-pal/pay-pal.component';
import { NgxPayPalModule } from 'ngx-paypal';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { BankWireComponent } from '../partials/bank-wire/bankWire.component';
import { CryptoCurrencyComponent } from '../partials/crypto-currency/crypto-currency.component';

@NgModule({
  declarations: [
    CashierComponent,
    PayPalComponent,
    CryptoCurrencyComponent,
    BankWireComponent
  ],
  imports: [
    CommonModule,
    CashierRoutingModule,
    NgxPayPalModule,
    NgbDatepickerModule,
    SharedModule
  ]
})
export class CashierModule { }
