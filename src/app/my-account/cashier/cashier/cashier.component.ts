import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { PayPalService } from 'src/app/shared/services/payment/pay-pal.service';
import { StackService } from 'src/app/shared/services/stack/stack.service';
import { Router } from '@angular/router';
import { HelperService } from 'src/app/shared/services/helper.service';


@Component({
  selector: 'app-cashier',
  templateUrl: './cashier.component.html',
  styleUrls: ['./cashier.component.scss']
})
export class CashierComponent implements OnInit {
  paymentMethod = 'credit';
  withdrawMethod = 'paypal';
  paymentData: any = {};
  withdrawData: any = {};
  invoiceData: any = {};
  user: any = {};
  withdraw_amount: any;
  withdraw_email: any;
  stackHttpResponse: any = {};
  loading = false;
  loadMore = false;

  invoiceRequest: any = {
    page: 0,
    limit: 20
  };

  constructor(
    private modalService: NgbModal,
    private authService: AuthService,
    private payPalService: PayPalService,
    private stackService: StackService,
    private router: Router,
    private helper: HelperService,
  ) { }

  ngOnInit() {
    this.authService.$user.subscribe((data) => {
      if (!data) {
        return
      }
      this.user = data;
      /* if (this.user.user_type == 'user'){
        this.getInvoices().then((data: any) => {
          this.invoiceData = data;
        });
      } */
    });
  }

  addAllAmount() {
    this.withdraw_amount = this.user.wallet_amount;
  }

  openPaymentModel(content) {
    this.paymentMethod = "credit";
    this.openModel(content);
  }

  openModel(content) {
    return this.modalService.open(content, { size: 'lg' });
  }

  openModelThankyouModel($event, content) {
    this.modalService.dismissAll();
    this.modalService.open(content, { size: 'lg' });
    this.paymentData = $event;
    this.authService.user = Object.assign(this.authService.user, { wallet_amount: $event.closing_balance });
  }

  withdrawNow(content) {
    if (parseFloat(this.user.wallet_amount) < parseFloat(this.withdraw_amount)) {
      return false;
    }
    if (!this.withdraw_email) {
      return false;
    }
    this.loading = true;

    this.payPalService.withdraw(this.withdraw_amount, this.withdraw_email).then((resp: any) => {
      this.modalService.dismissAll();
      this.withdrawData = resp.data;
      if (resp.data.status == 'pending'){
        this.withdrawData.msg = "Your withdraw payment request sent to PAYPAL."
      }else{
        this.withdrawData.msg = "Amount successfully sent to your PAYPAL account."
      }
      this.authService.user = Object.assign(this.authService.user, { wallet_amount: this.withdrawData.closing_balance });
      this.modalService.open(content, { size: 'lg' });
      this.loading = false;
    }).catch((error) => {
      this.helper.errorMessage(error);
      this.loading = false;
      //this.stackHttpResponse = error;
    })
  }

  /* getInvoices(){
    this.loading = true;
    return this.stackService.getInvoices(this.invoiceRequest).then((data) => {
      this.loading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.loadMore = true;
      } else {
        this.loadMore = false;
      }
      return data.data;
    })
  }

  onLoadMore() {
    this.invoiceRequest.page++;
    this.getInvoices().then((data) => {
      this.invoiceData = this.invoiceData.concat(data);
    });
  }

  downloadFile(file) {
    var file_path = file;
    let a = document.createElement('a');
    a.href = file_path;
    a.target = '_blank';
    a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  } */

}
