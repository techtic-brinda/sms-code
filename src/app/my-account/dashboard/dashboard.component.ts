import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AppInitService } from 'src/app/shared/services/app-init.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { Location, DatePipe } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PayPalService } from 'src/app/shared/services/payment/pay-pal.service';
import { UserService } from '../../../app/shared/services/auth/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  sport_types: any = [];
  user: any = {};
  page: any = 'dashboard';
  stackHttpResponse: any = {};
  paymentMethod = 'credit';
  withdeawMethod = 'paypal';
  paymentData: any = {};
  withdraw_amount: any;
  withdraw_email: any;
  //statisticsList: any;

  constructor(
    private appInitService: AppInitService,
    private authService: AuthService,
    private location: Location,
    private modalService: NgbModal,
    private payPalService: PayPalService,
    private userService: UserService,
  ) {
    //this.getStatistics();
  }

  addAllAmount() {
    this.withdraw_amount = this.user.wallet_amount;
  }
  
  ngOnInit() {
    this.appInitService.$sport_types.subscribe((data) => this.sport_types = data);
    this.authService.$user.subscribe((data) => {
      if (!data) {
        return
      }
      this.user = data;
    });
  }

  ngAfterViewInit() {

  }

  openModel(content) {
    return this.modalService.open(content, { size: 'lg' });
  }

  openPaymentModel(content) {
    this.openModel(content);
  }

  openModelThankyouModel($event, content) {
    this.modalService.dismissAll();
    this.modalService.open(content, { size: 'lg' });
    this.paymentData = $event;
    this.authService.user = Object.assign(this.authService.user, { wallet_amount: $event.closing_balance });
  }



  chagneTab($event) {
    switch ($event.nextId) {
      case 'dashboard':
        this.location.replaceState('my-account');
        break;

      default:
        this.location.replaceState(`my-account/${$event.nextId}`);
        break;
    }
  }

  withdrawNow() {
    if (this.user.wallet_amount < this.withdraw_amount) {
      return false;
    }
    if (!this.withdraw_email) {
      return false;
    }

    this.payPalService.withdraw(this.withdraw_amount, this.withdraw_email).then((resp: any) => {
    }).catch((error) => {
      this.stackHttpResponse = error;
    })
  }

  // getStatistics(){
  //   this.userService.getStatistics().then((data: any) => {
  //     this.statisticsList = data;
  //   }).catch((error) => {
  //     this.stackHttpResponse = error;
  //   })
  // }
  
}
