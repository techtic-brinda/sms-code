import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { StackService } from 'src/app/shared/services/stack/stack.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {

 
  invoiceData: any = {};
  user: any = {};
  withdraw_amount: any;
  withdraw_email: any;
  stackHttpResponse: any = {};
  loading = false;
  loadMore = false;

  invoiceRequest: any = {
    page: 0,
    limit: 20
  };

  constructor(
    private authService: AuthService,
    private stackService: StackService,
   
  ) { }

  ngOnInit() {
    this.authService.$user.subscribe((data) => {
      if (!data) {
        return
      }
      this.user = data;
      if (this.user.user_type == 'user') {
        this.getInvoices().then((data: any) => {
          this.invoiceData = data;
        });
      }
    });
  }
 
  getInvoices() {
    this.loading = true;
    return this.stackService.getInvoices(this.invoiceRequest).then((data) => {
      this.loading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.loadMore = true;
      } else {
        this.loadMore = false;
      }
      return data.data;
    })
  }
  onLoadMore() {
    this.invoiceRequest.page++;
    this.getInvoices().then((data) => {
      this.invoiceData = this.invoiceData.concat(data);
    });
  }

  downloadFile(file) {
    var file_path = file;
    let a = document.createElement('a');
    a.href = file_path;
    a.target = '_blank';
    a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

}
