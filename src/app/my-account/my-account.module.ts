import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyAccountRoutingModule } from './my-account-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPayPalModule } from 'ngx-paypal';
import { NgxStripeModule } from 'ngx-stripe';
import { PayPalComponent } from './partials/pay-pal/pay-pal.component';
import { BankWireComponent } from './partials/bank-wire/bankWire.component';


@NgModule({
  declarations: [
    DashboardComponent,
    // StripeComponent,
    // PayPalComponent,
   // BankWireComponent
  ],
  imports: [
    CommonModule,
    MyAccountRoutingModule,
    SharedModule,
    // NgxStripeModule,
    // NgxPayPalModule,
    // NgbDatepickerModule
  ],
  providers: []
})
export class MyAccountModule { }
