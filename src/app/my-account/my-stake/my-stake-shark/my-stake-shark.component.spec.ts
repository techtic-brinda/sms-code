import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyStakeSharkComponent } from './my-stake-shark.component';

describe('MyStakeSharkComponent', () => {
  let component: MyStakeSharkComponent;
  let fixture: ComponentFixture<MyStakeSharkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyStakeSharkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyStakeSharkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
