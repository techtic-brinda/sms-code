import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { AppInitService } from 'src/app/shared/services/app-init.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { StackService } from 'src/app/shared/services/stack/stack.service';
import { DatePipe, isPlatformBrowser } from '@angular/common';
import { NgForm } from '@angular/forms';
import { _ } from 'underscore';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HelperService } from 'src/app/shared/services/helper.service';
import { flatMap } from 'rxjs/operators';
import * as moment from 'moment';


@Component({
  selector: 'app-my-stake-shark',
  templateUrl: './my-stake-shark.component.html',
  styleUrls: ['./my-stake-shark.component.scss']
})
export class MyStakeSharkComponent implements OnInit {
  sport_types: any[] = [];
  listStack: any[] = [];
  listPastStack: any[] = [];
  //private _shark_amount: any = "";
  private _sold_percentage: any = "";
  addStack: any = {};
  stackHttpResponse: any = {};
  cloasingMaxDate: { year: any; month: any; day: any; };

  listStackRequest: any = {
    page: 0,
    limit: 10,
    type: 'upcomming'
  };
  listStackLoadMore: boolean = false;
  listStackLoading: boolean = true;

  listPastStackRequest: any = {
    page: 0,
    limit: 10,
    type: 'past'
  };
  listPastStackLoadMore: boolean = false;
  listPastStackLoading: boolean = true;
  deleteStackList: any;
  isBrowser: boolean;
  nextday;
  is_streaming_error: boolean = false;

  constructor(

    private appInitService: AppInitService,
    private authService: AuthService,
    private modalService: NgbModal,
    private stackService: StackService,
    private helperService: HelperService,
    @Inject(PLATFORM_ID) platformId,
  ) {
    this.isBrowser = isPlatformBrowser(platformId);

    const currentDate = new Date();
    this.nextday = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 1);
    this.cloasingMaxDate = { year: this.nextday.getFullYear(), month: this.nextday.getMonth() + 1, day: this.nextday.getDate() };
  }




  /*  public get shark_amount(): any {
    return this._shark_amount;
  }
  public set shark_amount(value: any) {

    this.addStack.sold_percentage = (100 - ((parseFloat(value) * 100) / parseFloat(this.addStack.game_amount))).toFixed(2);
    this._shark_amount = value;
    if (this.addStack.sold_percentage <= 0) {
      this._shark_amount = null;
      this.helperService.errorMessage('You have to add less amount then grand total.');
    }  else if (this.addStack.sold_percentage == 100){
      this.helperService.errorMessage('You have to add less amount then grand total.');
      this._shark_amount = null;
    }
  } */

  /* changePercentage(value){
    this.addStack.sold_percentage = (100 - ((parseFloat(this.shark_amount) * 100) / parseFloat(value))).toFixed(2);
    if (this.addStack.sold_percentage <= 0) {
      this._shark_amount = null;
    }
  } */


  ngOnInit() {

    this.appInitService.$sport_types.subscribe((data) => {
      data.forEach(element => {
        if (element.status == 'active') {
          this.sport_types.push(element);
        }
      });
    });

    this.authService.$user.subscribe((data) => {
      if (!data) {
        return
      }
      if (data.user_type == 'player') {
        this.getupComingPlayerStack().then((data: any) => {
          this.listStack = data;
        });
        this.getupPastPlayerStack().then((data: any) => {
          this.listPastStack = data;
        });
      }
    });

  }
  public get sold_percentage(): any {
    return this._sold_percentage;
  }

  public set sold_percentage(value: any) {
    this.addStack.shark_amount = ((parseFloat(value) * parseFloat(this.addStack.game_amount)) / 100).toFixed(2);
    this._sold_percentage = value;
    if ((value != null && value <= 0) || (value != null && value >= 100)) {
      this._sold_percentage = null;
      this.addStack.shark_amount = null;
      this.helperService.errorMessage('You have to add less percentage then 100.');
    }
  }


  changePercentage(value) {
    this.addStack.shark_amount = ((parseFloat(this.sold_percentage) * parseFloat(value)) / 100).toFixed(2);
    if (value <= 0 && value >= 100) {
      this._sold_percentage = null;
      this.addStack.shark_amount = null;
    }
  }


  getProgress(stake) {
    return ((stake.sold_total > 0) ? 100 - ((stake.sold_total * 100) / stake.total_staked_amount) : 100) + "%";
  }


  getupComingPlayerStack() {
    this.listStackLoadMore = true;
    return this.stackService.getActiveUserStack(this.listStackRequest).then((data) => {
      this.listStackLoading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.listStackLoadMore = true;
      } else {
        this.listStackLoadMore = false;
      }
      return data.data;
    }).catch(() => { });
  }
  onLoadMoreListStack() {
    this.listStackRequest.page++;
    this.getupComingPlayerStack().then((data) => {
      this.listStack = this.listStack.concat(data);
    });
  }


  getupPastPlayerStack() {
    this.listPastStackLoadMore = true;
    return this.stackService.getActiveUserStack(this.listPastStackRequest).then((data) => {
      this.listPastStackLoading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.listPastStackLoadMore = true;
      } else {
        this.listPastStackLoadMore = false;
      }
      return data.data;
    }).catch(() => { });
  }
  onLoadMorePastPlayerStack() {
    this.listPastStackRequest.page++;
    this.getupPastPlayerStack().then((data) => {
      this.listPastStack = this.listPastStack.concat(data);
    });
  }
  openDeleteStakeModel(content, stake?: any) {
    this.modalService.open(content, { size: 'sm' });
    this.deleteStackList = stake;
  }

  deleteStack(deleteStack) {
    this.stackService.deleteStack(this.deleteStackList._id).then(async (data) => {
      this.modalService.dismissAll();
      this.deleteStackList = {};
      this.helperService.successMessage(data, 'Stake successfully deleted.');
      this.getupComingPlayerStack().then((data: any) => {
        this.listStack = data;
      });
    }).catch((error) => {
      this.stackHttpResponse = error;
    });

  }

  openAddEditStakeModel(content, stake?: any) {

    this.is_streaming_error = false;
    this.modalService.open(content, { size: 'lg' });
    this.addStack = {};
    console.log(stake,'stake');

    if (stake) {
      let moment_closing_time = moment.utc(stake.closing_time).local();
      //console.log(moment_closing_time);
      //console.log(moment_closing_time.format('YYYY-MM-DD HH:mm'));
      this.addStack = Object.assign({}, stake);

      this.addStack.closing_time = moment_closing_time.format('YYYY-MM-DD HH:mm')
      this.addStack.date = {
        day: parseInt(moment_closing_time.format('DD')),
        month: parseInt(moment_closing_time.format('MM')),
        year: parseInt(moment_closing_time.format('YYYY'))
      }
      this.addStack.time = {
        hour: parseInt(moment_closing_time.format('HH')),
        minute: parseInt(moment_closing_time.format('mm'))
      }
      // this.addStack.game_amount = (100 * this.addStack.total_staked_amount) / parseFloat(this.addStack.sold_percentage)
      //this.shark_amount = parseFloat(this.addStack.player_amount);
      this.sold_percentage = parseFloat(this.addStack.sold_percentage);
    } else {
      let moment_closing_time = moment(new Date()).add(1, 'days');;

      //this.shark_amount = null;
      this.sold_percentage = null;
      this.addStack.sport_type_id = null;
      this.addStack.date = {
        day: parseInt(moment_closing_time.format('DD')),
        month: parseInt(moment_closing_time.format('MM')),
        year: parseInt(moment_closing_time.format('YYYY'))
      }
      this.addStack.time = {
        hour: parseInt(moment_closing_time.format('HH')),
        minute: parseInt(moment_closing_time.format('mm'))
      }
    }
    console.log(this.addStack);
  }

  doAddEditStake(form: NgForm) {

    this.stackHttpResponse = null;
    if (form.valid && this.is_streaming_error == false) {

      /* if (this.addStack.sold_percentage < 0){
        this.helperService.errorMessage({}, 'Please enter correct total staking amount and amount you have.');
      } */
      if (this.sold_percentage < 0) {
        this.helperService.errorMessage({}, 'Please enter correct total staking amount and amount you have.');
      }
      this.addStack.closing_time = new Date(this.addStack.date.year, this.addStack.date.month - 1, this.addStack.date.day, this.addStack.time.hour, this.addStack.time.minute, 0); //,"YYYY-MM-DD HH:mm"
      this.addStack.closing_time = moment(this.addStack.closing_time).utc().format('YYYY-MM-DD HH:mm');

      let inputData: any = {
        title: this.addStack.title,
        sport_type_id: this.addStack.sport_type_id,
        description: this.addStack.description,
        //total_staked_amount: parseFloat(this.addStack.game_amount) - parseFloat(this._shark_amount),
        total_staked_amount: parseFloat(this.addStack.game_amount) - parseFloat(this.addStack.shark_amount),
        //sold_percentage: parseFloat(this.addStack.sold_percentage),
        sold_percentage: parseFloat(this.sold_percentage),
        game_amount: parseFloat(this.addStack.game_amount),
        player_amount: parseFloat(this.addStack.shark_amount),
        closing_time: this.addStack.closing_time,
        is_streaming: this.addStack.is_streaming,
        stream_type: this.addStack.stream_type,
        stream_url: this.addStack.stream_url,
        markup_amount: parseFloat(this.addStack.markup_amount)
        //status: 'active'
      }
      if (this.addStack._id != undefined) {
        inputData._id = this.addStack._id;
      } else {
        inputData.status = 'active';
      }
      this.stackService.addStack(inputData).then((data) => {
        this.modalService.dismissAll();
        this.addStack = {};
        form.reset();
        if (this.addStack._id == undefined) {
          this.getupComingPlayerStack().then((data: any) => {
            this.listStack = data;
          });
        }
        this.helperService.successMessage(data, 'Stake successfully created.');
      }).catch((error) => {
        this.stackHttpResponse = error;
        // this.modalService.dismissAll();
        //this.helperService.errorMessage(error, 'Error while creating stake, Please try again.');
      });
    }
  }

}
