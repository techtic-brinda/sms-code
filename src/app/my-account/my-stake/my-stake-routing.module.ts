import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyStakeSharkComponent } from './my-stake-shark/my-stake-shark.component';


const routes: Routes = [
  {
    path:"",
    component: MyStakeSharkComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyStakeRoutingModule { }
