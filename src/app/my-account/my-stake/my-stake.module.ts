import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyStakeRoutingModule } from './my-stake-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MyStakeSharkComponent } from './my-stake-shark/my-stake-shark.component';
import { CountdownTimerModule } from 'ngx-countdown-timer';
import { NgbDatepickerModule, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [MyStakeSharkComponent],
  imports: [
    CommonModule,
    SharedModule,
    MyStakeRoutingModule,
    CountdownTimerModule,
    NgbDatepickerModule,
    NgbTimepickerModule
  ]
})
export class MyStakeModule { }
