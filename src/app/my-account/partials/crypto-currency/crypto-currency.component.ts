import { Component, OnInit } from '@angular/core';
import { CryptoCurrencyService } from 'src/app/shared/services/payment/crypto-currency.service';

@Component({
    selector: 'app-crypto-currency',
    templateUrl: './crypto-currency.component.html',
    styleUrls: ['./crypto-currency.component.scss']
})
export class CryptoCurrencyComponent implements OnInit {
    loading = false;
    totalAmount = 0.0;
    hostUrl: any;
    step = 1;
    constructor(
        private cryptoCurrencyService: CryptoCurrencyService
    ) { }

    ngOnInit() {
    }

    onAmountChange(amount) {
        this.totalAmount = (amount) ? parseFloat(amount) : 0.0;
    }
    payNow() {
        if (!this.totalAmount || this.totalAmount < 15){
            return false;
        }
        this.loading = true;
        let request = {
            payment_gateway: 'cryptocurrency',
            amount: this.totalAmount
        }
        this.cryptoCurrencyService.addFund(request).then(result => {
            this.hostUrl = result.data.hosted_url;
            this.step++;
        })
        // this.stripeService
        //     .createToken(this.card.getCard(), { name: this.name })
        //     .subscribe(result => {
        //         if (result.token) {
        //             this.appStripeService.charge(result.token.id, this.amount).then((data: any) => {
        //                 this.done.emit(data.data);
        //                 this.loading = false;
        //             }).catch((error) => {
        //                 this.error.emit(error);
        //                 this.loading = false;
        //             })
        //         } else if (result.error) {
        //             this.error.emit(result.error);
        //             this.loading = false;
        //         }
        //     });
    }
}