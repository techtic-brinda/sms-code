import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PayPalService } from 'src/app/shared/services/payment/pay-pal.service';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ICreateOrderRequest, IPayPalConfig } from 'ngx-paypal';

@Component({
  selector: 'app-pay-pal',
  templateUrl: './pay-pal.component.html',
  styleUrls: ['./pay-pal.component.scss']
})
export class PayPalComponent implements OnInit {

  public payPalConfig?: IPayPalConfig;

  $payPalAmount: Subject<any> = new Subject();

  private _paypal_amount: any = '';
  public get paypal_amount(): any {
    return this._paypal_amount;
  }
  public set paypal_amount(value: any) {
    this._paypal_amount = value;
    this.$payPalAmount.next(value);
  }

  @Output('done') done: EventEmitter<any> = new EventEmitter()
  @Output('error') error: EventEmitter<any> = new EventEmitter()

  constructor(
    private payPalService: PayPalService
  ) { }

  ngOnInit() {
    this.$payPalAmount.asObservable().pipe(debounceTime(800)).subscribe(() => {
      this.initConfig();
    })
  }

  payNow(amount, data) {
    this.payPalService.charge(amount, data).then((resp: any) => {
      this.done.emit(resp.data);
    }).catch((error) => {
      this.error.emit(error);
    })
  }

  private initConfig(): void {
    this.payPalConfig = {
        currency: 'USD',
        clientId: 'AavTVEFTEqyqSYECeXFtKdCbFJ3s8oaH0BzlAqCnpwRSoFXObWM523lECWnaiu1qyLhAwgFIX0Fk9i6d',
      //  clientId: 'AbheYRaTDILqMIxgk_7ZW1kllyI1jqSuYzK78cRVPvCmUwcDDTMls460BdYXG1y8Go7R-c-9Q_hEBszE',
        createOrderOnClient:() => {
          return {
              intent: 'CAPTURE',
              purchase_units: [{
                  amount: {
                      currency_code: 'USD',
                      value: this.paypal_amount,
                      breakdown: {
                          item_total: {
                              currency_code: 'USD',
                              value: this.paypal_amount
                          }
                      }
                  },
                  items: [{
                      name: 'Add funds in stake my shark wallet',
                      quantity: '1',
                      category: 'DIGITAL_GOODS',
                      unit_amount: {
                          currency_code: 'USD',
                          value: this.paypal_amount,
                      },
                  }]
              }]
          }
      },
      advanced: {
          commit: 'false'
      },
      style: {
          label: 'paypal',
          layout: 'horizontal',
          size: 'small',
          shape: 'pill'
      },
      onApprove: (data, actions) => {
        actions.order.get().then(details => {
        });
      },
      onClientAuthorization: (data) => {
        this.payNow(this.paypal_amount, data);

        //this.showSuccess = true;
      },
      onCancel: (data, actions) => {
      },
      onError: err => {
        this.error.emit(err);
      },
      onClick: (data, actions) => {
        //actions.reject();
        //return false
      },
    };
  }
}