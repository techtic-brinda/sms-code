import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { StripeService as AppStripeService } from 'src/app/shared/services/payment/stripe.service';
import { StripeCardComponent, ElementOptions, ElementsOptions, StripeService } from 'ngx-stripe';


@Component({
  selector: 'app-bankwire',
  templateUrl: './bankWire.component.html',
  styleUrls: ['./bankWire.component.scss']
})
export class BankWireComponent implements OnInit {
  name:string;
  formData: any = {};

  @Output('done') done: EventEmitter<any> = new EventEmitter()
  @Output('error') error: EventEmitter<any> = new EventEmitter()

  private _amount: number;
  @Input()
  public get amount(): number {
    return this._amount;
  }
  public set amount(value: number) {
    this._amount = value;
  }
  constructor(
    private stripeService: StripeService,
    private appStripeService: AppStripeService,

  ) { }

  @ViewChild(StripeCardComponent, { static: false }) card: StripeCardComponent;

  cardOptions: ElementOptions = {
    hidePostalCode:true
  }

 
  elementsOptions: ElementsOptions = {
    locale: 'en',
  };

  ngOnInit() {
  }

  payNow() {
    this.stripeService
      .createToken(this.card.getCard(), { name : this.name })
      .subscribe(result => {
        if (result.token) {
          this.appStripeService.charge(result.token.id, this.amount).then((data:any)=>{
            this.done.emit(data.data);
          }).catch((error)=>{
            this.error.emit(error);
          })
        } else if (result.error) {
          this.error.emit(result.error);
        }
      });
  }

}
