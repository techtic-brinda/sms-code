import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankWireComponent } from './bankWire.component';

describe('BankWireComponent', () => {
  let component: BankWireComponent;
  let fixture: ComponentFixture<BankWireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankWireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankWireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
