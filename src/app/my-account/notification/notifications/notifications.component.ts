import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NotificationService } from 'src/app/shared/services/notification.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements AfterViewInit {
  notificationRequest: any = {
    page: 0,
    limit: 20
  };
  notifications: any[] = [];
  loadMore: boolean = false;
  loading: boolean = true;


  constructor(
    private notificationService: NotificationService
  ) {
    this.readAllNotifications();
  }

  ngAfterViewInit() {
    this.getNotifications().then((data: any) => {
      this.notifications = data;
      console.log("notification",this.notifications)

    });
  }

  onLoadMore() {
    this.notificationRequest.page++;
    this.getNotifications().then((data) => {
      this.notifications = this.notifications.concat(data);
    });
  }



  readAllNotifications() {
    this.notificationService.readAllNotifications().then((data) => {
    });
  }

  getNotifications() {
    this.loading = true;
    return this.notificationService.getNotifications(this.notificationRequest).then((data) => {
      this.loading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.loadMore = true;
      } else {
        this.loadMore = false;
      }
      return data.data;
    })
  }
}
