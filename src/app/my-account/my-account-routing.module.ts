import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path: "edit",
    loadChildren: () => import('src/app/my-account/edit-profile/edit-profile.module').then(m => m.EditProfileModule)
  },
  {
    path: "",
    component: DashboardComponent,
    children: [
      {
        path: "",
        loadChildren: () => import('src/app/my-account/statistic/statistic.module').then(m => m.StatisticModule)
      },
      {
        path: "track-my-teams",
        loadChildren: () => import('src/app/my-account/track-my-teams/track-my-teams.module').then(m => m.TrackMyTeamsModule)
      },
      {
        path: "my-stake",
        loadChildren: () => import('src/app/my-account/my-stake/my-stake.module').then(m => m.MyStakeModule)
      },
      {
        path: "notification",
        loadChildren: () => import('src/app/my-account/notification/notification.module').then(m => m.NotificationModule)
      },
      {
        path: "transactions",
        loadChildren: () => import('src/app/my-account/transactions/transactions.module').then(m => m.TransactionsModule)
      },
      {
        path: "invoices",
        loadChildren: () => import('src/app/my-account/invoices/invoices.module').then(m => m.InvoicesModule)
      },
      {
        path: "availability",
        loadChildren: () => import('src/app/my-account/availability/availability.module').then(m => m.AvailabilityModule)
      },
      {
        path: "cashier",
        loadChildren: () => import('src/app/my-account/cashier/cashier.module').then(m => m.CashierModule)
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MyAccountRoutingModule { }
