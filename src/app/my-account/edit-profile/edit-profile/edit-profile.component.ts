import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { UserService } from 'src/app/shared/services/auth/user.service';
import { HelperService } from 'src/app/shared/services/helper.service';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { AppInitService } from 'src/app/shared/services/app-init.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  changePasswordHttpResponse: any = {};
  editUser: any = {};
  changePassword: any = {};
  changeRate: any = {};
  user: any = {};
  profileHttpResponse: any = {};
  rateHttpResponse: any = {};
  dobMaxDate: any = {};
  settings: any;
  sport_types: any = [];
  loading:boolean = false;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private helperService: HelperService,
    private appInitService: AppInitService,
  ) {
    const currentDate = new Date();
    let lastday = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - 1);
    this.dobMaxDate = { year: lastday.getFullYear(), month: lastday.getMonth() + 1, day: lastday.getDate() };

  }

  ngOnInit() {
    this.appInitService.$settings.subscribe((data) => this.settings = data);
    this.appInitService.$sport_types.subscribe((data) =>{
      data.forEach(element => {
        if(element.status == 'active'){
          this.sport_types.push(element);
        }
      });
    });
    this.authService.$user.subscribe((data) => {
      if (!data) {
        return
      }
      this.user = data;

      const datePipe = new DatePipe("en-US");
      let dob:Object;
      if (this.user && this.user.dob != undefined) {
        dob = {
          day: parseInt(datePipe.transform(this.user.dob, 'dd')),
          month: parseInt(datePipe.transform(this.user.dob, 'M')),
          year: parseInt(datePipe.transform(this.user.dob, 'yyyy'))
        }
      } else {
        // dob = {
        //   day: parseInt(datePipe.transform(new Date(), 'dd')) - 1,
        //   month: parseInt(datePipe.transform(new Date(), 'M')),
        //   year: parseInt(datePipe.transform(new Date(), 'yyyy'))
        // }
      }
      this.editUser = {
        first_name: data.first_name,
        last_name: data.last_name,
        email: data.email,
        dob: dob,
        gender: data.gender,
        about: data.about,
        sport_type_id: data.sport_type_id,
        profile_pic: data.profile_pic
      }
      this.changeRate = {
        coaching_amount: data.coaching_amount,
        admin_amount: parseFloat(this.settings.coaching_commissions) || 0
      }
      this.onAmountChange()
    });
  }

  doChangePassowrd(form: NgForm) {
    this.loading = true;
    this.changePasswordHttpResponse = null;
    if (form.valid) {
      this.userService.changePassword({
        old_password: form.value.old_password,
        password: form.value.password
      }).then((data) => {
        this.loading = false;
        this.authService.user = Object.assign(this.authService.user, data.data);
        this.helperService.successMessage(data, 'Your password successfully changed.');
        form.resetForm(true);
      }).catch((error) => {
        this.loading = false;
        this.changePasswordHttpResponse = error;
      });
    }
  }
  doUpdateRate(form: NgForm){
    this.rateHttpResponse = null
    if (form.valid) {
      let request = {
        coaching_amount: parseFloat(this.changeRate.coaching_amount)
      }
      this.userService.updateProfile(request).then((data) => {
        this.rateHttpResponse = data
        this.authService.user = Object.assign(this.authService.user, data.user);
        this.helperService.successMessage(data, 'Private Coaching Rate successfully updated.');
      }).catch((error) => {
        this.rateHttpResponse = error;
      });
    }

  }

  doUpdateProfile(form: NgForm) {
    this.loading = true;
    this.profileHttpResponse = null;
    if (form.valid) {
      if (this.editUser.dob) {
        this.editUser.dob = `${this.editUser.dob.year}-${this.editUser.dob.month}-${this.editUser.dob.day}`;
      }

      this.userService.updateProfile(this.editUser).then((data) => {
        this.loading = false;
        this.profileHttpResponse = data;
        this.authService.user = Object.assign(this.authService.user, data.user);
        this.helperService.successMessage(data, 'Profile successfully updated.');
      }).catch((error) => {
        this.loading = false;
        this.profileHttpResponse = error;
      });
    }
  }

  setProfilePic($event) {
    if ($event.target.files.length > 0) {
      var fileToLoad = $event.target.files[0];
      var reader: FileReader = new FileReader();
      reader.onloadend = (readerEvt: any) => {
        this.editUser.profile_pic = readerEvt.target.result;
      }
      reader.readAsDataURL(fileToLoad);
      this.editUser.profile_pic = $event.target.files;
    }
  }

  onAmountChange(){
    let adminValue = 0;
    if(this.changeRate.admin_amount > 0){
      adminValue = (this.changeRate.admin_amount * this.changeRate.coaching_amount) / 100;
    }
    this.changeRate.rec_coaching_amount = this.changeRate.coaching_amount - adminValue;
  }

}
