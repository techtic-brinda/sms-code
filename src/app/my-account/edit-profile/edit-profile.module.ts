import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditProfileRoutingModule } from './edit-profile-routing.module';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [EditProfileComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgbDatepickerModule,
    EditProfileRoutingModule
  ]
})
export class EditProfileModule { }
