import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyStakeBuyerComponent } from './my-stake-buyer/my-stake-buyer.component';

const routes: Routes = [
  {
    path:"",
    component: MyStakeBuyerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrackMyTeamsRoutingModule { }
