import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { StackService } from 'src/app/shared/services/stack/stack.service';
import { AppInitService } from 'src/app/shared/services/app-init.service';
import { UserService } from 'src/app/shared/services/auth/user.service';
import { postGropByUser } from 'src/app/shared/utils';
import { _ } from 'underscore';


@Component({
  selector: 'app-my-stake-buyer',
  templateUrl: './my-stake-buyer.component.html',
  styleUrls: ['./my-stake-buyer.component.scss']
})
export class MyStakeBuyerComponent implements OnInit {
  sport_types: any[] = [];
  players: any = [];
  buyStackList: any;
  pastBuyStackList: any;
  buyStacksFilters: any = {
    limit: 20,
    page: 0,
    type: 'upcomming'
  };
  buyStacksLoadMore: boolean = false;
  buyStacksLoading: boolean = true;

  pastBuyStacksFilters: any = {
    limit: 20,
    page: 0,
    type:'past'
  };
  pastBuyStacksLoadMore: boolean = false;
  pastBuyStacksLoading: boolean = true;
  loading: boolean = true;

  constructor(
    private appInitService: AppInitService,
    private authService: AuthService,
    private userService: UserService,
    private stackService: StackService
  ) { }

  ngOnInit() {
    

    this.appInitService.$sport_types.subscribe((data) => this.sport_types = data);

    this.userService.getPlayers().then((data) => {
      this.players = data;
    });

    this.authService.$user.subscribe((data) => {
      if (!data) {
        return
      }
      if (data.user_type == 'user') {
        this.buyStacks().then((data: any) => {
          this.buyStackList = data;
          this.loading = false;
        });
        this.pastBuyStacks().then((data: any) => {
          this.pastBuyStackList = data;
          this.loading = false;
          _.map(this.pastBuyStackList, function (key) { 
            _.map(key['posts'], function (key) { 
                key['past_stack'] = 1;
              })
           });
          });
      }
      
    });
  }

  applyUpcommingFilter(key:string, value?:string) {
    this.loading = true;
    if (value) {
      this.buyStacksFilters[key] = value;
    } else {
      delete this.buyStacksFilters[key];
    }
    this.buyStacks().then((data: any) => {
      this.buyStackList = data;
      this.loading = false;
    });
  }

  applyPastFilter(key: string, value?: string) {
    this.loading = true;
    if (value){
      this.pastBuyStacksFilters[key] = value;
    }else{
      delete this.pastBuyStacksFilters[key];
    }
    this.pastBuyStacks().then((data: any) => {
      this.pastBuyStackList = data;
      this.loading = false;
    });
  }

  // buyStacks() {
  //   this.stackService.getBuyerPost(this.buyStacksFilters).then((data) => {
  //     this.buyStackList = postGropByUser(data.data);
  //     //this.buyStackList = data.data;
  //   }).catch(() => { });
  // }

  buyStacks() {
    this.buyStacksLoadMore = true;
    return this.stackService.getBuyerPost(this.buyStacksFilters).then((data) => {
      this.buyStacksLoading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.buyStacksLoadMore = true;
      } else {
        this.buyStacksLoadMore = false;
      }
      return postGropByUser(data.data);
    }).catch(() => { });
  }
  onLoadMoreBuyStacks() {
    this.buyStacksFilters.page++;
    this.buyStacks().then((data) => {
      this.buyStackList = this.buyStackList.concat(data);
    });
  }

  pastBuyStacks() {
    this.pastBuyStacksLoadMore = true;
    return this.stackService.getBuyerPost(this.pastBuyStacksFilters).then((data) => {
      this.pastBuyStacksLoading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.pastBuyStacksLoadMore = true;
      } else {
        this.pastBuyStacksLoadMore = false;
      }
      return postGropByUser(data.data);
      //this.pastBuyStackList = data.data;
    }).catch(() => { });
  }
  onLoadMorepastBuyStacks() {
    this.pastBuyStacksFilters.page++;
    this.pastBuyStacks().then((data) => {
      this.pastBuyStackList = this.pastBuyStackList.concat(data);
    });
  }

}
