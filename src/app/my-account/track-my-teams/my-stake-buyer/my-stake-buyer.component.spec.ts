import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyStakeBuyerComponent } from './my-stake-buyer.component';

describe('MyStakeBuyerComponent', () => {
  let component: MyStakeBuyerComponent;
  let fixture: ComponentFixture<MyStakeBuyerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyStakeBuyerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyStakeBuyerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
