import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrackMyTeamsRoutingModule } from './track-my-teams-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MyStakeBuyerComponent } from './my-stake-buyer/my-stake-buyer.component';


@NgModule({
  declarations: [MyStakeBuyerComponent],
  imports: [
    CommonModule,
    SharedModule,
    TrackMyTeamsRoutingModule
  ]
})
export class TrackMyTeamsModule { }
