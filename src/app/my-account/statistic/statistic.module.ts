import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticComponent } from './statistic/statistic.component';
import { StatisticRoutingModule } from './statistic-routing.module';
import { ChartComponent } from './chart/chart.component';
import { ChartsModule } from 'ng2-charts';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [StatisticComponent, ChartComponent],
  imports: [
    CommonModule,
    StatisticRoutingModule,
    ChartsModule,
    NgbDropdownModule
  ]
})
export class StatisticModule { }
