import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/auth/user.service';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.scss']
})
export class StatisticComponent implements OnInit {

  statisticsList: any;
  stackHttpResponse: any = {};
  
  constructor(
    private userService: UserService,
  ) {
    this.getStatistics();
   }

  ngOnInit() {
  }

  getStatistics() {
    this.userService.getStatistics().then((data: any) => {
      this.statisticsList = data;
    }).catch((error) => {
      this.stackHttpResponse = error;
    })
  }

}
