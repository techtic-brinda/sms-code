import { Component, OnInit, ViewChild, Input } from '@angular/core';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { Label, BaseChartDirective, Color } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { UserService } from 'src/app/shared/services/auth/user.service';
import * as _ from 'underscore';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { StackService } from 'src/app/shared/services/stack/stack.service';
import { AppInitService } from 'src/app/shared/services/app-init.service';
import * as moment from 'moment';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  

  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Investment'},
    { data: [], label: 'Won/lost'}
  ];
  public lineChartBalanceData: ChartDataSets[] = [
    { data: [], label: 'Balance' },
  ];
  public lineChartLabels: Label[];
  public lineChartBalanceLabels: Label[];
  public lineChartOptions: ChartOptions  = {
    responsive: true,
    scales: {
      xAxes: [{
        scaleLabel: {
          labelString: 'Day of Month',
          display: true
        },
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          scaleLabel: {
            labelString: 'Amount In USD',
            display: true
          }
        },
      ],
    },
    legend:{
      position: "top",
      labels : {
        usePointStyle: true
      }
    },
    elements:{
      point:{
        radius : 5
      }
    }
  };
  public lineChartColors: Color[] = [
    { //Investment
      backgroundColor: '#73cba7',
      borderColor: '#73cba7',
      pointBackgroundColor: '#73cba7',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#73cba7'
    },
    { //Won/lost
      backgroundColor: '#ffd876',
      borderColor: '#ffd876',
      pointBackgroundColor: '#ffd876',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#ffd876'
    }
  ];
  public lineChartBalanceColors: Color[] = [
    { // Balance
      backgroundColor: '#0BB5FF',//#A9D213',
      borderColor: '#0BB5FF',//#A9D213',
      pointBackgroundColor: '#0BB5FF',//#A9D213',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#0BB5FF',//#A9D213'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];
  monthList = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  
  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
  //minor
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes:[{
        scaleLabel: {
          labelString: 'Amount In USD',
          display: true
        },
        type: 'linear',
        ticks: {
          min: 0
        }
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          scaleLabel: {
            labelString: 'Users',
            display: true
          }
        },
      ],
    },
    legend: {
      position: "top",
      labels: {
        usePointStyle: true
      }
    }
  };
  // /FFAC5A
  public barChartColors: Color[] = [
    { //Investment
      backgroundColor: '#FFAC5A',
      borderColor: '#FFAC5A',
      pointBackgroundColor: '#FFAC5A',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#FFAC5A'
    }, 
    { //Total get amount
      backgroundColor: '#5EB5EF',
      borderColor: '#5EB5EF',
      pointBackgroundColor: '#5EB5EF',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#5EB5EF'
    },
  ];
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'horizontalBar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    { data: [], label: 'Investment', maxBarThickness:20 },
    { data: [], label: 'Winnings', maxBarThickness: 20  }
  ];
  
  currentDate: any;
  filter: any = {};
  barChartFilter: any = {};
  transationStatisticsList: any;
  balanceStatisticsList: any;
  stackHttpResponse: any = {};
  yearList: any = [];
  sport_types_list: any = [];
  first_year: any;
  first_month: any;
  user: any = {};

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private stackService: StackService,
    private appInitService: AppInitService,
  ) {
    this.currentDate = new Date();
    this.filter = {
      year: this.currentDate.getFullYear(),
      month: this.monthList[this.currentDate.getMonth()]
    }
   }

  ngOnInit() {
    this.authService.$user.subscribe((data) => {
      if (!data) {
        return
      }
      this.user = data;
      if (this.user.user_type == 'user'){
        this.barChartOptions.scales.yAxes[0].scaleLabel.labelString = 'My Sharks';
      }
      let created_at = moment(this.user.created_at).toDate();

      //let created_at = new Date(this.user.created_at);
      this.first_year = created_at.getFullYear();
      this.first_month = created_at.getMonth() + 1;
      for (let i = this.first_year; i <= this.currentDate.getFullYear(); i++) {
        this.yearList.push(i);
      }
      this.changeYear(this.filter.year);
     // this.getSportTypes();
      this.appInitService.$sport_types.subscribe((data) => this.sport_types_list = data);
      this.getTransationStatistics();
      this.getUserStatistics();
      
    });
  }

  getUserStatistics(){
    this.userService.getUserStatistics(this.barChartFilter).then((data: any) => {
      this.barChartData[0].data = [];
      this.barChartData[1].data = [];
      this.barChartLabels = [];
        data.userData.forEach(element => {
          this.barChartData[0].data.push(element.staked_amount);
          this.barChartData[1].data.push(element.won_amount);
          this.barChartLabels.push(element.full_name);
        });
    }).catch((error) => {
      this.stackHttpResponse = error;
    })
  }

  getTransationStatistics() {
    this.userService.getTransactionStatistics(this.filter).then((data: any) => {
      this.transationStatisticsList = data.transactionData;
      this.balanceStatisticsList = data.balanceData;

      let monthNumber:any = moment().month(this.filter.month).format("M");
      this.lineChartLabels = [];
      this.lineChartBalanceLabels = [];
      let lastDate = new Date(this.filter.year, monthNumber, 0).getDate();
      this.lineChartData[0].data = [];
      this.lineChartData[1].data = [];
      this.lineChartBalanceData[0].data = [];

      for (let index = 1; index <= lastDate; index++) {

        this.lineChartLabels.push(index.toString());
        if (this.transationStatisticsList.length) {
          let f = _.findIndex(this.transationStatisticsList, {
            day: index.toString()
          });
          if (f >= 0) {
            this.lineChartData[0].data.push(this.transationStatisticsList[f].staked_amount);
            this.lineChartData[1].data.push(this.transationStatisticsList[f].won_amount);
          } else {
            this.lineChartData[0].data.push(null);
            this.lineChartData[1].data.push(null);
          }
        }
        
        if (this.balanceStatisticsList.length) {
          let f1 = _.findIndex(this.balanceStatisticsList, {
            day: index.toString()
          });

          if (f1 >= 0) {
            this.lineChartBalanceLabels.push(index.toString());
            this.lineChartBalanceData[0].data.push(this.balanceStatisticsList[f1].closing_balance);
           }else if (index == lastDate || index == 1) {
            this.lineChartBalanceLabels.push(index.toString());
            this.lineChartBalanceData[0].data.push(null);
          }
        }
      }
    }).catch((error) => {
      this.stackHttpResponse = error;
    })
  }

  changeYear(year) {
    this.monthList = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    if (this.filter.year == this.currentDate.getFullYear()) {
      this.monthList.splice((this.first_month + 1), 11);
      this.monthList.splice(0, (this.first_month - 1));
    }
    this.filter.year = year;
    this.getTransationStatistics();
  }

  changeMonth(month) {
    this.filter.month = month;
    this.getTransationStatistics();
  }

  changeSportTypes(type){
    this.barChartFilter = (type == 'all') ? {} : type;
    this.getUserStatistics();
  }

}
