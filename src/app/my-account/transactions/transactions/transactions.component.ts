import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { UserService } from 'src/app/shared/services/auth/user.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  list_transation_history: any[] = [];
  transationRequest: any = {
    page: 0,
    limit: 20
  };
  loadMore: boolean = false;
  loading: boolean = true;

  constructor(
    private authService: AuthService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.authService.$user.subscribe((data) => {
      if (!data) {
        return
      }
        this.getTransationHistory().then((data: any) => {
          this.list_transation_history = data;
          console.log('history', this.list_transation_history);
          
        });
    });
  }
  onLoadMore() {
    this.transationRequest.page++;
    this.getTransationHistory().then((data) => {
      this.list_transation_history = this.list_transation_history.concat(data);
    });
  }
  // reload() {
  //   this.getTransationHistory();
  // }


  getTransationHistory() {
    return this.userService.getTransationHistory(this.transationRequest).then((data) => {
      this.loading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.loadMore = true;
      } else {
        this.loadMore = false;
      }
      return data.data;
    });
  }
}
