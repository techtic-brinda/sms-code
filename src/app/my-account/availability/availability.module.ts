import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullCalendarModule } from '@fullcalendar/angular';
import { AvailabilityRoutingModule } from './availability-routing.module';
import { CalendarComponent } from './calendar/calendar.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [CalendarComponent],
  imports: [
    CommonModule,
    AvailabilityRoutingModule,
    SharedModule,
    FullCalendarModule 
  ]
})
export class AvailabilityModule { }
