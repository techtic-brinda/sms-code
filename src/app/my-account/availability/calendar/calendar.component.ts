import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import * as moment from 'moment';
import { UserAvailabilityService } from 'src/app/shared/services/auth/user-availability.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as _ from 'underscore';
import { AuthService } from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  @ViewChild('calendar', { static: false, read: FullCalendarComponent }) calendarComponent: any;
  @ViewChild('alertInfo', { static: false }) alertInfo: NgbModal;
  @ViewChild('deleteModel', { static: false }) deleteModel: NgbModal;

  calendarPlugins = [dayGridPlugin, timeGridPlugin, interactionPlugin];

  selectedEvent:any = null
  user: any = {};
  isMobile: boolean;

  constructor(
    private userAvailabilityService: UserAvailabilityService,
    private modalService: NgbModal,
    private authService: AuthService,
  ) {
    this.authService.$user.subscribe((data) => {
      if (!data) {
        return
      }
      this.user = data;
    });
  }

  @HostListener('window:resize')
  onResize() {
    if (window.innerWidth >= 767) {
      this.isMobile = false;
      this.calendarComponent.calendar.changeView('timeGridWeek');
    } else {
      this.isMobile = true;
      this.calendarComponent.calendar.changeView('timeGridDay');
    }
  }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.onResize();
  }
  events = ({ start, end }) => {
    return this.userAvailabilityService.getEvent({ start, end });
  }

  selectOverlap = (event) => {
    return event.rendering === 'background';
  }

  selectAllow = (info) => {
    return !moment(info.start).isBefore(moment()) && this.isValidEvent(info.start, info.end);
  }

  addEvent({ _id, start, end }: any) {
    return this.userAvailabilityService.addUpdateEvent({ _id, start, end }).then(data => {
      this.calendarComponent.calendar.refetchEvents();
      this.calendarComponent.calendar.unselect();
    });
  }

  isValidDrop(data) {
  }

  dateClick(data) {
    this.calendarComponent.calendar.changeView('timeGridWeek', data.date);
  }

  select({ start, end }) {
    this.addEvent({ start, end });
  }
  
  eventResize({ event: { id, start, end } }) {
    this.addEvent({ _id: id, start, end });
  }
  
  eventDrop({ event: { id, start, end } }) {
    this.addEvent({ _id: id, start, end });
  }

  openInfo( data ) {
    let { event } = data
    this.selectedEvent = event;
    this.modalService.open(this.alertInfo, { ariaLabelledBy: 'modal-basic-title' }).result.then(() => { }, () => { });
  }

  openDeleteConfim( data ) {
    this.modalService.open(this.deleteModel, { ariaLabelledBy: 'modal-basic-title'}).result.then(() => { }, () => { });
  }

  clearSlectedEvent() {
    this.selectedEvent = null;
  }

  deleteEvent() {
    this.userAvailabilityService.delete(this.selectedEvent.id).then(()=>{
      this.calendarComponent.calendar.refetchEvents();
      this.clearSlectedEvent();
      this.modalService.dismissAll();
    });
  }
  

  isValidEvent = (start, end) => {
    start = moment(start);
    end = moment(end);
    let events = this.calendarComponent.calendar.getEvents();
    if(events.length == 0){
      return true;
    }
    let valid: any[] = _.filter(events, (event) => {
      return event.extendedProps.status === "available";
    })
    return valid.length > 0;
  };

}
