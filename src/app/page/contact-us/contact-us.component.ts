import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, AbstractControl } from '@angular/forms';
import { HelperService } from 'src/app/shared/services/helper.service';
import { PageService } from 'src/app/shared/services/page.service';
import { Email } from 'src/app/shared/modules/validation';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  contactUs: any = {};
  // contactUs: any = {
  //   name : 'test',
  //   email : 'test@gmail.com',
  //   subject: 'test subject',
  //   message: 'test message test message test message test message test message test message ',
  // };

  @ViewChild('contactUsForm', { static: false }) contactUsFormDir : NgForm;
  isLoading: boolean = false;
  submitted: boolean = true;
  loading: boolean = false;
  contactUsInfo:any;

  constructor(
    private helperService: HelperService,
    private pageService: PageService,
  ) { }

  ngOnInit() {
    this.getSetting();
  }
  sendContactUs(form: NgForm){
    if (form.valid) {
      this.isLoading = true;
      this.pageService.sendContactUs(this.contactUs).then((data) => {
        form.resetForm();
        this.isLoading = false;
        this.contactUs = {};
        this.helperService.successMessage(data, 'Your message has been send to admin. we will contact you soon.');
      }).catch((error) => {
        this.helperService.errorMessage(error, 'Some error occured, Please try again.');
      });
    }
  }

  getSetting(){
    this.loading = true;
    this.pageService.getSettings().then((data) => {
      this.contactUsInfo = data;
      this.loading = false;
    }).catch((error) => {
      this.loading = false;
      this.helperService.errorMessage(error, 'Some error occured, Please try again.');
    })
  }

}
