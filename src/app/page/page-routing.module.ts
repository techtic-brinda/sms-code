import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FaqComponent } from './faq/faq.component';
import { PageComponent } from './page/page.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PageResolver } from './resolver/page.resolver';

const routes: Routes = [
  {
    path: "contact-us",
    component: ContactUsComponent
  },
  {
    path: "faqs",
    //resolve: [FAQResolver],
    component: FaqComponent
  },
  {
    path: "tutorials",
    //resolve: [TutorialsResolver],
    component: TutorialComponent
  },
  {
    path: "404",
    component: PageNotFoundComponent
  },
  {
    path: ":slug",
    resolve: {page : PageResolver},
    component: PageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
