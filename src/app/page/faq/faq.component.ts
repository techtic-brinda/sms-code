import { Component, OnInit } from '@angular/core';
import { PageService } from 'src/app/shared/services/page.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HelperService } from 'src/app/shared/services/helper.service';
import { SupportTicketService } from 'src/app/shared/services/supportTicket.service';
import { NgForm } from '@angular/forms';
import { _ } from 'underscore';
import { Router } from '@angular/router';


@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  faqs: any = [];
  supportTicket: any = {};
  supportTickets: any = [];
  isLoading: boolean = false;
  fileType: String;
  fileName: String;

  constructor(
    private supportTicketService: SupportTicketService,
    private router: Router,
    private pageService: PageService,
    private modalService: NgbModal,
    private helperService: HelperService,
  ) { }

  ngOnInit() {
    this.pageService.getFaqs().then((data) => {
      this.faqs = data.data
    })
  }

  openModel(content) {
    this.supportTicket = [];
    this.fileName = '';
    this.fileType = '';
    return this.modalService.open(content, { size: 'lg' });
  }

  openTicket(form: NgForm) {
    let request = _.pick(this.supportTicket, ["message", "file", "subject"]);
    if (!request.message || !request.subject) {
      this.helperService.errorMessage('', `Please enter all required field.`);
      return false;
    }
    if (request.subject.length > 150) {
      this.helperService.errorMessage('', `Please enter maximum 150 char for subject field.`);
      return false;
    }
    this.isLoading = true;
    this.supportTicketService.openTicket(request).then((data) => {
      this.helperService.successMessage('', `Support  ticket has been created successsfully.`);
      this.clearContent();
      this.supportTickets.unshift(data.data);
      form.reset();
      this.isLoading = false;
      this.router.navigate(['support-ticket']);
    }).catch((error) => {
      this.isLoading = false;
      this.helperService.errorMessage(error, `Error, while submit support ticket.`);
    })
  }

  readFile(fileEvent: any) {
    const file = fileEvent.target.files[0];
    this.fileName = file.name;
    this.fileType = this.fileName.substr(this.fileName.lastIndexOf('.') + 1);
  }

  setFile($event) {
    if ($event.target.files.length > 0) {
      var fileToLoad = $event.target.files[0];
      var reader: FileReader = new FileReader();
      reader.onloadend = (readerEvt: any) => {
        this.supportTicket.file = readerEvt.target.result;
      }
      reader.readAsDataURL(fileToLoad);
      this.supportTicket.file = $event.target.files;
    }
  }

  clearContent() {
    this.supportTicket = {
      message: ""
    };
    this.modalService.dismissAll();
  }

}
