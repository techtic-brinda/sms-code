import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageRoutingModule } from './page-routing.module';
import { PageComponent } from './page/page.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FaqComponent } from './faq/faq.component';
import { SharedModule } from '../shared/shared.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { CKEditorModule } from 'ckeditor4-angular';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [PageComponent, ContactUsComponent, FaqComponent, PageNotFoundComponent, TutorialComponent],
  imports: [
    NgbAccordionModule,
    CommonModule,
    SharedModule,
    PageRoutingModule,
    CKEditorModule,
    MatIconModule
  ]
})
export class PageModule { }
