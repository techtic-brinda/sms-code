import { Component, OnInit } from '@angular/core';
import { PageService } from 'src/app/shared/services/page.service';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss']
})
export class TutorialComponent implements OnInit {
  tutorials: any = [];
  tutorialsRequest: any = {
    page: 0,
    limit: 100
  };
  tutorialsLoadMore: boolean = false;
  tutorialsLoading: boolean = true;

  constructor(
    private pageService: PageService,
  ) { }

  ngOnInit() {
    this.getTutorial().then((data: any) => {
      this.tutorials = data;
    });
  }

  getTutorial(){
    this.tutorialsLoadMore = true;
    return this.pageService.getTutorial(this.tutorialsRequest).then((data) => {
      this.tutorialsLoading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.tutorialsLoadMore = true;
      } else {
        this.tutorialsLoadMore = false;
      }
      return data.data;
    }).catch(() => { });
  }

  onLoadMoreTutorial() {
    this.tutorialsRequest.page++;
    this.getTutorial().then((data) => {
      this.tutorials = this.tutorials.concat(data);
    });
  }
}