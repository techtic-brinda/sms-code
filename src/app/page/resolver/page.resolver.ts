import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { isPlatformServer } from '@angular/common';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { of, Observable, from } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { PageService } from 'src/app/shared/services/page.service';

@Injectable({
    providedIn: 'root'
})
export class PageResolver implements Resolve<any> {
    constructor(
        private pageService: PageService,
        private router: Router,
        private transferState: TransferState,
       // @Inject(PLATFORM_ID) private platformId,
    ) { }

    resolve(
        route: ActivatedRouteSnapshot
    ): Observable<any> {

        const data: any = route.params


        const PAGE_KEY = makeStateKey<any>('page-' + data.slug);
        if (this.transferState.hasKey(PAGE_KEY)) {
            const course = this.transferState.get<any>(PAGE_KEY, null);
            this.transferState.remove(PAGE_KEY);
            return of(course);
        } else {
            return from(this.pageService.getPage(data.slug))
                .pipe(
                    catchError((error: any) => {
                        const message = `Retrieval error: ${error}`;
                        this.router.navigateByUrl('404', { skipLocationChange: true });
                        return of(message);
                    }),
                    tap((resp: any) => {
                        //this.transferState.set(PAGE_KEY, resp);
                        //const path = this.location.path();
                        //this._seoService.updateMeta(resp);
                        // if (isPlatformServer(this.platformId)) {
                        //     this.transferState.set(PAGE_KEY, resp);
                        // }
                    })
                );
        }
    }

}
