import gql from 'graphql-tag';




const PaginationData = gql`
        fragment PaginationData on PaginationMeta {
            from
            to
            total
            per_page
            current_page
            last_page
        }
    `


const UserBasic = gql`
        fragment UserBasic on User {
            _id
            first_name
            last_name
            profile_pic
        }
    `

const UserData = gql`
        fragment UserData on User {
            ... UserBasic
            email
            dob
            gender
            status
            user_type
            wallet_amount
            created_at
            updated_at
            about
            sport_type_id
            coaching_amount
        }
        ${UserBasic}
    `

const PostBasic = gql`
        fragment PostBasic on Post {
            _id
            description
            title
            closing_time
            sold_percentage
            sold_total
            total_staked_amount
            total_won_amount
            game_amount
            player_amount
            sport_type_id
            user_id
            is_streaming
            stream_url
            stream_type
            status
            paid_to_player
            markup_amount
            user {
                ... UserBasic
            }
        }
        ${UserBasic}
    `

const TransactionData = gql`
        fragment TransactionData on Transaction {
            _id
            user_id
            amount
            closing_balance
            description
            transactions_type
            data
            getway_transactions_id
            getway_type
            status
            created_at
        }
        ${UserBasic}
    `


const EventData = gql`
        fragment EventData on Events {
            id
            start
            end
            status
            color
            amount
        }
    `

const MessageData = gql`
        fragment MessageData on Message {
            _id
            message
            user_id
            user {
                ... UserBasic
            }
            contact_id
            contact {
                ... UserBasic
            }
            status
            data
            created_at
        }
        ${UserBasic}
    `


export const Fragments = { UserBasic, UserData, PostBasic, TransactionData, EventData, MessageData, PaginationData };
