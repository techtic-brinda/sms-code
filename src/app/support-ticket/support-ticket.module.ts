import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SupportTicketRoutingModule } from './support-ticket-routing.module';
import { SupportTicketComponent } from './support-ticket/support-ticket.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SupportTicketViewComponent } from './support-ticket-view/support-ticket-view.component';
import { CKEditorModule } from 'ckeditor4-angular';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [SupportTicketComponent, SupportTicketViewComponent],
  imports: [
    CommonModule,
    SharedModule,
    SupportTicketRoutingModule,
    CKEditorModule,
    MatIconModule 
  ]
})
export class SupportTicketModule { }
