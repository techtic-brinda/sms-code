import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupportTicketComponent } from './support-ticket/support-ticket.component';
import { SupportTicketViewComponent } from './support-ticket-view/support-ticket-view.component';

const routes: Routes = [
    {
        path: "",
        component: SupportTicketComponent
    },
    {
        path: ":id",
        component: SupportTicketViewComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SupportTicketRoutingModule { }
