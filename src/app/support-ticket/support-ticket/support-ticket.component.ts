import { Component, OnInit } from '@angular/core';
import { SupportTicketService } from 'src/app/shared/services/supportTicket.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { HelperService } from 'src/app/shared/services/helper.service';
import { _ } from 'underscore';

@Component({
  selector: 'app-support-ticket',
  templateUrl: './support-ticket.component.html',
  styleUrls: ['./support-ticket.component.scss']
})
export class SupportTicketComponent implements OnInit {

  loading: boolean = true;
  loadMore: boolean = false;
  supportTicketRequest: any = {
    page: 0,
    limit: 20,
    type: ""
  };
  supportTickets: any[] = [];
  supportTicket: any = {};
  isLoading: boolean = false;
  fileType: String;
  fileName: String;

  constructor(
    private supportTicketService: SupportTicketService,
    private router: Router,
    private modalService: NgbModal,
    private helperService: HelperService,
  ) { }

  ngOnInit() {
    this.getSupportTicketList().then((data: any) => {
      this.supportTickets = data;
    });
  }

  readFile(fileEvent: any) {
    const file = fileEvent.target.files[0];
    this.fileName = file.name;
    this.fileType = this.fileName.substr(this.fileName.lastIndexOf('.') + 1);
  }

  getSupportTicketList() {
    this.loading = true;
    return this.supportTicketService.getList(this.supportTicketRequest).then((data) => {
      this.loading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.loadMore = true;
      } else {
        this.loadMore = false;
      }
      return data.data;
    })
  }
  onLoadMore() {
    this.supportTicketRequest.page++;
    this.getSupportTicketList().then((data) => {
      this.supportTickets = this.supportTickets.concat(data);
    });
  }
  redirect(id) {
    this.router.navigate(['support-ticket',id]);
  }
  applyFilter(type){
    this.supportTicketRequest.page = 0;
    this.supportTicketRequest.limit = 20;
    this.supportTicketRequest.type = type;
    this.getSupportTicketList().then((data: any) => {
      this.supportTickets = data;
    });
  }

  openModel(content) {
    this.supportTicket = [];
    this.fileName = '';
    this.fileType = '';
    return this.modalService.open(content, { size: 'lg' });
  }
  openTicket(form: NgForm){
    let request = _.pick(this.supportTicket, ["message", "file", "subject"]);
    if (!request.message || !request.subject){
      this.helperService.errorMessage('', `Please enter all required field.`);
      return false;
    }
    if (request.subject.length > 150 ){
      this.helperService.errorMessage('', `Please enter maximum 150 char for subject field.`);
      return false;
    }
    this.isLoading = true;
    this.supportTicketService.openTicket(request).then((data) => {
      this.helperService.successMessage('', `Supprot ticket has been created successsfully.`);
      this.clearContent();
      this.supportTickets.unshift(data.data);
      form.reset();
      this.isLoading = false;
    }).catch((error) => {
      this.isLoading = false;
      this.helperService.errorMessage(error, `Error, while send reply.`);
    })
  }

  setFile($event) {
    if ($event.target.files.length > 0) {
      var fileToLoad = $event.target.files[0];
      var reader: FileReader = new FileReader();
      reader.onloadend = (readerEvt: any) => {
        this.supportTicket.file = readerEvt.target.result;
      }
      reader.readAsDataURL(fileToLoad);
      this.supportTicket.file = $event.target.files;
    }
  }

  clearContent() {
    this.supportTicket = {
      message: ""
    };
    this.modalService.dismissAll();
  }

  // change({ editor }) {
    //(change)="change($event)"
  //   const EditorData = editor.getData();
  //   this.supportTicket.message = EditorData;
  //  // this.supportTicket.get('description').setValue(EditorData);
  // }

}
