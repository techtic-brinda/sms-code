import { Component, OnInit } from '@angular/core';
import { SupportTicketService } from 'src/app/shared/services/supportTicket.service';
import { HelperService } from 'src/app/shared/services/helper.service';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import * as _ from 'underscore';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-support-ticket-view',
  templateUrl: './support-ticket-view.component.html',
  styleUrls: ['./support-ticket-view.component.scss']
})
export class SupportTicketViewComponent implements OnInit {

  loading: boolean = false;
  supportTicketDetail: any;
  supportTicket: any = {};
  openReply: boolean = false;
  isLoading: boolean = false;

  constructor(
    public supportTicketService: SupportTicketService,
    private helperService: HelperService,
    public route: ActivatedRoute,
    private modalService: NgbModal,
  ) {
    route.params.subscribe((params) => {
      this.getSupportTicket(params['id']);
    });
  }

  ngOnInit() {
  }

  getSupportTicket(id) {
    this.loading = true;
    this.supportTicketService.getSupportTicket(id).then(data => {
      this.loading = false;
      this.supportTicketDetail = data;
    }).catch(err => {
      this.loading = false;
      this.helperService.errorMessage(err, "You can not open the page.");
    });
  }

  downloadFile(file) {
    var file_path = file.file;
    let a = document.createElement('a');
    a.href = file_path;
    a.target = '_blank';
    a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
  openReplyForm(){
    this.openReply = !this.openReply;
  }

  openTicket(form: NgForm) {
    let request = _.pick(this.supportTicket, ["message", "file"]);
    if (!request.message) {
      this.helperService.errorMessage('', `Please enter message.`);
      return false;
    }
    this.isLoading = true;
    request._id = this.supportTicketDetail._id;
    this.supportTicketService.addReplyToMsg(request).then((data) => {
      this.supportTicketDetail.messages.unshift(data);
      this.helperService.successMessage('', `Reply has been send successfully.`);
      this.clearContent();
      this.modalService.dismissAll();
      this.isLoading = false;
    }).catch((error) => {
      this.isLoading = false;
      this.helperService.errorMessage(error, `Error, while send reply.`);
    })
  }
  clearContent() {
    this.supportTicket = {
      message: "",
      file: ""
    };
  }

  setFile($event) {
    if ($event.target.files.length > 0) {
      var fileToLoad = $event.target.files[0];
      var reader: FileReader = new FileReader();
      reader.onloadend = (readerEvt: any) => {
        this.supportTicket.file = readerEvt.target.result;
      }
      reader.readAsDataURL(fileToLoad);
      this.supportTicket.file = $event.target.files;
    }
  }

  markAsClosed(status?: any) {
    let request = {
      id: this.supportTicketDetail._id,
      status: status,
    }
    this.supportTicketService.changeStatus(request).then((data) => {
      this.helperService.successMessage('', `Ticket has been ${status} successfully.`);
      this.supportTicketDetail = data;
      this.modalService.dismissAll();
    }).catch((error) => {
      this.helperService.errorMessage(error, `Error, while close ticket.`);
    })
  }

  openConfirmModel(content, type) {
    this.modalService.open(content, { size: type });
  }
  
}
