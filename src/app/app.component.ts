import { Component, Inject, PLATFORM_ID } from '@angular/core';
import { AuthService } from './shared/services/auth/auth.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { QueryRef } from 'apollo-angular';
import { ChatService } from './shared/services/chat.service';
import { isPlatformBrowser } from '@angular/common';
import * as firebase from "firebase/app";
import '@firebase/messaging';
import { UserService } from './shared/services/auth/user.service';
import { Config } from './config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  messageQuery: QueryRef<any>;
  user: any;
  firebaseObj: any;
  messaging: any;

  constructor(
    private authService: AuthService,
    private chatService: ChatService,
    private userService: UserService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId
  ) {
    //basic configuration from firebase general settings
   
  }

  ngOnInit() {

    

    //this.authService.init();

    if (isPlatformBrowser(this.platformId)){
      this.firebaseObj = firebase.initializeApp(Config.firebase);
      this.router.events.pipe(
        filter(event => event instanceof NavigationEnd)
      ).subscribe(() => {
        window.scrollTo(0, 0)
      });
    }

    this.authService.$user.subscribe((user) => {
      if (user){
        this.user = user;
        this.chatService.init();

        if (this.user.user_type == 'user'){
          this.messaging = firebase.messaging();

          //get 'Key pair' from firebase => settings => cloud messaging => Web configuration
          this.messaging.usePublicVapidKey(Config.publicVapidKey);
          //initiat the firebase app
          this.requestPermissionToGenerateOne();
        }
      }
    });
  }

 
  //get permission from browser to allow notification
  requestPermissionToGenerateOne(){
    Notification.requestPermission().then((permission) => {
      if (permission === 'granted') {
        console.log('Notification permission granted.');
        this.messagingGetToken();
        // TODO(developer): Retrieve an Instance ID token for use with FCM.
        // ...
      } else {
        console.log('Unable to get permission to notify.');
      }
    });
  }

  //get tocket to send notification on perticular device
  messagingGetToken(){
    this.messaging.getToken().then((currentToken) => {
      console.log('messagingGetToken', currentToken);
      if (currentToken) {
        //save into database
        this.userService.saveFirebaseToken(currentToken);
        // this.messaging.onMessage((payload) => {
        //   // ...
        // });
        // updateUIForPushEnabled(currentToken);
      } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        // this.requestPermissionToGenerateOne();
        // Show permission UI.
        // updateUIForPushPermissionRequired();
        // setTokenSentToServer(false);
      }
    }).catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
      // showToken('Error retrieving Instance ID token. ', err);
      // setTokenSentToServer(false);
    });
  }

  //get refresh tocket to send notification on perticular device
  messagingRefeshGetToken(){
    this.messaging.onTokenRefresh(() => {
      this.messaging.getToken().then((refreshedToken) => {
        console.log('Token refreshed.');
        // Indicate that the new Instance ID token has not yet been sent to the
        // app server.
        // setTokenSentToServer(false);
        // Send Instance ID token to app server.
        // sendTokenToServer(refreshedToken);
        // ...
      }).catch((err) => {
        console.log('Unable to retrieve refreshed token ', err);
        // showToken('Unable to retrieve refreshed token ', err);
      });
    });
  }

  //  this.messaging.setBackgroundMessageHandler(function (payload) {
  //    // Customize notification here
  //    const notificationTitle = 'Background Message Title';
  //    const notificationOptions = {
  //      body: 'Background Message body.',
  //    };

  //    // return self.registration.showNotification(notificationTitle,notificationOptions);
  //  });

}
