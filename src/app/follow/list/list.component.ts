import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { UserService } from 'src/app/shared/services/auth/user.service';
import { HelperService } from 'src/app/shared/services/helper.service';

@Component({
  selector: 'follow-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  currentUser: any;
  total: any;
  folllow_list: any[] = [];
  follow_request: any = {
    page: 0,
    limit: 10
  };
  isLoading = false;
  loading = false;
  loadMore = false;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private helperService: HelperService,

  ) {}

  async ngOnInit() {
    this.authService.$user.subscribe((data) => {
      if (!data) {
        return
      }
      this.currentUser = data;
    });
    this.followList().then((data: any) => {
      this.folllow_list = data;
    });
  }

  followList(){
    this.loading = true;
    return this.userService.followingList(this.follow_request).then((data) => {
      this.loading = false;
      this.total = data.meta.total;
      if (data.meta.last_page > data.meta.current_page) {
        this.loadMore = true;
      } else {
        this.loadMore = false;
      }
      return data.data;
    })
  }

  onLoadMore() {
    this.follow_request.page++;
    this.followList().then((data) => {
      this.folllow_list = this.folllow_list.concat(data);
    });
  }
  
  userFollow(player_id, index) {
      this.isLoading = true;
      this.userService.setFollow(player_id).then((data: any) => {
      this.total = this.total - 1;      
      this.isLoading = false;
      this.folllow_list.splice(index, 1);
    }).catch((error) => {
      this.isLoading = false;
      this.helperService.errorMessage(error, "Some error occurred. Please try some time later.");
    })
  }

}
