import { NgModule, PLATFORM_ID, Inject } from '@angular/core';
import { ApolloModule, APOLLO_OPTIONS, Apollo } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { environment } from 'src/environments/environment';
import { ApolloLink, concat, split } from 'apollo-link';
import { WebSocketLink } from 'apollo-link-ws';
import { getOperationAST } from 'graphql/utilities/getOperationAST';
import { isPlatformBrowser } from '@angular/common';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { AppStorage } from './shared/services/cookies/app-storage';

const STATE_KEY = makeStateKey<any>('apollo.state');

const middleware = new ApolloLink((operation, forward) => {
  const token = localStorage.getItem('token');
 
  if (token) {
    operation.setContext(({ headers = {} }) => ({
      headers: {
        ...headers,
        Authorization: `Bearer ${token}`,
      }
    }));
  }
  return forward(operation);
});

// export function createApollo(httpLink: HttpLink, platformId: Object) {

//   const isBrowser = isPlatformBrowser(platformId);
//   const uri = environment.graphQlUrl + '/graphql';
//   const wsUrl = environment.graphQlWsUrl;
//   const http = httpLink.create({ uri });
 
//   let subscriptionLink;
//   let link;
  
//   if (isBrowser){
//     subscriptionLink = new WebSocketLink({
//       uri: wsUrl,
//       options: {
//         reconnect: true,
//         connectionParams: {
//           authToken: localStorage.getItem('token') || null
//         }
//       }
//     });

//     link = split(
//       operation => {
//         const operationAST = getOperationAST(operation.query, operation.operationName);
//         return !!operationAST && operationAST.operation === 'subscription';
//       },
//       subscriptionLink,
//       http
//     );
//   }


//   return {
//     cache: new InMemoryCache(),
//     ...(isBrowser ? { ssrForceFetchDelay: 200, link: concat(middleware, link) } : { ssrMode: true, link: concat(middleware, http) })
//   };
// }


@NgModule({
  exports: [ApolloModule, HttpLinkModule],
  imports : [
    HttpLinkModule
  ],
  // providers: [
  //   {
  //     provide: APOLLO_OPTIONS,
  //     useFactory: createApollo,
  //     deps: [HttpLink, PLATFORM_ID],
  //   },
  // ],
})
export class GraphQLModule {

  cache: InMemoryCache;

  constructor(
    apollo: Apollo,
    httpLink: HttpLink,
    @Inject(PLATFORM_ID) platform: any,
    private readonly transferState: TransferState,
    @Inject(AppStorage) private appStorage: Storage,
  ) {
    this.cache = new InMemoryCache();

    const isBrowser = isPlatformBrowser(platform);

    const uri = environment.graphQlUrl + '/graphql';
    const wsUrl = environment.graphQlWsUrl;
    const http = httpLink.create({ uri });

    let subscriptionLink;
    let link;


    if (isBrowser) {
      subscriptionLink = new WebSocketLink({
        uri: wsUrl,
        options: {
          reconnect: true,
          connectionParams: {
            authToken: this.appStorage.getItem('token') || null
          }
        }
      });

      link = split(
        operation => {
          const operationAST = getOperationAST(operation.query, operation.operationName);
          return !!operationAST && operationAST.operation === 'subscription';
        },
        subscriptionLink,
        http
      );
    }

    apollo.create({
      cache: this.cache,
      ...(isBrowser ? { ssrForceFetchDelay: 200, link: concat(middleware, link) } : { ssrMode: true, link: concat(middleware, http) })
    });

  
    if (isBrowser) {
      this.onBrowser();
    } else {
      this.onServer();
    }
  }

  onServer() {
    this.transferState.onSerialize(STATE_KEY, () => {
      return this.cache.extract();
    });
  }

  onBrowser() {
    const state = this.transferState.get<any>(STATE_KEY, null);
    this.cache.restore(state);
  }
}
