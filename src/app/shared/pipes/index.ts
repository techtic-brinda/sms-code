import { AssetsPath } from './image';
import { WrapPTag } from "./wrap-p-tag";
import { FilterPipe } from './filter.pipe';
import { UpdateAnchor, SafeHtmlPipe, TruncatePipe, Nl2BrPipe } from './text';
import { StrToDatePipe } from './str-to-date.pipe';

const Pipes = [
    AssetsPath,
    FilterPipe,
    WrapPTag,
    UpdateAnchor,
    SafeHtmlPipe,
    TruncatePipe,
    StrToDatePipe,
    Nl2BrPipe
];

export { Pipes };
