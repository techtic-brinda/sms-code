import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class PayPalService {

	constructor(
		private apollo: Apollo,
	) { }

	charge(amount: number, data: any) {

		return this.apollo.mutate({
    		mutation: gql`
    			mutation addFund($payment_gateway:String!, $payment_data:JSON, $amount:Float){
    			data : addFund(input: {
    				payment_gateway: $payment_gateway
    				payment_data: $payment_data
    				amount: $amount
    			}){
    				data
    				status
    				message
    			}
    			}
    		`,
    		variables: {
    			payment_gateway: "paypal",
    			payment_data: data,
    			amount: parseFloat(amount + ""),
    		}
		})
		.pipe(
			map((resp: any) => resp.data.data),
			// catchError((error) => {
			//     return throwError(JSON.parse(JSON.stringify(error)));
			// })
		)
		.toPromise()
		.then((data: any) => {
			return data;
		}).catch((error) => {
			error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
			throw error;
		})
	}

	withdraw(amount: number, email: string) {

		return this.apollo.mutate({
		mutation: gql`
			mutation withdraw($payment_gateway:String!, $email:String!, $amount:Float!){
			data : withdraw(input: {
				payment_gateway: $payment_gateway
				email: $email
				amount: $amount
			}){
				data
				status
				message
			}
			}
		`,
		variables: {
			payment_gateway: "paypal",
			email: email,
			amount: parseFloat(amount + ""),
		}
		})
		.pipe(
			map((resp: any) => {
				return resp.data.data;
            }),
			// catchError((error) => {
			//     return throwError(JSON.parse(JSON.stringify(error)));
			// })
		)
		.toPromise()
		.then((data: any) => {
			return data;
		}).catch((error) => {
			error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
			throw error;
		})
	}
}
