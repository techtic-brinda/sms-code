import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CryptoCurrencyService {
    constructor(
        private apollo: Apollo,
    ) { }

    addFund(data: any) {
        return this.apollo.mutate({
            mutation: gql`
    			mutation addFund($input:AddFundInput){
                    data : addFund(input: $input){
                        data
                        status
                        message
                    }
    			}
    		`,
            variables: {
                input: data
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }
}