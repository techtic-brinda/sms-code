import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StripeService {

  constructor(
    private apollo: Apollo,
  ) { }

  charge(token: string, amount: number) {
    return this.apollo.mutate({
      mutation: gql`
        mutation addFund($payment_gateway:String!, $token:String, $amount:Float){
          data : addFund(input: {
            payment_gateway: $payment_gateway
            token: $token
            amount: $amount
          }){
            data
            status
            message
          }
        }  
      `,
      variables: {
        payment_gateway: "stripe",
        token: token,
        amount: parseFloat(amount+"")
      }
    })
      .pipe(
        map((resp: any) => resp.data.data),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((data: any) => {
        return data;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }
}
