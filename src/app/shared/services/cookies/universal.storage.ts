import { Injectable, Inject } from '@angular/core';
import { REQUEST } from '@nguniversal/express-engine/tokens';
//import { CookieService } from '@gorniv/ngx-universal';

@Injectable()
export class UniversalStorage implements Storage {
    [index: number]: string;
    [key: string]: any;
    length: number;
    cookies: any;

    constructor(@Inject(REQUEST) request: any) {
      
        if (request === null) {
            this.cookies = {};
            return;
        }
        this.cookies = request.cookies;

        console.log({ cookies : this.cookies})
     }

    public clear(): void {
        this.cookies = [];
    }

    public getItem(key: string): string {
        return this.cookies[key];
    }

    public key(index: number): string {
        return this.cookies.propertyIsEnumerable[index];
    }

    public removeItem(key: string): void {
        delete this.cookies[key];
    }

    public setItem(key: string, data: string): void {
        this.cookies[key] = data;
    }
}