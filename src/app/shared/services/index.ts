import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth/auth.interceptor';
import { PageService } from './page.service';
import { AuthService } from './auth/auth.service';
import { UserService } from './auth/user.service';
import { StripeService } from './payment/stripe.service';
import { PayPalService } from './payment/pay-pal.service';
import { HelperService } from './helper.service';
import { NotificationService } from './notification.service';
import { RouteUtil } from './route';
import { UserAvailabilityService } from './auth/user-availability.service';
import { ChatService } from './chat.service';


const Services = [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    UserService,
    PageService,
    AuthService,
    StripeService,
    PayPalService,
    NotificationService,
    HelperService,
    UserAvailabilityService,
    RouteUtil,
    ChatService,
];

export { Services };
