import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiUrl } from '../utils';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PageService {

  constructor(
    private apollo: Apollo
  ) { }

  getFaqs() {
    return this.apollo.query({
      query: gql`
        query getFaq{
          data : getFaq{
            meta{
              total
            }
            data {
              _id
              question
              answer
            }
          }
        }   
      `,
    })
      .pipe(
        map((resp: any) => resp.data.data),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((data: any) => {
        return data;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  getPage(slug) {
    return this.apollo.query({
      query: gql`
        query getPageBySlug($slug: String!){
          data : getPageBySlug(slug: $slug){
            _id
            title
            slug
            content
            meta_title
            meta_description
            meta_keyword
          }
        }   
      `,
      variables: {
        slug
      }
    })
      .pipe(
        map((resp: any) => resp.data.data),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((data: any) => {
        return data;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  getTutorial(request) {
    return this.apollo.query({
      query: gql`
        query getTutorials($input: TutorialDataTableInput){
          data : getTutorials(input : $input){
            meta{
              total
              last_page
              current_page
            }
            data {
              _id
              title
              description
              file
              link
              created_at
              updated_at
            }
          }
        }
      `,
      variables: {
        input: request
      }
    })
      .pipe(
        map((resp: any) => resp.data.data),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((data: any) => {
        return data;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  sendContactUs(request: any) {
    return this.apollo.mutate({
      mutation: gql`
                mutation sendContactUS($request: SendContactUSInput!){
                    data : sendContactUS( input: $request){
                        message
                        data
                    }
                }
            `,
      variables: {
        request
      }
    })
      .pipe(
        map((resp: any) => {
          return resp.data.data;
        }),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((data: any) => {
        return data;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  getSettings(){
    return this.apollo.query({
        fetchPolicy: 'no-cache',
        query: gql`
              query getSettings{
                  data : getSettings
              }
          `,
      })
      .pipe(
        map((resp: any) => {
          return resp.data.data;
        }),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((data: any) => {
        return data;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })

  }

}
