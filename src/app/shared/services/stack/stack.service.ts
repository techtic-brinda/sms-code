import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Fragments } from 'src/app/graphql/fregments';


@Injectable({
    providedIn: 'root'
})
export class StackService {
    constructor(
        private apollo: Apollo
    ) {

    }

    addStack(request: any){
        return this.apollo.mutate({
            fetchPolicy: 'no-cache',
            mutation: gql`
                mutation addMyPost($input: PostsInput!){
                    data : addMyPost( input : $input){
                        data
                        status
                        message
                    }
                }
            `,
            context: {
                useMultipart: true
            },
            variables: {
                input: request
            }
        })
            .pipe(
                map((resp: any) => resp.data.data),
                // catchError((error) => {
                //     return throwError(JSON.parse(JSON.stringify(error)));
                // })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    deleteStack(id:any){
        return this.apollo.mutate({
            fetchPolicy: 'no-cache',
            mutation: gql`
                mutation deletePost($id:String!){
                    data : deletePost(id : $id){
                        message
                    }
                }
            `,
            variables: {
                id
            }

        })
        .pipe(
            map((resp: any) => {
                return resp.data.data;
            }),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getActiveUserStack(filters?:any) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getMyPost($input: GetPostsInput){
                    data : getMyPost(input : $input){
                        data{
                            ... PostBasic
                            stack_users {
                                won_amount
                                staked_amount
                                created_at
                                user {
                                ... UserBasic
                                }
                            }
                            sport_type{
                                name
                            }
                        }
                        meta{
                            total
                            last_page
                            current_page
                        }
                    }
                }
                ${Fragments.PostBasic}
                ${Fragments.UserBasic}
            `,
            variables: {
                input: filters
            }
        })
            .pipe(
                map((resp: any) => resp.data.data),
                // catchError((error) => {
                //     return throwError(JSON.parse(JSON.stringify(error)));
                // })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }


    getAllActivePost(request) {

        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getPosts($input: GetPostsInput){
                    data : getPosts(input : $input){
                        data{
                            ...PostBasic,
                            stack_users {
                                _id
                                staked_amount
                                won_amount
                                user{
                                    ...UserBasic
                                }
                                created_at
                                updated_at
                            }
                            sport_type{
                                name
                            }
                        }
                        meta{
                            total
                            last_page
                            current_page
                        }
                    }
                }
                ${Fragments.PostBasic}
                ${Fragments.UserBasic}
                `,
            variables: {
                input: request
            }
        }).pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getBuyerPost(request) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getBuyerPosts($input: GetPostsInput){
                    data : getBuyerPosts(input : $input){
                        data{
                            ... PostBasic
                            stack_users {
                                staked_amount
                                created_at
                                user {
                                ... UserBasic
                                }
                            }
                        }
                        meta{
                            total
                            last_page
                            current_page
                        }
                    }
                }
                ${Fragments.PostBasic}
                ${Fragments.UserBasic}
                `,
            variables: {
                input: request
            }
        }).pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    buyStack(request:any){
        
        return this.apollo.mutate({
            fetchPolicy: 'no-cache',
            mutation: gql`
                mutation buyStack($input: AddStackUserInput!){
                    data : buyStack( input : $input){
                        post{
                            ...PostBasic
                        }
                        transaction {
                            ...TransactionData
                        }
                        message
                    }
                }
                ${Fragments.PostBasic}
                ${Fragments.TransactionData}
            `,
            variables: {
                input: request
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }
    // getSportType(){
    //     return this.apollo.query({
    //         fetchPolicy: 'no-cache',
    //         query: gql`
    //             query getSportTypes{
    //                 data : getSportTypes{
    //                     _id
    //                     name
    //                     status
    //                     created_at
    //                     updated_at
    //                 }
    //             }
    //         `
    //     })
    //     .pipe(
    //         map((resp: any) => {
    //             return resp.data.data;
    //         }),
    //         // catchError((error) => {
    //         //     return throwError(JSON.parse(JSON.stringify(error)));
    //         // })
    //     )
    //     .toPromise()
    //     .then((data: any) => {
    //         return data;
    //     }).catch((error) => {
    //         error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
    //         throw error;
    //     })
    // }

    getInvoices(request){
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getByUser($input: InvoiceTableInput){
                    data : getByUser(input : $input){
                        data{
                            _id
                            post {
                                title
                                description
                            }
                            file_name
                            file
                            amount
                            created_at
                        }
                        meta{
                            total
                            last_page
                            current_page
                        }
                    }
                }
                `,
            variables: {
                input: request
            }
        }).pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }
}
