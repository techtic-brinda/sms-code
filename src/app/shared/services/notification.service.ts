import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { Fragments } from 'src/app/graphql/fregments';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  dataStore: {
    notifications: any
    counts: any
  }

  private _notifications: BehaviorSubject<any>;
  private _counts: BehaviorSubject<any>;


  constructor(
    private apollo: Apollo,
  ) {
    this.dataStore = {
      notifications: {},
      counts: 0,
    }
    this._notifications = new BehaviorSubject(this.dataStore.notifications);
    this._counts = new BehaviorSubject(this.dataStore.counts);
  }

  public get notifications(): any {
    return this.dataStore.notifications;
  }

  public get $notifications(): Observable<any> {
    return this._notifications.asObservable();
  }

  public set notifications(value: any) {
    this.dataStore.notifications = value;
    this._notifications.next(Object.assign({}, this.dataStore).notifications);
  }

  public get counts(): any {
    return this.dataStore.counts;
  }

  public get $counts(): Observable<any> {
    return this._counts.asObservable();
  }

  public set counts(value: any) {
    this.dataStore.counts = value;
    this._counts.next(Object.assign({}, this.dataStore).counts);
  }

  getNotificationCount() {
    return this.apollo.query({
      fetchPolicy: 'no-cache',
      query: gql`
        query getNotificationCount{
          data : getNotificationCount{
            data 
          }
        }
       
      `,
    })
      .toPromise()
      .then((data: any) => {
        this.counts = data.data['data']['data']
        return data;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  readAllNotifications() {
    return this.apollo.query({
      fetchPolicy: 'no-cache',
      query: gql`
        query readAllNotifications{
          data : readAllNotifications{
            status 
          }
        }
       
      `,
    })
      .toPromise()
      .then((data: any) => {
        this.counts = 0
        return data;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  getNotifications(request) {
    return this.apollo.query({
      fetchPolicy: 'no-cache',
      query: gql`
        query getNotifications($input: NotificationsInput){
          data : getNotifications(input: $input){
            data {
              _id,
              body,
              data,
              user_id,
              user {
                ...UserData
              }
              read_at
              created_at
            }
            meta{
              total
              last_page
              current_page
            }
          }
        }
        ${Fragments.UserData}        
      `,
      variables: {
        input: request
      }
    })
      .pipe(
        map((resp: any) => resp.data.data),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((data: any) => {
        this.notifications = data;
        return data;
      }).catch((error) => {
        this.notifications = [];
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

}
