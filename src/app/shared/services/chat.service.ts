import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Fragments } from 'src/app/graphql/fregments';
import { map, catchError } from 'rxjs/operators';
import { throwError, BehaviorSubject, Observable, Subject } from 'rxjs';
import * as _ from 'underscore';
import { AuthService } from './auth/auth.service';

export const GetMessageQuery = gql`
    query getMessages($input: MessageInput){
      data : getMessages(input: $input){
        data {
          ...MessageData
        }
        meta {
          ...PaginationData
        }
      }
    } 
    ${Fragments.MessageData}
    ${Fragments.PaginationData}
  `

export const AddMessageQuery = gql`
    mutation addMessage($input: AddMessageInput!){
      data : addMessage(input: $input){
        ...MessageData
      }
    }
    ${Fragments.MessageData}
  `

export const MarkAsReadQuery = gql`
    mutation markAsRead($user_id: String!){
      data : markAsRead(user_id: $user_id){
        _id
        status
        user_id
        contact_id
        unread
      }
    }
  `

export const AddContactQuery = gql`
    mutation addContact($input: AddContactInput!){
      data : addContact(input: $input){
        _id
        status
        user_id
        contact_id
        unread
        last_message {
          ...MessageData
        }
        user {
          ...UserBasic
        }
        contact {
          ...UserBasic
        }
      }
    }
    ${Fragments.MessageData}
    ${Fragments.UserBasic}
  `

export const OnGetMessage = gql`
    subscription messageAdded{
      messageAdded{
        ...MessageData
      }
    }
    ${Fragments.MessageData}
  `

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  dataStore: {
    contacts: any
    contactsCount: any
  }

  private _contacts: BehaviorSubject<any>;
  private _contactsCount: BehaviorSubject<any>;
  private _message: Subject<any>;
  messageSubscription: any;

  constructor(
    private apollo: Apollo,
    private authService: AuthService
  ) {
    this.dataStore = { contacts: [], contactsCount : 0 }
    this._contacts = new BehaviorSubject(this.dataStore.contacts);
    this._contactsCount = new BehaviorSubject(this.dataStore.contactsCount);
    this._message = new Subject();
  }

  public get $message(): Observable<any> {
    return this._message.asObservable();
  }

  public set message(value: any) {
    this._message.next(value);
  }


  public get contacts(): any {
    return this.dataStore.contacts;
  }

  public get $contacts(): Observable<any> {
    return this._contacts.asObservable();
  }

  public set contacts(value: any) {
    this.dataStore.contacts = value;
    this._contacts.next(Object.assign({}, this.dataStore).contacts);
  }

  public get contactsCount(): any {
    return this.dataStore.contactsCount;
  }

  public get $contactsCount(): Observable<any> {
    return this._contactsCount.asObservable();
  }

  public set contactsCount(value: any) {
    this.dataStore.contactsCount = value;
    this._contactsCount.next(Object.assign({}, this.dataStore).contactsCount);
  }

  getContact(id: any) {
    return this.apollo.query({
      query: gql`
        query getContact(id:String!){
          data : getContact(id:$id){
            _id
            status
            created_at
            contact_id
            contact {
              ...UserData
            }
            last_message {
              ...MessageData
            }
          }
        }
        ${Fragments.MessageData}        
        ${Fragments.UserData}        
      `,
      variables: {
        id
      }
    })
      .pipe(
        map((resp: any) => resp.data.data),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((data: any) => {
        this.contacts = data;
        return data;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  getContacts() {
    return this.apollo.query({
      query: gql`
        query getContacts{
          data : getContacts{
            _id
            status
            created_at
            contact_id
            unread
            contact {
              ...UserData
            },
            last_message {
              message
            }
          }
        }
        ${Fragments.UserData}        
      `
    })
      .pipe(
        map((resp: any) => resp.data.data),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((data: any) => {

        data = _.map(data, (item) => this.mapContact(item));
        this.contacts = data;
        this.updateCount()
        return data;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  getMessages(input: any, contact: any) {
    const { page, limit } = input;
    return this.apollo.query({
      fetchPolicy: 'no-cache',
      query: GetMessageQuery,
      variables: {
        input: { user_id: contact.contact_id, page: (page || 0), limit: (limit || 30) }
      }
    })
      .pipe(
        map((resp: any) => resp.data.data),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((data: any) => {

        
        
        return data;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  addContact(user_id: any) {
    return this.apollo.mutate({
      mutation: AddContactQuery,
      variables: {
        input: { user_id: user_id }
      }
    })
      .pipe(
        map((resp: any) => resp.data.data),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((contact: any) => {
        contact = this.mapContact(contact);
        let contacts = [contact].concat(this.contacts);

        this.contacts = _.uniq(contacts, (contact) => contact.contact_id)

        return contact;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  markAsRead(user_id: any) {
    return this.apollo.mutate({
      mutation: MarkAsReadQuery,
      variables: {
        user_id: user_id 
      }
    })
      .pipe(
        map((resp: any) => resp.data.data),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((contact: any) => {
        this.updateCount()
        return contact;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  sendMessage(message: any, contact: any) {

    return this.apollo.mutate({
      mutation: AddMessageQuery,
      variables: {
        input: { message, user_id: contact.contact_id }
      }
    })
      .pipe(
        map((resp: any) => resp.data.data),
        // catchError((error) => {
        //   return throwError(JSON.parse(JSON.stringify(error)));
        // })
      )
      .toPromise()
      .then((message: any) => {

        message = this.mapMessage(message);

        contact.messages.push(message)
        contact.last_message = message;
        return message;
      }).catch((error) => {
        error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
        throw error;
      })
  }

  destroy() {
    if (this.messageSubscription) {
      this.messageSubscription.unsubscribe();
    }
  }

  init() {
    this.destroy();
    this.getContacts();
    this.messageSubscription = this.apollo
      .subscribe({
        query: OnGetMessage
      })
      .subscribe((data:any) => {
          let message = data.data.messageAdded;
          if(message){
              let contact = _.find(this.contacts, (contact) => contact.contact_id == message.user_id);
              if (contact) {
                if (!contact.messages) {
                  contact.messages = [];
                }
                message = this.mapMessage(message);
                contact.messages = contact.messages.concat([message]);
                contact.last_message = message;
                if(contact.unread){
                  contact.unread = contact.unread+1;
                }else{
                  contact.unread = 1;
                }
                this.message = message;
              } else {
                this.addContact(message.user_id);
              }
            this.updateCount()
          }
      });
  }

  updateCount(){
    let chat_counts = 0
    this.contacts.forEach(contact => {
      chat_counts = chat_counts + contact.unread
    });

    this.contactsCount = chat_counts;
  }

  mapMessage(message) {
    message.my_message = message.user_id == this.authService.user._id
    return message
  }

  mapContact(contact) {
    contact.messages = [];
    if (!contact.last_message)
      contact.last_message = {};
    return contact
  }
}
