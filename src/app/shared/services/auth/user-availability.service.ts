import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { Fragments } from 'src/app/graphql/fregments';
import * as _ from "underscore";
import * as moment from "moment";
import { AuthService } from './auth.service';


@Injectable({
    providedIn: 'root'
})
export class UserAvailabilityService {
    isLoggedIn = false;

    constructor(
        private apollo: Apollo,
        private authService: AuthService,

    ) {

    }

    getEvent({ start, end }) {

        return this.apollo.query({
            fetchPolicy: "no-cache",
            query: gql`
                query getAvailability($input: GetAvailabilityInput!){
                    data : getAvailability( input : $input){
                        ...EventData
                        booking_user{
                            _id
                            first_name
                            last_name
                            profile_pic
                        }
                    }
                }
                ${Fragments.EventData}
            `,
            variables: {
                input: { start, end }
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            //description
            //return data;
            return _.chain(data).filter((item) => {
                return moment(item.end).isSameOrAfter(moment());
            }).map((item) => {
                if (item.status == 'booked') {
                    item.durationEditable = false;
                    item.eventOverlap = false;
                    item.eventStartEditable = false;
                    item.droppable = false;
                    item.editable = false;
                }
                return item;
            }).value()
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }



    getUserEvent(user_id, { start, end }) {

        return this.apollo.query({
            fetchPolicy: "no-cache",
            query: gql`
                query getAvailability($input: GetAvailabilityInput!){
                    data : getAvailability( input : $input){
                        ...EventData
                        booking_user{
                            _id
                        }
                    }
                }
                ${Fragments.EventData}
            `,
            variables: {
                input: { start, end, user_id }
            }
        })
        .pipe(
            map((resp: any) => {
                return resp.data.data
            }),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            const currentUser = this.authService.user;
            return _.chain(data).filter((item) => {
                return moment(item.end).isSameOrAfter(moment());
            }).map((item) => {
                if (item.booking_user==null || (item.booking_user._id !== currentUser._id)){
                    item.rendering = 'background';
                }
                return item
            }).value()
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getRequestList(request) {
        return this.apollo.query({
            fetchPolicy: "no-cache",
            query: gql`
                query getBookingRequestInput($input: GetBookingRequestInput){
                    data : getBookingRequestInput( input : $input){
                        data {
                            ...EventData
                            booking_user{
                                _id
                                first_name
                                last_name
                                profile_pic
                            }
                            user{
                                _id
                                first_name
                                last_name
                                profile_pic
                            }
                        }
                        meta{
                            ...PaginationData
                        }
                    }
                }
                ${Fragments.EventData}
                ${Fragments.PaginationData}
            `,
            variables: {
                input: request
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }


    addUpdateEvent(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation addUpdateAvailability( $input: AvailabilityInput! ){
                    data : addUpdateAvailability( input : $input ){
                        ...EventData
                    }
                }
                ${Fragments.EventData}
            `,
            variables: {
                input: request
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }


    delete(id: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation deleteAvailability( $id: String! ){
                    data : deleteAvailability( id : $id ){
                        message
                    }
                }
            `,
            variables: {
                id: id
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }


    confirmBooking(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation confirmBooking( $input: BookingRequestInput! ){
                    data : confirmBooking( input : $input ){
                        ...EventData
                    }
                }
                ${Fragments.EventData}
            `,
            variables: {
                input: request
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

}
