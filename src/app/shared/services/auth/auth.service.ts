import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map } from 'rxjs/operators';
import { Fragments } from 'src/app/graphql/fregments';
import { AppStorage } from '../cookies/app-storage';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    isLoggedIn = false;

    dataStore: {
        token: string
        user: any
    }

    private _token: BehaviorSubject<string>;
    private _user: BehaviorSubject<any>;
    private _onShowLogin: Subject<any> = new Subject();

    constructor(
        @Inject(AppStorage) private appStorage: Storage,
        private apollo: Apollo
    ) {
        this.dataStore = {
            token: null,
            user: null
        }
        
        this._token = new BehaviorSubject(this.dataStore.token);
        this._user = new BehaviorSubject(this.dataStore.user);
        this.init();
    }


    public get $onShowLogin(): Observable<any> {
        return this._onShowLogin.asObservable();
    }

    public get $token(): Observable<any> {
        return this._token.asObservable();
    }

    public get token(): any {
        return this.dataStore.token;
    }

    public set token(value: any) {
        this.dataStore.token = value;
        this.appStorage.setItem('token', this.dataStore.token);
        this._token.next(this.dataStore.token);
    }

    public get user(): any {
        return this.dataStore.user;
    }

    public get $user(): Observable<any> {
        return this._user.asObservable();
    }

    public set user(value: any) {
        this.dataStore.user = value;
        this.appStorage.setItem('user', JSON.stringify(this.dataStore.user));
        this._user.next(Object.assign({}, this.dataStore).user);
    }

    showLogin() {
        this._onShowLogin.next(true);
    }

    login(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation login($email:String!, $password:String!){
                    data : login( input : {
                        email : $email
                        password : $password
                    }){
                        status
                        token
                        user {
                            ...UserData
                        }
                    }
                }
                ${Fragments.UserData}
            `,
            variables: {
                ...request
            }
        })
            .pipe(
                map((resp: any) => resp.data.data),
                // catchError((error) => {
                //     return throwError(JSON.parse(JSON.stringify(error)));
                // })
            )
            .toPromise()
            .then((data: any) => {
                //console.log("data", data);
                this.token = data.token;
                this.user = data.user;
                this.isLoggedIn = true;
                return data;
            }).catch((error) => {
                this.isLoggedIn = false;
                this.token = null;
                this.user = null;
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    register(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation register(
                    $first_name: String!,
                    $last_name: String!,
                    $email:String!,
                    $password:String!,
                    $dob:String,
                    $gender:String,
                    $status:String
                ){
                    data : register( input : {
                        first_name : $first_name
                        last_name : $last_name
                        email : $email
                        password : $password
                        dob : $dob
                        gender : $gender
                        status : $status
                    }){
                        status
                        token
                        user {
                            ...UserData
                        }
                    }
                }
                ${Fragments.UserData}
            `,
            variables: {
                ...request
            }
        })
            .pipe(
                map((resp: any) => resp.data.data),
                // catchError((error) => {
                //     return throwError(JSON.parse(JSON.stringify(error)));
                // })
            )
            .toPromise()
            .then((data: any) => {
                this.token = data.token;
                this.user = data.user;
                this.isLoggedIn = true;
                return data;
            }).catch((error) => {
                this.isLoggedIn = false;
                this.token = null;
                this.user = null;
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })


    }

    socialLogin(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation socialLogin(
                    $provider: String!,
                    $token: String!,
                    $email: String!,
                    $name: String!,
                    $user_type: String!
                ){
                    data : socialLogin( input : {
                        provider : $provider
                        token : $token
                        email : $email
                        name : $name
                        user_type : $user_type
                    }){
                        status
                        token
                        user {
                            ...UserData
                        }
                    }
                }
                ${Fragments.UserData}
            `,
            variables: {
                ...request
            }
        })
            .pipe(
                map((resp: any) => resp.data.data),
                // catchError((error) => {
                //     return throwError(JSON.parse(JSON.stringify(error)));
                // })
            )
            .toPromise()
            .then((data: any) => {
                this.token = data.token;
                this.user = data.user;
                this.isLoggedIn = true;
                return data;
            }).catch((error) => {
                this.isLoggedIn = false;
                this.token = null;
                this.user = null;
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    forgotPassword(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation forgotPassword($email:String!){
                    data : forgotPassword(
                        email : $email
                    ){
                        message
                    }
                }
            `,
            variables: {
                ...request
            }
        })
            .pipe(
                map((resp: any) => resp.data.data),
                // catchError((error) => {
                //     return throwError(JSON.parse(JSON.stringify(error)));
                // })
            )
            .toPromise()
            .then((data: any) => {
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })

    }

    setPassword(request: any){
        return this.apollo.mutate({
            mutation: gql`
                mutation setPassword($password:String!,$token:String!){
                    data : setPassword( input : {
                        password : $password
                        token : $token
                    }){
                        status
                        message
                        data
                    }
                }
            `,
            variables: {
                ...request
            }
        })
            .pipe(
                map((resp: any) => resp.data.data),
                // catchError((error) => {
                //     return throwError(JSON.parse(JSON.stringify(error)));
                // })
            )
            .toPromise()
            .then((data: any) => {
                //this.token = data.token;
                //this.user = data.user;
                //this.isLoggedIn = true;
                return data;
            }).catch((error) => {
                error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
                throw error;
            })
    }

    logout() {
        return new Promise((resolve, reject) => {
            this.appStorage.clear();
            //localStorage.removeItem("user");
            //localStorage.removeItem("token");
            this.isLoggedIn = false;
            this.token = null;
            this.user = null;
            resolve(true);
        });
    }

    init() {
        this.dataStore.token = this.appStorage.getItem('token');
        //console.log(this.appStorage.getItem('user'));
        try {
            this.dataStore.user = JSON.parse(this.appStorage.getItem('user'));
        } catch (error) {
            this.dataStore.user = null
        }
        

        console.log('this.token', this.token);
        if (this.token) {
            this.isLoggedIn = true;
        }
    }

}
