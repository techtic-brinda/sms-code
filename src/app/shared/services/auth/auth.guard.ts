import { Injectable, Inject } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthService } from './auth.service';
import { AppStorage } from '../cookies/app-storage';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        @Inject(AppStorage) private appStorage: Storage,
        private authService: AuthService
    ) { }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
       // const currentUser = this.authService.isLoggedIn;

        const currentUser = this.appStorage.getItem('user');

        return currentUser ? true : false;

        // return await new Promise((resolve, reject)=>{
        //     this.authService.$user.subscribe((user) => {
        //         if (user && user._id){
        //             resolve();
        //         }else{
        //             reject();
        //         }
        //     })
        // }).then(()=> true).catch(()=>{
        //     this.router.navigate(['/'], { queryParams: { returnUrl: state.url, login:true } });
        //     return false;
        // })
    }
}