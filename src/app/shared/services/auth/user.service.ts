import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { map, catchError } from 'rxjs/operators';
import { Fragments } from 'src/app/graphql/fregments';


@Injectable({
    providedIn: 'root'
})
export class UserService {
    isLoggedIn = false;

    constructor(
        private apollo: Apollo
    ) {

    }

    updateProfile(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation updateProfile($input: UpdateProfileInput!){
                    data : updateProfile( input : $input){
                        status
                        user {
                            ...UserData
                            about
                            sport_type_id
                        }
                    }
                }
                ${Fragments.UserData}
            `,
            // context: {
            //     useMultipart: true
            // },
            variables: {
                input: request
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    changePassword(request: any) {
        return this.apollo.mutate({
            mutation: gql`
                mutation changePassword(
                    $old_password: String!,
                    $password: String!
                ){
                    data : changePassword(
                        old_password : $old_password
                        password : $password
                    ){
                        message
                    }
                }
            `,
            variables: {
                ...request
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getTransationHistory(input) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getPaymentHistory($input:TransactionTableInput){
                    data : getPaymentHistory(input:$input){
                        data{
                            _id
                            user_id
                            amount
                            closing_balance
                            description
                            transactions_type
                            data
                            getway_transactions_id
                            getway_type
                            status
                            created_at
                        }
                        meta{
                            total
                            last_page
                            current_page
                        }
                    }
                }
            `,
            variables: {
                input: input
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getUser(id) {
        return this.apollo.query({
            //fetchPolicy: 'no-cache',
            query: gql`
                query user($id: String!){
                    data : user(id : $id){
                        ...UserData
                    }
                }
                ${Fragments.UserData}
            `,
            variables: { id }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getPlayers() {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getPlayers{
                    data : getPlayers{
                        ...UserData
                        about
                        sport_type_id
                    }
                }
                ${Fragments.UserData}
            `
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //    return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getStatistics() {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getStatistics{
                    data : getStatistics{
                        investment
                        amount
                        totalWithdraw
                        availbleBalance
                    }
                }
            `,
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getTransactionStatistics(request) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getTransactionStatistics($input: TransactionStatistics){
                    data : getTransactionStatistics(input : $input){
                        
                        balanceData{
                            year
                            month
                            day
                            closing_balance
                        }
                        transactionData{
                            year
                            month
                            day
                            staked_amount
                            won_amount
                        }
                    }
                }
            `,
            variables: {
                input: request
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getUserStatistics(request){
        let sport_id = (request._id) ? (request._id): null;
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getUserStatistics($sport_id: String ){
                    data : getUserStatistics(sport_id : $sport_id){
                        userData{
                            full_name
                            staked_amount
                            won_amount
                        }
                    }
                }
            `,
            variables: {
                sport_id: sport_id
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    getSportTypeStatistics(user_id) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query getSportTypeStatistics($user_id: String ){
                    data : getSportTypeStatistics(user_id : $user_id){
                        total
                        name
                        won_total
                    }
                }
            `,
            variables: {
                user_id: user_id
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
            // catchError((error) => {
            //     return throwError(JSON.parse(JSON.stringify(error)));
            // })
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    setFollow(player_id) {
        return this.apollo.mutate({
            mutation: gql`
                mutation setFollow($player_id: String!){
                    data : setFollow(player_id : $player_id){
                        message
                    }
                }
            `,
            variables: {
                player_id
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    isFollow(player_id) {
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query isFollow($player_id: String){
                    data : isFollow(player_id : $player_id){
                        message
                        data
                    }
                }
            `,
            variables: {
                player_id
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    followingList(request){
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query followingList($input: UserFollowDataTableInput){
                    data : followingList(input : $input){
                        data{
                            player_id
                            user{
                                first_name
                                last_name
                                profile_pic
                                _id
                            }
                            player{
                                first_name
                                last_name
                                profile_pic
                                _id
                            }
                        }
                        meta{
                            total
                            last_page
                            current_page
                        }
                    }
                }
            `,
            variables: {
                input : request
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

    saveFirebaseToken(token){
        return this.apollo.query({
            fetchPolicy: 'no-cache',
            query: gql`
                query saveFirebaseToken($token: String){
                    data : saveFirebaseToken(token : $token){
                        data
                    }
                }
            `,
            variables: {
                token: token
            }
        })
        .pipe(
            map((resp: any) => resp.data.data),
        )
        .toPromise()
        .then((data: any) => {
            return data;
        }).catch((error) => {
            error = ((error.graphQLErrors && error.graphQLErrors[0]) ? error.graphQLErrors[0] : error);
            throw error;
        })
    }

}
