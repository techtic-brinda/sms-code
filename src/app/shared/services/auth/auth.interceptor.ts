import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';
import { AppStorage } from '../cookies/app-storage';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    isBrowser: boolean;
    
    constructor(
        @Inject(AppStorage) private appStorage: Storage,
        @Inject(PLATFORM_ID)
        platform: any
    ) {
        this.isBrowser = isPlatformBrowser(platform);
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.appStorage.getItem('token');

        if (token != undefined && token != null && token != "" && token != "null"){
            const req1 = req.clone({
                headers: req.headers.set('Authorization', `Bearer ${token}`),
            });
            return next.handle(req1).pipe(
                catchError((error) => {
                    if(this.isBrowser){
                        return throwError(JSON.parse(JSON.stringify(error)));
                    }else{
                        return throwError(error);
                    }
                })
            );
        }else{
            return next.handle(req).pipe(
                catchError((error) => {
                    if (this.isBrowser) {
                        return throwError(JSON.parse(JSON.stringify(error)));
                    } else {
                        return throwError(error);
                    }
                })
            );
        }
    }

}