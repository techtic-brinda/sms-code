import { RouteTransformerDirective } from "./route-transformer.directive";
import { ReadMoreDirective } from './read-more';
import { ImgDirective } from './img.directive';
import { AutofocusDirective } from './auto-focus.directive';

export { RouteTransformerDirective } from "./route-transformer.directive";

export const Directives = [
    RouteTransformerDirective,
    ReadMoreDirective,
    ImgDirective,
    AutofocusDirective
];
