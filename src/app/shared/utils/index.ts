import { environment } from 'src/environments/environment';
import * as _ from 'underscore';

export function apiUrl(path?: string) {
    return environment.apiUrl + '/' + path;
}


export function scrollToElement(item: any, duration: number = 750, container?: any) {
    if (typeof item === 'string') {
        item = document.querySelector(item);
    }
    if (item) {
        const itemPos = item.offsetTop;
        if (container) {
            if (typeof container === 'string') {
                container = document.querySelector(container);
            }
            scrollTo(container, itemPos, duration, true);
        } else {
            scrollTo(window.document, itemPos, duration);
        }
    }
}


export function scrollTo(element, to: number, duration, isContainer: boolean = false) {
    const increment = 20;
    let start,
        remaining,
        currentTime = 0,
        animateScroll;

    if (isContainer) {
        // for custom container element
        start = element.scrollTop;
    } else if (element.body.scrollTop > 0) {
        // for chrome
        start = element.body.scrollTop;
    } else if (element.documentElement.scrollTop > 0) {
        // for firefox
        start = element.documentElement.scrollTop;
    } else {
        start = 0;
    }

    remaining = to - start;

    animateScroll = () => {
        currentTime += increment;
        const val = easeInOut(currentTime, start, remaining, duration);
        if (isContainer) {
            element.scroll(0, val);
        } else {
            // to allow scroll function on different browsers both chrome and firefox
            top.window.scroll(0, val);
        }

        if (currentTime < duration) {
            setTimeout(animateScroll, increment);
        }
    };
    animateScroll();
}

export function easeInOut(
    currentTime: number,
    startTime: number,
    remainingTime: number,
    duration: number
) {
    currentTime /= duration / 2;

    if (currentTime < 1) {
        return (remainingTime / 2) * currentTime * currentTime + startTime;
    }

    currentTime--;
    return (
        (-remainingTime / 2) * (currentTime * (currentTime - 2) - 1) + startTime
    );
}

export function isJsObject(object: any) {
    let type: string = typeof object;
    return (type == 'object' || type == 'Array') && object != null;
}


export function postGropByUser(data) {
    return _.chain(data).groupBy('user_id').compact().map((item) => {
        const user = item[0].user || {};
        const posts = _.map(item, (post) => {
            delete item.user;
            return post
        })
        user.posts = posts;
        return user;
    }).value();
}



