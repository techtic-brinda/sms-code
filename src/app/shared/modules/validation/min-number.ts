import { IValidation, Validation } from './validation';

export class MinNumber extends Validation implements IValidation {
    attrs
    isValid(...attrs) {
        this.attrs = attrs;
        return parseFloat(this.value) >= parseFloat(this.attrs[0]);
    }

    getMessage(): string {
        return `Please enter more than ${this.attrs[0]} value.`;
    }
}
