import { Domain } from './domain';
import { IValidation, Validation } from './validation';

export class Email extends Validation implements IValidation {

    isValid() {

        let EMAIL_REGEXP = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return (EMAIL_REGEXP.test(this.value));
    }

    getMessage(): string {
        return `Please enter a valid email address.`;
    }
}