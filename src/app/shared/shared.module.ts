import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Components } from './components';
import { Pipes } from './pipes';
import { RouterModule } from '@angular/router';
import { StrToDatePipe } from './pipes/str-to-date.pipe';
import { Directives } from './diractive';
import { Moduels as CustomModules } from './modules';
import { MomentModule } from 'ngx-moment';
import { NgbToastModule } from '@ng-bootstrap/ng-bootstrap';
import { CountdownTimerModule } from 'ngx-countdown-timer';
import { NgxStripeModule } from 'ngx-stripe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


const Modules = [
  RouterModule,
  FormsModule,
  ReactiveFormsModule,
  MomentModule,
  NgbToastModule,
  ...CustomModules,

];

@NgModule({
  imports: [
    CommonModule,
    NgxStripeModule,
    NgbModule,
    ...Modules,
    CountdownTimerModule.forRoot()
  ],
  declarations: [
    ...Components,
    ...Pipes,
    ...Directives,
    StrToDatePipe
  ],
  exports: [
    ...Modules,
    ...Pipes,
    ...Components,
    ...Directives,

  ],
  entryComponents: [
  ]
})
export class SharedModule {

  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [ 
        //{ provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter }
      ]
    };
  }
}
