import { Component, Input } from '@angular/core';
import { trigger, state, style, animate, transition, group } from '@angular/animations';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'http-message',
    template: `
        <div class="alert toggle-alert alert-{{type}}" [ngClass]="{active : showMessage}">
            <div class="row">
                <div class="col">
                    <div *ngFor="let error of errorMessage">{{ error }} </div>
                </div>
                <div class="col-auto">
                    <span aria-hidden="true"  (click)="closeMessage()">×</span>
                </div>
            </div>
		</div>`,
    styles: ['.alert{display:none} .alert.active{ display:block } .material-icons{vertical-align: text-bottom; font-size: 1em;}'],

})
export class HttpMessage {
    errorMessage: any = [];
    showMessage: boolean = false;
    private _type: any;

    @Input('type') 
    get type(): any {
        return this._type;
    }

    set type(value: any) {
        if (value == 'error') {
            this._type = 'danger';
        } else {
            this._type = value;
        }
    }

    @Input('data') set data(response: any) {
        this.errorMessage = [];

        
        if (response){
            if (typeof response == 'string') {
                this.showMessage = true;
                this.errorMessage.push(response);
            } else  {

                if (response.extensions != undefined) {
                    this.type = 'error';
                } else {
                    this.type = 'success';
                }

                if (response.message != undefined) {
                    this.showMessage = true;
                    this.errorMessage.push(response.message);
                } else if (response.messages != undefined) {
                    this.showMessage = true;
                    let all_errors: any = response.messages;
                    Object.keys(all_errors).map((key) => {
                        for (let er of all_errors[key]) {
                            this.errorMessage.push(er);
                        }
                    });
                } else if (response.message != undefined || response.message != null) {
                    this.showMessage = true;
                    this.errorMessage.push(response.message);
                }
            }
        }else{
            this.showMessage = false;
            this.errorMessage = [];
        }
    };

    constructor() {

    }

    closeMessage() {
        this.showMessage = false;
        setTimeout(() => {
            this.errorMessage = [];
        }, 500);
    }
}
