import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { StripeService as AppStripeService } from 'src/app/shared/services/payment/stripe.service';
import { StripeCardComponent, ElementOptions, ElementsOptions, StripeService } from 'ngx-stripe';
import { AppInitService } from 'src/app/shared/services/app-init.service';


@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.scss']
})
export class StripeComponent implements OnInit {
  name:string;
  loading = false;
  settings:any = {};
  totalAmount = 0;

  @Output('done') done: EventEmitter<any> = new EventEmitter()
  @Output('error') error: EventEmitter<any> = new EventEmitter()

  private _amount: number;
  @Input()
  public get amount(): number {
    return this._amount;
  }
  public set amount(value: number) {
    this._amount = value;
  }
  constructor(
    private stripeService: StripeService,
    private appStripeService: AppStripeService,
    private appInitService: AppInitService,
  ) { }

  @ViewChild(StripeCardComponent, { static: false }) card: StripeCardComponent;

  cardOptions: ElementOptions = {
    hidePostalCode:true
  }

 
  elementsOptions: ElementsOptions = {
    locale: 'en',
  };

  ngOnInit() {
    this.appInitService.$settings.subscribe((data) => this.settings = data);
  }

  payNow() {
    this.loading = true;
    this.stripeService
      .createToken(this.card.getCard(), { name : this.name })
      .subscribe(result => {
        if (result.token) {
          this.appStripeService.charge(result.token.id, this.amount).then((data:any)=>{
            this.done.emit(data.data);
            this.loading = false;
          }).catch((error)=>{
            this.error.emit(error);
            this.loading = false;
          })
        } else if (result.error) {
          this.error.emit(result.error);
          this.loading = false;
        }
      });
  }
  onAmountChange(amount) {
    this.totalAmount = parseFloat(amount) + ((amount * this.settings.stripe_charges) / 100)
  }

}
