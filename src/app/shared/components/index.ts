import { ImageTagComponent } from './image-tag/image-tag.component';
import { LoadingComponent } from './loading/loading.component';
import { ControlMessages } from './control-messages';
import { HttpMessage } from './http-message';
import { SharkItemComponent } from './partials/shark-item/shark-item.component';
import { StakeItemComponent } from './partials/stake-items/stake-items.component';
import { CountDownTimeComponent } from './countdown-time/countdown-time.component';
import { StripeComponent } from './stripe/stripe.component';

const Components = [
    ImageTagComponent,
    LoadingComponent,
    ControlMessages,
    HttpMessage,
    SharkItemComponent,
    StakeItemComponent,
    CountDownTimeComponent,
    StripeComponent
];

export { Components };
