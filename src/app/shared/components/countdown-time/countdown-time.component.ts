import { Observable } from 'rxjs/Rx';
import { Component, OnInit, ElementRef, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
    selector: 'countdown-time',
    templateUrl: './countdown-time.component.html',
    styleUrls: ['./countdown-time.component.scss']
})
export class CountDownTimeComponent implements OnInit {

    private _trialEndsAt;
    private _diff: any;
    public _days: any;
    public _hours: any;
    public _minutes: any;
    public _seconds: any;
    @Input('inputDate') inputDate: any;
    constructor() {}

    ngOnInit() {
       
        this._trialEndsAt = this.inputDate;
        Observable.interval(1000).map((x) => {
            let date_diff:any = moment.utc(this._trialEndsAt).toDate();
            date_diff = new Date(date_diff).getTime() - new Date().getTime();
            this._diff = new Date(date_diff);
        }).subscribe((x) => {
            this._days = this.getDays(this._diff);
            this._hours = this.getHours(this._diff);
            this._minutes = this.getMinutes(this._diff);
            this._seconds = this.getSeconds(this._diff);
        });
    }

    getDays(t) {
        var days = Math.floor(t / 1000 / 60 / (60 * 24));
        return (days >= 0) ? this.twoDigit(days) : "";
    }

    getHours(t) {
        return this.twoDigit(t.getUTCHours());
    }

    getMinutes(t) {
        return this.twoDigit(t.getMinutes());
    }

    getSeconds(t) {
        return this.twoDigit(t.getSeconds());
    }

    twoDigit = function (number) {
        return number > 9 ? "" + number : "0" + number;
    };

}