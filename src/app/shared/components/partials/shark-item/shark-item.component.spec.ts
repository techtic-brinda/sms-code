import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharkItemComponent } from './shark-item.component';

describe('SharkItemComponent', () => {
  let component: SharkItemComponent;
  let fixture: ComponentFixture<SharkItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharkItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharkItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
