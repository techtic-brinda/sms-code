import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-shark-item',
  templateUrl: './shark-item.component.html',
  styleUrls: ['./shark-item.component.scss']
})
export class SharkItemComponent implements OnInit {
  private _user: any;
  private _post: any;

  @Input('user')
  public get user(): any {
    return this._user;
  }
  
  public set user(value: any) {
    this._user = value;
  }

  @Input('type') type:any = ""


  @Input('post')
  public set post(value: any) {
    this._user = value.user;
    this._user.posts = [value];
  }

  constructor() { }

  ngOnInit() {
  }

}
