import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StakeItemComponent } from './stake-items.component';

describe('StakeItemComponent', () => {
  let component: StakeItemComponent;
  let fixture: ComponentFixture<StakeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StakeItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StakeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
