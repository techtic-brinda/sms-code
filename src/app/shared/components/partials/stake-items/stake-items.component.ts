import { Component, OnInit, Input, ViewChild, Inject, PLATFORM_ID } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { HelperService } from 'src/app/shared/services/helper.service';
import { StackService } from 'src/app/shared/services/stack/stack.service';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { isPlatformBrowser } from '@angular/common';
import { AppInitService } from 'src/app/shared/services/app-init.service';

@Component({
  selector: 'app-stake-items',
  templateUrl: './stake-items.component.html',
  styleUrls: ['./stake-items.component.scss']
})
export class StakeItemComponent implements OnInit {

  buying_data: any = {};
  settings: any = {};
  total_my_staked_amount: number = 0;
  buttons: any[] = [];
  isBrowser: boolean = false;
  is_show: boolean = false;
  loadingBuy: boolean = false;
  private _progress: any = Math.random() * 100;
  stake: boolean;
  markup_amount: number = 0;
  total_amount: number = 0;
  public get progress(): any {
    return (100 - this._progress) + "%";
  }
  public set progress(value: any) {
    this._progress = value;
  }

  @ViewChild('buyConfirmation', {static : false}) buyConfirmation;
  @ViewChild('custom_amount', { static: false }) custom_amount;

  @Input('type') type: any = ""
  @Input('edit') edit: boolean;

  private _data: any;
  @Input('data')
  public get data(): any {
    return this._data;
  }
  public set data(value: any) {
    if (!value.sold_total) {
      value.sold_total = 0.00;
    }

    if (!value.sold_percentage) {
      value.sold_percentage = 100;
    }

    const maxStake = parseFloat(value.total_staked_amount) - parseFloat(value.sold_total);
    if (maxStake > 2000) {
      this.buttons = [25, 50, 100];
    } else if (maxStake > 1000) {
      this.buttons = [25, 50, 100];
    } else if (maxStake > 500) {
      this.buttons = [25, 50, 100];
    } else if (maxStake > 400) {
      this.buttons = [25, 50, 100];
    } else if (maxStake > 200) {
      this.buttons = [25, 50, 100];
    } else if (maxStake > 100) {
      this.buttons = [5, 8, 10];
    } else if (maxStake > 50) {
      this.buttons = [2, 5, 8];
    } else if (maxStake > 10) {
      this.buttons = [2, 5, 8];
    } else if (maxStake > 5) {
      this.buttons = [1, 2, 5];
    } else {
      this.buttons = [Math.round(maxStake * 100) / 100];
    }

    value.reamining = (value.total_staked_amount - value.sold_total);


    this.total_my_staked_amount = 0;

    if (value.stack_users) {
      for (let index = 0; index < value.stack_users.length; index++) {
        const stack_user = value.stack_users[index];
        this.total_my_staked_amount += parseFloat(stack_user.staked_amount);
      }
    }


    value.is_closed = moment(new Date()).isAfter(new Date(value.close_date));
    this._data = value;
  }

  constructor(
    private authService: AuthService,
    private appInitService: AppInitService,
    private modalService: NgbModal,
    private helperService: HelperService,
    private stackService: StackService,
    @Inject(PLATFORM_ID) platformId,
  ) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit() {
    this.appInitService.$settings.subscribe((data) => {
      this.settings = data;
      //this.markup_amount = this.settings.markup_amount;
    });

    if (this.authService.user && this.authService.user.user_type == "player"){
      this.stake= false;
    }else{
      this.stake =true;
    }


  }

  getProgress(stake) {
    return ((stake.sold_total > 0) ? 100 - ((stake.sold_total * 100) / stake.total_staked_amount) : 100) + "%";
  }

  getPer(amount) {
    // let per: any = Math.round(
    //   (parseFloat(this.data.sold_percentage) / 100) *
    //   (amount / parseFloat(this.data.total_staked_amount)) *
    //   100000,
    // ) / 1000;
    let per: any = (amount*100)/ this.data.total_staked_amount;

    return parseFloat(per).toFixed(2) + " %";
  }

  onBuy(amount) {
    if (!amount || amount <= 0 || amount > (Math.round(this.data.reamining * 100) / 100)) {
      this.helperService.errorMessage('Please select amount or enter valid amount into input.');
      return false;
    }
    let per = this.getPer(amount);

    this._data.is_show = !this._data.is_show;
    this._data.amount = amount;
    this.total_amount = +this._data.amount + +this.data.markup_amount;
    this.buying_data.amount = amount;
    this.buying_data.per = this.getPer(amount);
    this._data.note = `Confirm $${amount} (${per}) stake`;
  }


  confirmBuy() {
    if (this.authService.user && this.authService.user._id) {
      let user = this.authService.user;
      if (user.user_type != 'user') {
        this.helperService.errorMessage('You can not buy a stake. If you want to buy stake then you have to login as buyer user.');
        return false;
      }
      if (parseFloat(user.wallet_amount) && parseFloat(this.data.amount) <= parseFloat(user.wallet_amount) ){
        return this.modalService.open(this.buyConfirmation, { size: 'sm' });
      } else {
        this.helperService.errorMessage('You have not enough funds to buy a stake.');
      }
    } else {
      this.authService.showLogin()
    }
    return false;
  }

  buy() {
    this.custom_amount.nativeElement.value = '';

    let buyData = {
      post_id: this.data._id,
      staked_amount: parseFloat(this.data.amount),
      markup_amount: parseFloat(this.data.markup_amount)
    }
    this.loadingBuy = true;
    this.stackService.buyStack(buyData).then(
      (_res) => {
        this.loadingBuy = false;
        this.helperService.successMessage(_res, "Stake has been purchased successfully");
        this.modalService.dismissAll();
        this.data.is_show = !this.data.is_show;

        this.authService.user.wallet_amount = _res.transaction.closing_balance
        this.data.sold_total = _res.post.sold_total;
        this.data = this.data;

        this.authService.user = this.authService.user;



      }).catch((err) => {
        this.helperService.errorMessage(err, 'Please try again after some time.');
      });
  }

}
