import { Component, OnInit } from '@angular/core';
import { StackService } from '../../shared/services/stack/stack.service';
import { postGropByUser } from 'src/app/shared/utils';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  products:any[] = [];
  salons:any[] = [];

  listStack: any = [];

  constructor(
    private stackService: StackService,
  ) { }

  ngOnInit() {
    this.getAllActivePost();
  }
  getAllActivePost() {
    let input = {
      page: 0,
      limit:10
    }
    this.stackService.getAllActivePost(input).then((data) => {
      this.listStack = postGropByUser(data.data);
    })
  }

}
