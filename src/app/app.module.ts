import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { CommonModule } from "@angular/common"
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { Services } from './shared/services';
import { HttpClientModule } from '@angular/common/http';
import { NgbAlertModule, NgbToastModule } from '@ng-bootstrap/ng-bootstrap';
import { GraphQLModule } from './graphql.module';
import { AppInitService } from './shared/services/app-init.service';
import { NgxStripeModule } from 'ngx-stripe';
import { NgxPayPalModule } from 'ngx-paypal';
import { Config } from './config';
import { MomentModule } from 'ngx-moment';
import { ToastyModule } from './shared/modules/ngx-toasty';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { CookieStorage } from './shared/services/cookies/cookie.storage';
import { AppStorage } from './shared/services/cookies/app-storage';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserTransferStateModule,
    SharedModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    NgbAlertModule,
    GraphQLModule,
    NgxStripeModule.forRoot(Config.stripeKey),
    NgxPayPalModule,
    ToastyModule.forRoot(),
    MomentModule.forRoot()
  ],
  providers: [
    ...Services,
    { provide: 'REQUEST', useValue : REQUEST},
    { provide: AppStorage, useClass: CookieStorage },    
    {
      provide: APP_INITIALIZER,
      useFactory: (appInitService: AppInitService) => {
        return() => appInitService.init()
      },
      deps: [AppInitService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
