import { NgModule } from '@angular/core';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';
import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { UniversalStorage } from './shared/services/cookies/universal.storage';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppStorage } from './shared/services/cookies/app-storage';


@NgModule({
  imports: [
    AppModule,
    ServerModule,
    NoopAnimationsModule,
    ServerTransferStateModule
  ],
  providers : [
    { provide: AppStorage, useClass: UniversalStorage }
  ],
  bootstrap: [AppComponent],
})
export class AppServerModule {}
