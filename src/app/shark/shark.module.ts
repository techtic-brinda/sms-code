import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharkRoutingModule } from './shark-routing.module';
import { ListComponent } from './list/list.component';
import { ProfileComponent } from './profile/profile.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ListComponent, ProfileComponent],
  imports: [
    CommonModule,
    SharedModule,
    SharkRoutingModule
  ]
})
export class SharkModule { }
