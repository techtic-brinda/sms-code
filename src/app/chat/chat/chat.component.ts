import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ChatService } from 'src/app/shared/services/chat.service';
import { HelperService } from 'src/app/shared/services/helper.service';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'underscore';
import { AuthService } from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  all_contacts: any = [];
  contacts: any = [];
  active_contact: any;
  currentUser: any = {};
  new_message = "";

  showContacts:boolean = true;
  showMessages:boolean = true;

  @ViewChild('container', {static : false}) container: ElementRef
  loading: boolean;
  user: any;
  is_mobile: boolean;

  constructor(
    private chatService: ChatService,
    private activatedRoute: ActivatedRoute,
    private helperService: HelperService,
    private authService: AuthService,
  ) { }


  @HostListener('window:resize', ['$event']) // for window scroll events
  onResize() {
    if (window.innerWidth < 767) {
      this.is_mobile = true;
      //this.showContacts = true;
      //this.showMessages = false;
    } else {
      this.is_mobile = false;
      //this.showContacts = true;
      //this.showMessages = true;
    }
  }


  ngOnInit() {
    this.onResize();
    if (this.is_mobile){
      this.showContacts = true;
    }
    this.activatedRoute.params.subscribe((data) => {
      if (data.user_id) {
        let contact = _.find(this.contacts, { contact_id: data.user_id })
        if (!contact) {
          this.addContact(data.user_id);
        } else {
          this.changeUser(contact);
          this.showContacts = true
        }
      }
    })

    this.authService.$user.subscribe((user)=> {
      this.user = user
    });
    this.chatService.$message.subscribe(() => {
      this.active_contact.unread = 0;
      this.scrollToBottom()
    });
    
    this.chatService.$contacts.subscribe((data) => {
      if (data) {
        this.all_contacts = data;
        this.contacts = data;

        if (!this.active_contact) {
          this.changeUser(data[0]);
        }
      }
    });
  }

  filterList(event:any) {
    let value = event.target.value;
    this.contacts = _.filter(this.all_contacts, (contact) => {
      let str = contact.contact.first_name + " " + contact.contact.last_name;
      let values = value.split(' ');

      let seprate_match:boolean;
      if (values.length > 1){
        seprate_match = (contact.contact.first_name.indexOf(values[0]) >= 0 && contact.contact.last_name.indexOf(values[1]) >= 0) 
      }else{
        seprate_match = false;
      }
      return seprate_match || str.indexOf(value) >= 0;
    });
  }

  addContact(user_id: string) {
    this.chatService.addContact(user_id).then((contact) => {
      this.changeUser(contact);
    }).catch((error) => {

    })
  }

  getMoreMessages(contact: any) {
    const prev_page = (contact.page || 0) + 1;
    return this.chatService.getMessages({ page: prev_page, limit: 30 }, contact).then((data) => {
      this.loadMessages(data, contact, true)
    }).catch((error) => {
      this.helperService.errorMessage(error, "Error while connect with user, Please try again.")
    })
  }

  getMessages(contact: any) {
    return this.chatService.getMessages({ page: 0, limit: 30 }, contact).then((data) => {
      this.loadMessages(data, contact);
    }).catch((error) => {
      this.helperService.errorMessage(error, "Error while connect with user, Please try again.")
    })
  }

  sendMessage() {
  
    if (this.new_message.trim() == '' || this.new_message.trim() == null || this.new_message.trim() == undefined){
      this.helperService.errorMessage("Please enter valid message.")
    }else{
      this.chatService.sendMessage(this.new_message, this.active_contact).then(() => {
        this.new_message = "";
        this.scrollToBottom(true);
      }).catch((error) => {
        this.helperService.errorMessage(error, "Error while sending message, Please try again.")
      })
    }
   
  }

  async changeUser(connect:any) {

    if (connect == undefined){
      return
    }
    this.active_contact = connect;
    this.active_contact.unread = 0;
    this.chatService.markAsRead(connect.contact_id).then(()=>{
      this.chatService.updateCount()
    });
   
    if (!connect.messages || !connect.last_message_load) {
      this.loading = true;
      await this.getMessages(connect);
      this.loading = false
    }
    this.scrollToBottom(true);
  }

  scrollToBottom(firstTime: boolean = false) {
    setTimeout(() => {
      let container:HTMLElement = this.container.nativeElement;
      container.scrollTop = container.scrollHeight;
    }, 10);
  }

  loadMessages(data, contact, concat: boolean = false) {
    if (data.meta && data.meta.last_page <= contact.page) {
      contact.last_message_load = true
    } else {
      contact.last_message_load = false
    }

    let messages = data.data
    let new_messages: any[] = _.chain(messages).map((message) => {
      message = this.chatService.mapMessage(message);
      return message;
    }).reverse().value();
    if (concat) {
      contact.messages = new_messages.concat(contact.messages)
    } else {
      contact.messages = new_messages
    }
  }
}
