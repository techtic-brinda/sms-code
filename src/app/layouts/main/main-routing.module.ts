import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { AuthGuard } from 'src/app/shared/services/auth/auth.guard';

const routes: Routes = [
  {
    path : "",
    component: MainComponent,
    children : [
      {
        path: "",
        //loadChildren: "src/app/home/home.module#HomeModule"
        loadChildren: () => import('src/app/home/home.module').then(m => m.HomeModule)
      }, 
      { 
        path: '', 
        //loadChildren: 'src/app/auth/auth.module#AuthModule' 
        loadChildren: () => import('src/app/auth/auth.module').then(m => m.AuthModule)
      },
      {
        path: "my-account",
        canActivate: [AuthGuard],
        loadChildren: () => import('src/app/my-account/my-account.module').then(m => m.MyAccountModule)
      },
      {
        path: "private-coaching",
        canActivate: [AuthGuard],
        loadChildren: () => import('src/app/private-coaching/private-coaching.module').then(m => m.PrivateCoachingModule)
      },
      {
        path: "shark",
        loadChildren: () => import('src/app/shark/shark.module').then(m => m.SharkModule)
      },
      {
        path: "meet-our-sharks",
        canActivate: [AuthGuard],
        loadChildren: () => import('src/app/meet-our-sharks/meet-our-sharks.module').then(m => m.MeetOurSharksModule)
      },
      {
        path: "chat",
        loadChildren: () => import('src/app/chat/chat.module').then(m => m.ChatModule)
      },
      {
        path: "marketplace",
        loadChildren: () => import('src/app/marketplace/marketplace.module').then(m => m.MarketplaceModule)
      },
      {
        path: "support-ticket",
        loadChildren: () => import('src/app/support-ticket/support-ticket.module').then(m => m.SupportTicketModule)
      },
      {
        path: "",
        loadChildren: () => import('src/app/follow/follow.module').then(m => m.FollowModule)
      },
      {
        path: "",
        loadChildren: () => import('src/app/page/page.module').then(m => m.PageModule)
      }
      
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
