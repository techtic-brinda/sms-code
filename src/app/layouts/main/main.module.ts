import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main/main.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider } from "angular-6-social-login";



export function getAuthServiceConfigs() {
	let config = [
		// {
  //        id: FacebookLoginProvider.PROVIDER_ID,
	 //      provider: new FacebookLoginProvider("684998025334252")
  //       },
		// {
		// 	id: GoogleLoginProvider.PROVIDER_ID,
		// 	provider: new GoogleLoginProvider("412930796312-3ppcgqgqem19rn9jka1vgc8th2l73o0m.apps.googleusercontent.com")
		// }
	];
	return new AuthServiceConfig(config);
}


@NgModule({
  declarations: [ MainComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
    NgbModalModule,
    NgbDropdownModule,
    SocialLoginModule,
  ],
  providers: [
    {
		provide: AuthServiceConfig,
		useFactory: getAuthServiceConfigs
    }
  ]
})
export class MainModule { }
