import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, NavigationStart, NavigationEnd, NavigationCancel } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppInitService } from 'src/app/shared/services/app-init.service';
import { AuthService as SocialAuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angular-6-social-login';
import { HelperService } from 'src/app/shared/services/helper.service';
import { environment } from 'src/environments/environment';
import * as _ from 'underscore';
import { RouteUtil } from 'src/app/shared/services/route';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { ChatService } from 'src/app/shared/services/chat.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  host: {
    "[class.shrink]": "shrink"
  }
})
export class MainComponent implements OnInit {
  header_type: any = "with-background";
  paymentMethod = 'credit';
  paymentData: any = {};

  shrink: boolean = false;
  user: any = null;

  loginResponse: any;
  loginForm: any = {};

  registerResponse: any;
  registerForm: any = {};
  discordUrl: any = environment.discordUrl;

  forgotResponse: any;
  forgotForm: any = {};

  @ViewChild('loginModelContent', { static: false }) loginModelContent;
  @ViewChild('registerModelContent', { static: false }) registerModelContent;
  @ViewChild('forgotModelContent', { static: false }) forgotModelContent;

  sport_types: any = [];
  sport_types_footer: any = [];

  selected_sport_type: any;
  loading: boolean = false;
  notificationCnt:any;
  activeToggle: boolean;
  chat_counts: number = 0;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private appInitService: AppInitService,
    private modalService: NgbModal,
    private socialAuthService: SocialAuthService,
    private routeUtil: RouteUtil,
    private helperService: HelperService,
    private chatService: ChatService,
    private notificationService: NotificationService
  ) {

    this.appInitService.$sport_types.subscribe((data: any) => {
      // console.log(data,'d');
      this.sport_types = data;
      data.forEach(element => {
        if(element.status == 'active'){
          this.sport_types_footer.push(element);
        }
      });
    })

    this.routeUtil.$params.subscribe((params) => {
      if (params.sport_slug) {
        this.selected_sport_type = _.findWhere(this.sport_types, { slug: params.sport_slug });
      } else {
        this.selected_sport_type = null
      }
    })
    this.authService.$onShowLogin.subscribe((data) => {
      if (data) {
        this.openModel(this.loginModelContent, () => {
          this.router.navigate([], { replaceUrl: true });
        }).then(() => { })
      }
    })

    this.chatService.$contactsCount.subscribe((data) => {
      this.chat_counts = data
    });
  }
  restToggle(){
    if (this.activeToggle == false && this.user){
      this.activeToggle = true;
    }else{
      this.activeToggle = false;
    }
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: any) => {
      this.header_type = data.header;
    })
    this.notificationService.$counts.subscribe((data: any) => {
      this.notificationCnt = data;
    })

    this.authService.$user.subscribe((data) => {
      if (data) {
        this.user = data;
        this.notificationService.getNotificationCount()
      } else {
        this.user = null;
      }
    })
    this.onScroll();
  }

  openModel(content, beforeDismiss?: any) {
    this.blankAllform();

    return this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'lg', beforeDismiss: beforeDismiss }).result.then(() => {

    }, () => {

    });
  }

  ngAfterViewInit() {
    this.router.events
      .subscribe((event) => {

        if (event instanceof NavigationStart) {
          this.loading = true;
        }
        else if (
          event instanceof NavigationEnd ||
          event instanceof NavigationCancel
        ) {
          this.loading = false;
        }
      });

    this.activatedRoute.queryParams.subscribe((query: any) => {

      if (query.login) {
        this.authService.showLogin();
      }
    })

  }



  blankAllform() {
    this.forgotForm = {};
    this.loginForm = {};
    this.registerForm = {};

    this.loginResponse = null;
    this.registerResponse = null;
    this.forgotResponse = null;
  }

  doLogin(form) {
    if (form.valid) {
    this.authService.login(this.loginForm).then(
      (_res) => {
        this.loginForm = {};
        this.modalService.dismissAll();
        this.router.navigate(['/']);
        this.helperService.successMessage(_res, 'You have been successfully logged in.');
      }).catch((err) => {
        this.loginResponse = err;
      });
    }
  }

  doRegister(form) {
    this.registerForm.user_type = 'user';
    this.registerForm.status = 'active';
    if(form.valid){
      this.authService.register(this.registerForm)
      .then((_res) => {
        this.openModel(this.loginModelContent);
        this.registerForm = {};
        this.modalService.dismissAll();
        this.helperService.successMessage(_res, 'You have been successfully registered and logged in.');
      })
      .catch((err) => {
        this.registerResponse = err;
      });
    }
  }

  doForgot(form) {
    if (form.valid) {
    this.authService.forgotPassword(this.forgotForm).then(
      (_res) => {
        this.modalService.dismissAll();
        this.forgotForm = {};
        this.helperService.successMessage(_res, 'Password reset link has been sent on your email, Please check your inbox.');
      }).catch((err) => {
        this.forgotResponse = err;
      });
    }
  }

  logout() {
    this.authService.logout()
      .then(() => {
        this.router.navigate(['/']);
        this.activeToggle = false;
      }).catch((err) => {
        console.log(err);
      });
  }

  socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (_res) => {
        console.log(socialPlatform + " sign in data : ", _res);
        if (_res) {
          this.authService.socialLogin({
            provider: _res.provider,
            token: _res.token,
            email: _res.email,
            name: _res.name,
            user_type: "user"
          }).then(
            (_res) => {
              this.openModel(this.loginModelContent);
              this.registerForm = {};
              this.modalService.dismissAll();
              this.helperService.successMessage(_res, 'You have been successfully logged in.');
              this.router.navigate(['/']);
            }).catch((err) => {
              this.registerResponse = err;
            });
        }
      }
    );
  }

  openPaymentModel(content) {
    this.paymentMethod = "credit";
    this.openModel(content);
  }

  openModelThankyouModel($event, content) {
    this.modalService.dismissAll();
    this.modalService.open(content, { size: 'lg' });
    this.paymentData = $event;
    this.authService.user = Object.assign(this.authService.user, { wallet_amount: $event.closing_balance });
  }


  @HostListener('window:scroll', ['$event']) // for window scroll events
  onScroll() {
    if (window.scrollY > 50) {
      this.shrink = true;
    } else {
      this.shrink = false;
    }
  }
}
