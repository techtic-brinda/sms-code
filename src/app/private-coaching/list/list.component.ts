import { Component, OnInit } from '@angular/core';
import { UserAvailabilityService } from 'src/app/shared/services/auth/user-availability.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  upcomming_request: any = {
    page: 0,
    limit: 10
  };
  upcomming_list: any[] = [];
  past_request: any = {
    page: 0,
    limit: 10
  };
  past_list: any[] = [];
  currentUser: any;
  upcomingLoading = false;
  upcomingLoadMore = false;
  pastLoading = false;
  pastLoadMore = false;

  constructor(
    private userAvailabilityService: UserAvailabilityService,
    private authService: AuthService
  ) { }

  ngOnInit() {

    this.authService.$user.subscribe((data) => {
      if (!data) {
        return
      }
      this.currentUser = data;
    });
    this.upcomingSession().then((data: any) => {
      this.upcomming_list = data;
    });
    this.pastSession().then((data: any) => {
      this.past_list = data;
    });
  }
  
  upcomingSession(){
    this.upcomming_request.start = new Date();
    this.upcomingLoading = true;
    return this.userAvailabilityService.getRequestList(this.upcomming_request).then((data) => {
      this.upcomingLoading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.upcomingLoadMore = true;
      } else {
        this.upcomingLoadMore = false;
      }
      return data.data;
    })
  }
  pastSession(){
    this.past_request.end = new Date();
    this.pastLoading = true;
    return this.userAvailabilityService.getRequestList(this.past_request).then((data) => {
      this.pastLoading = false;
      if (data.meta.last_page > data.meta.current_page) {
        this.pastLoadMore = true;
      } else {
        this.pastLoadMore = false;
      }
      return data.data;
    })
  }
  onLoadMoreUpcoming() {
    this.upcomming_request.page++;
    this.upcomingSession().then((data) => {
      this.upcomming_list = this.upcomming_list.concat(data);
    });
  }

  onLoadMorePast(){
    this.past_request.page++;
    this.pastSession().then((data) => {
      this.past_list = this.past_list.concat(data);
    });
  }


}
