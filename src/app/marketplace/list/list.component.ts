import { Component, OnInit } from '@angular/core';
import { AppInitService } from 'src/app/shared/services/app-init.service';
import { StackService } from 'src/app/shared/services/stack/stack.service';
import { UserService } from 'src/app/shared/services/auth/user.service';
import { _ } from 'underscore';
import { ActivatedRoute, Router } from '@angular/router';
import { flatMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  sport_types: any = [];
  listStack: any = [];
  loadMore: boolean = false;
  loading: boolean = true;
  sport_type_id: any = "";
  sport_type_status: any = "active";
  postRequest: any = {
    page: 0,
    limit: 10
  };
  players: any = [];

  constructor(
    private appInitService: AppInitService,
    private userService: UserService,
    private stackService: StackService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {

  }

  async ngOnInit() {

    this.userService.getPlayers().then((data) => {
      this.players = data;
    });

    this.appInitService.$sport_types.pipe(
      tap((data) => {
        data.forEach(element => {
          if(element.status == 'active'){
            this.sport_types.push(element);
          }
        });
      }),
      flatMap(() => this.activatedRoute.params)
    )
    .subscribe((data)=>{
      if (data && data.sport_slug) {
        const sport_type = _.findWhere(this.sport_types, { 'slug': data.sport_slug });
        console.log(sport_type,'sport_type');
        if (sport_type) {
          this.sport_type_id = sport_type._id;
          this.sport_type_status = sport_type.status;
        }
        this.applyFilter('sport_type_id', this.sport_type_id);
      }
    })
    this.listStack = await this.getAllActivePost();
  }

  redirectSportType(_id){
    const sport_type = _.findWhere(this.sport_types, { _id });
    if (sport_type){
      this.router.navigate(['/marketplace', sport_type.slug]);
    }else{
      this.router.navigate(['/marketplace']);
    }
  }

  async applyFilter(key: string, value?: string) {
    this.loading = true;
    if (value) {

      this.postRequest[key] = value;
    } else {
      delete this.postRequest[key];
    }
    this.postRequest.page = 0;
    this.listStack = await this.getAllActivePost();
  }

  onLoadMore() {
    this.postRequest.page++;
    this.getAllActivePost().then((data)=>{
      this.listStack = this.listStack.concat(data);
    });
  }


  getAllActivePost() {

    this.loading = true;
    return this.stackService.getAllActivePost(this.postRequest).then((data) => {
      this.loading = false;

      if (data.meta.last_page > data.meta.current_page) {
        this.loadMore = true;
      }else{
        this.loadMore = false;
      }
      return _.chain(data.data).groupBy('user_id').compact().map((item) => {
        const user = item[0].user || {};
        const posts = _.map(item, (post) => {
          delete item.user;
          return post;
        })
        user.posts = posts;
        return user;
      }).value();
    })
  }

}
