import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { HelperService } from 'src/app/shared/services/helper.service';

@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.scss']
})
export class SetPasswordComponent implements OnInit {

  token:string;
  passwordForm :any = {};
  passwordError:any;
  passwordResponse: any;
  isValid: any = false;
  constructor(
    private activeRoute: ActivatedRoute,
    private authService:AuthService,
    private router: Router,
    private helperService: HelperService,
  ) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((routeParams) => {
			if (routeParams.token != undefined && routeParams.token != null) {
        this.token = routeParams.token;
			}
    });
    this.authService.$user.subscribe((data) => {
      if (data) {
        this.router.navigate(['/']);
      }
    });
  }

  setPassword() {
    if (this.passwordForm.password != this.passwordForm.confirm_password) {
      this.isValid = true;
      return false;
    } else {
      this.isValid = false;
    }
    this.passwordForm.token = this.token;
    this.authService.setPassword(this.passwordForm).then(
      (_res) => {
        this.helperService.successMessage(_res, 'Your password successfully changed.');
        this.router.navigate(['/']);
      }).catch((err) => {
        this.passwordResponse = err;
        this.helperService.errorMessage(err, 'something going wrong. Please try again.');
      });
  }

}
