import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: any = {};
  registerForm: any = {};
  loginError: any;
  registerError: any;


  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    const currentUser = this.authService.isLoggedIn;
    if (currentUser) {
      this.router.navigate(['/']);
    }
  }

  doLogin() {

    this.authService.login(this.loginForm).then(
      (_res) => {
        this.router.navigate(['/']);
      }).catch((err) => {
        this.loginError = err;
      });
  }

  doRegister() {
    this.authService.register(this.registerForm).then(
      (_res) => {
        this.router.navigate(['/']);
      }).catch((err) => {
        this.registerError = err;
      });
  }
}
