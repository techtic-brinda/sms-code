import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private authService:AuthService,private router: Router) { }
  forgetPassword:any = {};
  forgetPasswordError:any;
  

  ngOnInit() {
  }

  doForgetPassword(){
  	this.authService.forgotPassword(this.forgetPassword).then(
		(res)=>{
			this.router.navigate(['/login']);
		}).catch((err)=>{
			console.log(err);
		});
  }

}
