// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.6.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
    apiKey: "AIzaSyD3Hc2xsvxFDf_Igb511o6A8UC4Zguwrl8",
    authDomain: "stake-my-shark-7ff1d.firebaseapp.com",
    databaseURL: "https://stake-my-shark-7ff1d.firebaseio.com",
    projectId: "stake-my-shark-7ff1d",
    storageBucket: "stake-my-shark-7ff1d.appspot.com",
    messagingSenderId: "890500589181",
    appId: "1:890500589181:web:aced84dd2421903de47314",
    measurementId: "G-8HNJW8D0XP"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

// messaging.setBackgroundMessageHandler(function (payload) {
//     console.log('[firebase-messaging-sw.js] Received background message ', payload);
//     // Customize notification here
//     const notificationTitle = 'Background Message Title';
//     const notificationOptions = {
//         body: 'Background Message body.',
//     };

//     return self.registration.showNotification(notificationTitle,notificationOptions);
// });
