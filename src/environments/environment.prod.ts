export const environment = {
  production: true,
  apiUrl: 'https://stakemyshark.com/api',
  deployUrl: '',
  baseUrl: '',
  graphQlUrl: 'https://stakemyshark.com',
  graphQlWsUrl: 'ws://stakemyshark.com/graphql',
  discordUrl: 'https://discord.gg/yJshZV7',
};
