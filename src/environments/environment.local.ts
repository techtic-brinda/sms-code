
export const environment = {
    production: true,
    apiUrl: 'http://localhost:3000/api',
    deployUrl: '',
    baseUrl: '',
    graphQlUrl: 'http://localhost:3000',
    graphQlWsUrl: 'ws://localhost:3000/graphql',
    discordUrl: 'https://discord.gg/yJshZV7',
};
