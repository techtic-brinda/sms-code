
export const environment = {
  production: true,
  apiUrl: 'http://45.79.111.106:3000/api',
  deployUrl: '',
  baseUrl: '',
  graphQlUrl: 'http://45.79.111.106:3000',
  graphQlWsUrl: 'ws://45.79.111.106:3000/graphql',
  discordUrl: 'https://discord.gg/yJshZV7',
};
