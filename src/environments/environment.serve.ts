// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: '/api',
  deployUrl: '',
  baseUrl: '',
  graphQlUrl: 'http://localhost:3000',
  graphQlWsUrl: 'ws://localhost:3000/graphql',
  discordUrl: 'https://discord.gg/yJshZV7',
};
